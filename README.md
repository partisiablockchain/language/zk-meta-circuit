# Meta-Circuit Model

Meta Circuit Model

## ZK Bytecode Version

The _ZK bytecode version_ specifies the features that a ZK VM  must support to be able to run a specific ZK computation.

- The ZK compiler creates ZK bytecode which requires a specific _ZK bytecode version_.
- The ZK VM (zk-lowering + zk-circuit) support executing a range of _ZK bytecode versions_.

When the meta-circuit is serialized as bytecode the _ZK bytecode version_ is  automatically reverted to the lowest version possible.
This is achieved by inspecting all opcodes that are used in the meta-circuit being serialized.

| **Pom version** | **Highest ZK bytecode version** | **Changes**                                                                                |
|-----------------|---------------------------------|--------------------------------------------------------------------------------------------|
| 3.47.0          | 1.8.0                           | Extended maximum type sizes from `512` bits to `16383` bits. |
| 3.41.0          | 1.7.0                           | Added `EXTRACTDYN` and `INSERTDYN` to better support array operations. |
| 3.30.0          | 1.6.0                           | Added functions, including `RETURN` and `CALL`. Old bytecode are still supported, but will produce alternative representations. |
| 3.16.0          | 1.5.0                           | Added `OUTPUT` operation. First operation with side-effects.                               |
| 3.9.0           | 1.4.0                           | Added `NEXT_VARIABLE_ID` operation for iterating over all contract variables.              |
| 3.6.0           | 1.3.0                           | Added `EXTRACT` and `CONCAT` operation for operating over bit sequences.                   |
| 3.0.5           | 1.2.0                           | Added support for flexible versioning.                                                     |
| 3.0.4           | 1.2.0                           | Added unsigned operations `LESS_THAN_OR_EQUAL_SIGNED`, etc.                                |
| 0.4.6           | 1.1.0                           | Added `NUM_VARIABLES`.                                                                     |
| 0.4.3           | 1.0.0                           | First meta circuit version with a version. Also added `LOAD_VARIABLE` and `LOAD_METADATA`. |
