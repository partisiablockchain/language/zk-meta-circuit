(metacircuit
 (function %0
  (output sbi35)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi35 $1 (extract $0 17 0))
    (branch-always #return $1))))
