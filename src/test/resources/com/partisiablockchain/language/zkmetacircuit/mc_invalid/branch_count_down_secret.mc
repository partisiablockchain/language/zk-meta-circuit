(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs (sbi32 $0))
    (sbi32 $1 (constant 0))
    (sbi32 $2 (bitwise_xor $0 $1))
    (sbi1 $3 (equal $1 $2))
    (branch-if $3
      (0 #return $2)
      (1 #1 $2)))
  (block #1
    (inputs (sbi32 $3))
    (sbi32 $4 (constant 0))
    (sbi32 $5 (bitwise_not $4))
    (sbi32 $6 (add_wrapping $3 $5))
    (sbi1 $7 (equal $6 $4))
    (branch-if $7
      (0 #return $6)
      (1 #1 $6)))))
