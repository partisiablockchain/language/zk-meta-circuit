(metacircuit
 (function %0
  (output i1 i1)
  (block #0
    (inputs (i1 $0))
    (branch-if $0
      (0 #return $1 $2)
      (1 #return $2 $1)))))
