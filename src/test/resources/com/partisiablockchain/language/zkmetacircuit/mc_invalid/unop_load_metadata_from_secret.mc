(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $0 (constant 1))
    (i32 $1 (load_metadata $0))
    (branch-always #return $0))))
