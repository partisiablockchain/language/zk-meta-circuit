(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi1 $2 (extractdyn $0 1 $1))
    (branch-always #return $2))))
