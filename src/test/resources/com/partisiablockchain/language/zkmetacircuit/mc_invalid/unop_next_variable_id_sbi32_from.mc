(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $0 (constant 1))
    (sbi32 $1 (next_variable_id $0))
    (branch-always #return $1))))
