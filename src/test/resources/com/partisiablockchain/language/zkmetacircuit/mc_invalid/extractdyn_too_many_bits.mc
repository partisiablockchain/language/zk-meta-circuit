(metacircuit
 (function %0
  (output sbi2)
  (block #0
    (inputs
      (sbi1 $0))
    (i32 $1 (constant 0))
    (sbi2 $2 (extractdyn $0 2 $1))
    (branch-always #return $2))))
