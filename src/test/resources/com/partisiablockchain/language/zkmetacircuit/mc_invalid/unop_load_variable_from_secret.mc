(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $0 (constant 1))
    (sbi32 $1 (load_variable $0))
    (branch-always #return $1))))
