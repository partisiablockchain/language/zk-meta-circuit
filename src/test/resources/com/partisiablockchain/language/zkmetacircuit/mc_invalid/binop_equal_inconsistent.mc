(metacircuit
 (function %0
  (block #0
    (inputs
      (sbi32 $0)
      (sbi1 $1))
    (sbi1 $2 (equal $0 $1)))))
