(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi16 $0))
    (i32 $1 (constant 0))
    (sbi1 $2 (extractdyn $2 1 $1))
    (branch-always #return $2))))
