(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi1 $1 (extract $2 1 0))
    (branch-always #return $1))))
