(metacircuit
 (function %0
  (output sbi5)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi5 $1 (extract $0 5 12))
    (branch-always #return $1))))
