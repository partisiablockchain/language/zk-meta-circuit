(metacircuit
 (function %0
  (output i64)
  (block #0
    (inputs
      (i64 $0)
      (sbi7 $1))
    (i64 $2 (bit_concat $0 $1))
    (branch-always #return $2))))
