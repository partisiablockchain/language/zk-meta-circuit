(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0)
      (i7 $1))
    (sbi64 $2 (bit_concat $0 $1))
    (branch-always #return $2))))
