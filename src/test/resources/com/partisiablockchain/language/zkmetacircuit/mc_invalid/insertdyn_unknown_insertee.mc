(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i3 $2 (constant 0))
    (sbi32 $3 (insertdyn $0 $1 $2))
    (branch-always #return $3))))
