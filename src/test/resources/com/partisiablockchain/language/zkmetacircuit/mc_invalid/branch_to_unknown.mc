(metacircuit
 (function %0
  (block #0
    (i1 $0 (constant 0))
    (i1 $1 (bitwise_and $0 $0))
    (i1 $2 (bitwise_xor $1 $1))
    (branch-if $2
      (0 #return)
      (1 #99)))))
