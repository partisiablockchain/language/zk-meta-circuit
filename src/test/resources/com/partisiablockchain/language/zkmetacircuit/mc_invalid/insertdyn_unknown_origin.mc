(metacircuit
 (function %0
  (output sbi2)
  (block #0
    (inputs)
    (i3 $1 (constant 0))
    (i3 $2 (constant 0))
    (sbi3 $3 (insertdyn $0 $1 $2))
    (branch-always #return $3))))
