(metacircuit
 (function %0
  (output sbi2)
  (block #0
    (inputs
      (sbi3 $0))
    (sbi32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (sbi3 $3 (insertdyn $0 $1 $2))
    (branch-always #return $3))))
