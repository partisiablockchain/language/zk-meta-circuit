(metacircuit
 (function %0
  (output sbi7)
  (block #0
    (inputs
      (sbi64 $0)
      (sbi32 $1))
    (sbi7 $2 (extractdyn $0 7 $1))
    (branch-always #return $2))))
