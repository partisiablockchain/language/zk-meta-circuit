(metacircuit
 (function %0
  (output i5)
  (block #0
    (inputs
      (i64 $0))
    (i5 $1 (extract $0 5 5))
    (branch-always #return $1))))
