(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs (i32 $0))
    (i32 $1 (constant 0))
    (i32 $2 (bitwise_xor $0 $1))
    (i1 $3 (equal $1 $2))
    (branch-if $3
      (0 #return $2)
      (1 #1 $2)))
  (block #1
    (inputs (i32 $0))
    (i32 $1 (constant 0))
    (i32 $2 (bitwise_not $1))
    (i32 $3 (add_wrapping $0 $2))
    (i1 $4 (equal $3 $1))
    (branch-if $4
      (0 #return $3)
      (1 #1 $3)))))
