(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (i32 $2 (add_wrapping $0 $1))
    (i32 $3 (mult_wrapping_signed $0 $1))
    (i32 $4 (mult_wrapping_unsigned $0 $1))
    (i1 $5 (less_than_signed $2 $3))
    (i1 $6 (less_than_or_equal_signed $3 $4))
    (i1 $7 (less_than_unsigned $5 $6))
    (i1 $8 (less_than_or_equal_unsigned $5 $7))
    (branch-always #return $8))))
