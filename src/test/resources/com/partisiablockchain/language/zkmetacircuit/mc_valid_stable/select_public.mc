(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (i1 $0)
      (i64 $1)
      (i64 $2))
    (i64 $3 (select $0 $1 $2))
    (sbi64 $4 (cast $2))
    (branch-always #return $4))))
