(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (num_variables))
    (sbi32 $1 (cast $0))
    (branch-always #return $1))))
