(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0)
      (i8 $1))
    (i32 $2 (bitshift_right_logical $0 $1))
    (i32 $3 (bitshift_left_logical $2 $1))
    (branch-always #return $3))))
