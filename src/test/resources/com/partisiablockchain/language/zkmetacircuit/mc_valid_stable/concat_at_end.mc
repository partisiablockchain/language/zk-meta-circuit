(metacircuit
 (function %0
  (output sbi47)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi15 $1))
    (sbi47 $2 (bit_concat $0 $1))
    (branch-always #return $2))))
