(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi1 $0)
      (sbi64 $1))
    (sbi64 $2 (select $0 $1 $1))
    (branch-always #return $2))))
