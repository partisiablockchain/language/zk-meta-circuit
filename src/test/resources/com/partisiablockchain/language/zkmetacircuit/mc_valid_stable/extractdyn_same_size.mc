(metacircuit
 (function %0
  (output sbi5)
  (block #0
    (inputs
      (sbi5 $0))
    (i32 $1 (constant 0))
    (sbi5 $2 (extractdyn $0 5 $1))
    (branch-always #return $2))))
