(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 1))
    (sbi32 $1 (cast $0))
    (branch-always #return $1)))
)
