(metacircuit
 (function %0
  (block #0
    (sbi64 $0 (constant 1))
    (sbi64 $1 (bitwise_not $0))
    (sbi64 $2 (bitwise_and $0 $1))
    (sbi64 $3 (bitwise_xor $1 $0))
    (sbi64 $4 (bitwise_or $2 $3)))))
