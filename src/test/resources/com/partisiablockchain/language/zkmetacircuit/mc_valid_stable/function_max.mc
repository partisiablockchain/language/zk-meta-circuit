(metacircuit
 (function %0
  (output sbi64 sbi64)
  (block #0
    (inputs
      (sbi64 $0)
      (sbi64 $1))
    (call
      (%1 $0 $1)
      (#return $0))))
 (function %1
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0)
      (sbi64 $1))
    (sbi1 $2 (less_than_signed $0 $1))
    (sbi64 $3 (select $2 $1 $0))
    (branch-always #return $3))))
