(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0)
      (sbi64 $1))
    (sbi64 $2 (bitwise_and $0 $1))
    (branch-always #return $2))))
