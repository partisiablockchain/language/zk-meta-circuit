(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (i32 $1 (constant 0))
    (sbi64 $2 (constant 1))
    (sbi64 $3 (insertdyn $0 $2 $1))
    (branch-always #return $3))))
