(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi32 $2 (add_wrapping $0 $1))
    (sbi32 $3 (mult_wrapping_signed $0 $1))
    (sbi32 $4 (mult_wrapping_unsigned $0 $1))
    (sbi1 $5 (less_than_signed $2 $3))
    (sbi1 $6 (less_than_or_equal_signed $3 $4))
    (sbi1 $7 (less_than_unsigned $5 $6))
    (sbi1 $8 (less_than_or_equal_unsigned $5 $7))
    (branch-always #return $8))))
