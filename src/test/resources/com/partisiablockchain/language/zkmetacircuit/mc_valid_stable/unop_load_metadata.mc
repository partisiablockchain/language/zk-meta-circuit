(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 1))
    (i32 $1 (load_metadata $0))
    (sbi32 $2 (cast $1))
    (branch-always #return $2))))
