(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (insertdyn $0 $0 $0))
    (branch-always #return $1))))
