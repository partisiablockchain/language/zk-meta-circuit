(metacircuit
 (function %0
  (output i5)
  (block #0
    (inputs
      (i64 $0)
      (i32 $1))
    (i5 $2 (extractdyn $0 5 $1))
    (branch-always #return $2))))
