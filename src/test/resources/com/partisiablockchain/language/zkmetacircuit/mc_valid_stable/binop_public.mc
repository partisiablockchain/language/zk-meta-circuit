(metacircuit
 (function %0
  (output i64)
  (block #0
    (i64 $0 (constant 1))
    (i64 $1 (bitwise_not $0))
    (i64 $2 (bitwise_and $0 $1))
    (i64 $3 (bitwise_xor $1 $0))
    (i64 $4 (bitwise_or $2 $3))
    (branch-always #return $4)))
)
