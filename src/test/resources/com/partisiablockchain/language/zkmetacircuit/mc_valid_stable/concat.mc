(metacircuit
 (function %5
  (output sbi49)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi17 $1))
    (sbi49 $2 (bit_concat $0 $1))
    (branch-always #return $2))))
