(metacircuit
 (function %0
  (output i32)
  (block #0
    (i32 $0 (constant 1))
    (i32 $1 (next_variable_id $0))
    (i32 $2 (next_variable_id $1))
    (i32 $3 (next_variable_id $2))
    (i32 $4 (next_variable_id $3))
    (branch-always #return $4)))
)
