(metacircuit
 (function %0
  (output i1)
  (block #0
    (i1 $0 (constant 0))
    (i1 $1 (bitwise_and $0 $0))
    (i1 $2 (bitwise_xor $1 $1))
    (branch-if $2
      (0 #1 $1 $1)
      (1 #1 $2 $2)))
  (block #1
    (inputs
      (i1 $0)
      (i1 $1))
    (i1 $2 (bitwise_and $0 $0))
    (branch-always #return $2))))
