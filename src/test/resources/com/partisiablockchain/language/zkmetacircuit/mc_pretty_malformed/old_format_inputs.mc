(metacircuit
  (output)
  (inputs
    (sbi1 $0))
  (block #0
    (sbi1 $1 (constant 1))
    (sbi1 $2 (bitwise_xor $0 $1))))
