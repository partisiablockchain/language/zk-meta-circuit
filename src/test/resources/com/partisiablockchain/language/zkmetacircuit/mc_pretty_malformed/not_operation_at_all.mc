(metacircuit
 (function %1
  (output sbi64)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi1 $1))
    (sbi1 $2 (add $0 $1)))))
