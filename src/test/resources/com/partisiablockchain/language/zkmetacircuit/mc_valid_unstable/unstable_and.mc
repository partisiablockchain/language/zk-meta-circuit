(metacircuit
 (function %0
  (output sbi64)
  (block #9
    (inputs
      (sbi64 $44)
      (sbi64 $11))
    (sbi64 $22 (bitwise_and $44 $11))
    (branch-always #return $22))))

