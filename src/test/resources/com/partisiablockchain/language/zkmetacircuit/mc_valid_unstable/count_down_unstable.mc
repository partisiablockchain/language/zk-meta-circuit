(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs (i32 $0))
    (i32 $1 (constant 0))
    (i32 $2 (bitwise_xor $0 $1))
    (i1 $3 (equal $1 $2))
    (branch-if $3
      (0 #return $2)
      (1 #1 $2)))
  (block #1
    (inputs (i32 $3))
    (i32 $4 (constant 0))
    (i32 $5 (bitwise_not $4))
    (i32 $6 (add_wrapping $3 $5))
    (i1 $7 (equal $6 $4))
    (branch-if $7
      (0 #return $6)
      (1 #1 $6)))))
