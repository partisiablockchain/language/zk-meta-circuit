(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi64 $44))
    (sbi32 $1 (extract $44 32 32))
    (branch-always #return $1))))
