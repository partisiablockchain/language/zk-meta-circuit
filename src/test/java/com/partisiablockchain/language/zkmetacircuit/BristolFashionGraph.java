package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

/**
 * Self-contained Bristol Fashion formatted MPC circuit.
 *
 * @see <a href="https://homes.esat.kuleuven.be/~nsmart/MPC/">'Bristol Fashion' MPC Circuits</a>
 */
public record BristolFashionGraph(
    List<Line> lines, List<Integer> inputSizes, List<Integer> outputSizes, int firstOutputWire) {

  /**
   * Constructor.
   *
   * @param lines Never null
   * @param inputSizes Never null
   * @param outputSizes Never null
   * @param firstOutputWire Never negative
   */
  public BristolFashionGraph {
    requireNonNull(lines);
    requireNonNull(inputSizes);
    requireNonNull(outputSizes);
    if (firstOutputWire < 0) {
      throw new IllegalArgumentException("firstOutputWire cannot be negative");
    }
  }

  /** Individual line in an Bristol Fashion formatted MPC circuit. */
  record Line(String operation, List<Integer> inputWires, List<Integer> outputWires) {}

  /**
   * Number of input wires that the graph possess.
   *
   * @return Number of input wires
   */
  public int numInputWires() {
    return inputSizes().stream().mapToInt(x -> x).sum();
  }

  /**
   * Parses text to an Bristol Fashion graph instance.
   *
   * @param text Text to parse
   * @return Newly created Bristol Fashion graph
   */
  public static BristolFashionGraph parseFrom(final String text) {
    final Iterator<String> defWords = List.of(text.split("\\s+")).iterator();

    final int numGates = Integer.parseInt(defWords.next());
    final int numWires = Integer.parseInt(defWords.next());

    final int numInputSizes = Integer.parseInt(defWords.next());
    final List<Integer> inputSizes = iota(numInputSizes, idx -> Integer.parseInt(defWords.next()));
    final int numOutputSizes = Integer.parseInt(defWords.next());
    final List<Integer> outputSizes =
        iota(numOutputSizes, idx -> Integer.parseInt(defWords.next()));

    final ArrayList<Line> parsedLines = new ArrayList<>();

    for (int gateIdx = 0; gateIdx < numGates; gateIdx++) {
      final int numInputs = Integer.parseInt(defWords.next());
      final int numOutputs = Integer.parseInt(defWords.next());
      final List<Integer> inputs = iota(numInputs, idx -> Integer.parseInt(defWords.next()));
      final List<Integer> outputs = iota(numOutputs, idx -> Integer.parseInt(defWords.next()));
      parsedLines.add(new Line(defWords.next(), inputs, outputs));
    }

    final int firstOutputWire = numWires - outputSizes.stream().mapToInt(x -> x).sum();
    return new BristolFashionGraph(
        List.copyOf(parsedLines), inputSizes, outputSizes, firstOutputWire);
  }

  /**
   * Checks that the given Bristol Fashion graph is correctly ordered. By correctly ordered is
   * meant: All wires are defined as outputs before they are used as inputs.
   *
   * @return true if and only if the graph is correctly ordered
   */
  public boolean correctlyOrdered() {
    final int numInputWires = numInputWires();
    final var availableWires = new HashSet<Integer>();
    for (int lineIdx = 0; lineIdx < lines().size(); lineIdx++) {
      final Line line = lines().get(lineIdx);
      if (!isConstantOperation(line.operation())) {
        final boolean allInputsAvailable =
            line.inputWires().stream()
                .allMatch(
                    x -> {
                      final boolean wireAvailable = availableWires.contains(x);
                      final boolean wireIsInput = 0 <= x && x < numInputWires;
                      return wireAvailable || wireIsInput;
                    });
        if (!allInputsAvailable) {
          return false;
        }
      }
      availableWires.addAll(line.outputWires());
    }
    return true;
  }

  private static boolean isConstantOperation(final String opcode) {
    return "EQ".equals(opcode);
  }

  /**
   * Transforms a Bristol Fashion graph to a list of instructions. Assumes that input graph is
   * well-formed.
   *
   * @return List of instructions. Never null
   */
  public MetaCircuit toMetaCircuit() {
    if (!correctlyOrdered()) {
      throw new RuntimeException(
          "Invalid Bristol Circuit: Some wires were used before they were defined!");
    }

    // Initialize instructions
    int inputWireVarIdx = 0;
    int inputVariableIdx = (int) Math.pow(10, Math.ceil(Math.log10(numInputWires())));
    int variableIdx = 2 * inputVariableIdx;

    final var instructions = new ArrayList<Instruction>();

    // Initialize input variables
    final List<Block.Input> inputs = new ArrayList<>();
    final var wireNumToVariable = new HashMap<Integer, VariableId>();
    for (final int inputSize : inputSizes()) {
      final var actualInputVar = new VariableId(inputVariableIdx++);
      final var inputType = new Type.SecretBinaryInteger(inputSize);
      inputs.add(new Block.Input(actualInputVar, inputType));
      for (int bitIdx = 0; bitIdx < inputSize; bitIdx++) {

        final var tempVarShiftAmount = new VariableId(variableIdx++);
        final var tempVarShifted = new VariableId(variableIdx++);
        final var tempVarCast = new VariableId(inputWireVarIdx++);

        instructions.add(
            new Instruction(
                tempVarShiftAmount,
                new Type.PublicInteger(inputSize),
                new Operation.Constant(new Type.PublicInteger(inputSize), bitIdx)));
        instructions.add(
            new Instruction(
                tempVarShifted,
                inputType,
                new Operation.Binary(
                    Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL,
                    actualInputVar,
                    tempVarShiftAmount)));
        instructions.add(
            new Instruction(tempVarCast, Type.Sbit, new Operation.Cast(Type.Sbit, tempVarShifted)));

        wireNumToVariable.put(tempVarCast.variableId(), tempVarCast);
      }
    }

    // Construct instructions
    for (final Line line : lines()) {
      final var outputVariable = new VariableId(variableIdx++);

      final Operation operation;
      switch (line.operation()) {
        case "EQ":
          operation = new Operation.Constant(Type.Sbit, line.inputWires.get(0));
          break;
        case "INV":
          operation =
              new Operation.Unary(
                  Operation.UnaryOp.BITWISE_NOT, wireNumToVariable.get(line.inputWires.get(0)));
          break;
        case "NOT":
          operation =
              new Operation.Unary(
                  Operation.UnaryOp.BITWISE_NOT, wireNumToVariable.get(line.inputWires.get(0)));
          break;
        case "XOR":
          operation =
              new Operation.Binary(
                  Operation.BinaryOp.BITWISE_XOR,
                  wireNumToVariable.get(line.inputWires.get(0)),
                  wireNumToVariable.get(line.inputWires.get(1)));
          break;
        case "AND":
          operation =
              new Operation.Binary(
                  Operation.BinaryOp.BITWISE_AND,
                  wireNumToVariable.get(line.inputWires.get(0)),
                  wireNumToVariable.get(line.inputWires.get(1)));
          break;
        default:
          throw new UnsupportedOperationException(
              "Invalid Bristol Circuit: Unknown operation: " + line.operation());
      }

      final Instruction insn = new Instruction(outputVariable, Type.Sbit, operation);

      // Bookkeeping
      for (final int outputWire : line.outputWires()) {
        wireNumToVariable.put(outputWire, outputVariable);
      }
      instructions.add(insn);
    }

    // Construct outputs
    final List<Type> outputSignature = new ArrayList<>();
    final List<VariableId> outputVariables = new ArrayList<>();
    int outputWire = firstOutputWire();
    for (final int outputSize : outputSizes()) {
      final var outputType = new Type.SecretBinaryInteger(outputSize);
      outputSignature.add(outputType);

      var tempCombined = new VariableId(variableIdx++);
      instructions.add(
          new Instruction(tempCombined, outputType, new Operation.Constant(outputType, 0)));

      for (int bitIdx = 0; bitIdx < outputSize; bitIdx++) {

        final var tempVarWire = wireNumToVariable.get(outputWire++);
        final var tempVarCast = new VariableId(variableIdx++);
        final var tempVarShiftAmount = new VariableId(variableIdx++);
        final var tempVarShifted = new VariableId(variableIdx++);
        final var tempVarOr = new VariableId(variableIdx++);

        instructions.add(
            new Instruction(tempVarCast, outputType, new Operation.Cast(Type.Sbit, tempVarWire)));
        instructions.add(
            new Instruction(
                tempVarShiftAmount,
                new Type.PublicInteger(outputSize),
                new Operation.Constant(new Type.PublicInteger(outputSize), bitIdx)));
        instructions.add(
            new Instruction(
                tempVarShifted,
                outputType,
                new Operation.Binary(
                    Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL, tempVarCast, tempVarShiftAmount)));
        instructions.add(
            new Instruction(
                tempVarOr,
                outputType,
                new Operation.Binary(Operation.BinaryOp.BITWISE_OR, tempCombined, tempVarShifted)));

        wireNumToVariable.put(tempVarCast.variableId(), tempVarCast);
        tempCombined = tempVarOr;
      }

      outputVariables.add(tempCombined);
    }

    return new MetaCircuit(
        List.of(
            new MetaCircuit.Function(
                new FunctionId(0),
                List.of(
                    new Block(
                        new BlockId(0),
                        List.copyOf(inputs),
                        List.copyOf(instructions),
                        new Terminator.BranchAlways(
                            new Terminator.BranchInfo(BlockId.RETURN, outputVariables)))),
                outputSignature)));
  }

  private static <T> List<T> iota(final int numElements, Function<Integer, T> elementSupplier) {
    final var ls = new ArrayList<T>(numElements);
    for (int i = 0; i < numElements; i++) {
      ls.add(elementSupplier.apply(i));
    }
    return List.copyOf(ls);
  }
}
