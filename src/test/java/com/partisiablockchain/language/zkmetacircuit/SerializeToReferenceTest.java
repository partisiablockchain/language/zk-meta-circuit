package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.stream.Stream;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class SerializeToReferenceTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesValid")
  public void testSerializeCircuits(final ExampleCircuits.TestCase<MetaCircuit> testCase) {

    // Test paths
    final Path bytecodeFilepath =
        ReferenceTestUtility.getReferenceDirectoryPath(SerializeToReferenceTest.class)
            .resolve(testCase.filename() + ".zkbc");

    final MetaCircuit.Normalized normalizedCircuit =
        MetaCircuitNormalizer.normalizeMetaCircuit(testCase.data());

    final byte[] serializedBytecode =
        ExceptionConverter.call(
            () ->
                SafeDataOutputStream.serialize(
                    stream ->
                        MetaCircuitSerializer.serializeMetaCircuitToStream(
                            stream, normalizedCircuit)),
            "Could not serialize serialized circuit: " + testCase.filename());
    assertThat(serializedBytecode).describedAs(testCase.filename()).isNotNull();

    final String hexdumpBytecode = hexFormat(serializedBytecode).strip();

    if (!bytecodeFilepath.toFile().exists()) {
      ReferenceTestUtility.writeText(bytecodeFilepath, hexdumpBytecode);
      throw new RuntimeException("Regenerated %s. Rerun tests.".formatted(bytecodeFilepath));
    }

    // Validate compiled metaCircuit is as expected
    final String hexdumpBytecodeExpected = ReferenceTestUtility.loadText(bytecodeFilepath).strip();
    assertThat(hexdumpBytecode)
        .describedAs(bytecodeFilepath.toString())
        .isEqualToNormalizingNewlines(hexdumpBytecodeExpected);
  }

  //// Test providers

  static Stream<Arguments> provideTestCasesValid() {
    return Stream.concat(
        ExampleCircuits.VALID_STABLE.stream().map(x -> Arguments.of(x, true)),
        ExampleCircuits.VALID_UNSTABLE.stream().map(x -> Arguments.of(x, false)));
  }

  //// Utility

  private static String hexFormat(final byte[] bytes) {
    final List<String> bytesAsString =
        java.util.regex.Pattern.compile("..")
            .matcher(Hex.toHexString(bytes))
            .results()
            .map(MatchResult::group)
            .toList();
    final StringBuilder builder = new StringBuilder();
    for (int idx = 0; idx < bytesAsString.size(); idx++) {
      if (idx % 16 == 0 && idx != 0) {
        builder.append("\n");
      } else if (idx % 2 == 0 && idx != 0) {
        builder.append(" ");
      }
      builder.append(bytesAsString.get(idx));
    }

    return builder.toString();
  }
}
