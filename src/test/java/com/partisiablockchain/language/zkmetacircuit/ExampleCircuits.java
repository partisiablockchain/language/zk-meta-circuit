package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;
import org.bouncycastle.util.encoders.Hex;

/**
 * Contains a bunch of example circuits.
 *
 * <p>Note the following terminology:
 *
 * <ul>
 *   <li><b>Malformed</b> Bytecode is not a serialization of any circuit.
 *   <li><b>Invalid</b> Circuit is a circuit that cannot be validated.
 *   <li><b>Valid</b> Circuit is a circuit that can be validated.
 * </ul>
 */
final class ExampleCircuits {

  private ExampleCircuits() {}

  public static final class TestCase<T> {
    private final String filename;
    private final Function<String, T> dataLoader;
    private T data = null;

    TestCase(final String filename, final Function<String, T> dataLoader) {
      this.filename = filename;
      this.dataLoader = dataLoader;
    }

    public String filename() {
      return filename;
    }

    public T data() {
      if (data == null) {
        data = dataLoader.apply(filename);
      }
      return data;
    }

    @Override
    public String toString() {
      return "TestCase(%s)".formatted(filename);
    }
  }

  //// MetaCircuits

  static final List<TestCase<MetaCircuit>> VALID_STABLE =
      filesInDirectory("mc_valid_stable").stream()
          .map(filename -> new TestCase<MetaCircuit>(filename, ExampleCircuits::loadCircuitCase))
          .toList();

  static final List<TestCase<MetaCircuit>> VALID_UNSTABLE =
      Stream.concat(
              filesInDirectory("mc_valid_unstable").stream()
                  .map(
                      filename ->
                          new TestCase<MetaCircuit>(filename, ExampleCircuits::loadCircuitCase)),
              filesInDirectory("bristol_fashion").stream()
                  .map(
                      filename ->
                          new TestCase<MetaCircuit>(filename, ExampleCircuits::loadBristolFile)))
          .toList();

  static final List<TestCase<MetaCircuit>> INVALID =
      filesInDirectory("mc_invalid").stream()
          .map(filename -> new TestCase<>(filename, ExampleCircuits::loadCircuitCase))
          .toList();

  static final List<TestCase<String>> MALFORMED_PRETTY_CIRCUITS =
      filesInDirectory("mc_pretty_malformed").stream()
          .map(filename -> new TestCase<>(filename, ExampleCircuits::loadMalformedPrettyCircuit))
          .toList();

  //// Malformed MetaCircuits bytes

  /** Malformed as it contains no data. */
  public static final byte[] MALFORMED_BYTECODE_EMPTY = new byte[0];

  public static final byte[] MALFORMED_BYTECODE_UNKNOWN_INSN =
      Hex.decode(
          "5a4b 4d43" // Magic number
              + "0001 0000" // Version 1.0
              + "0000 0000" // Num outputs
              + "0000 0001" // Num blocks: 1
              + "0000 0000" // Num inputs
              + "0000 0001" // Num instructions
              + "0101 0000" // Insn: Unknown
              + "FFFF FFFF FFFF FFFF 0000 0000" // Branch to end
          );

  public static final byte[] MALFORMED_BYTECODE_UNKNOWN_INSN_NULOP =
      Hex.decode(
          "5a4b 4d43" // Magic number
              + "0001 0000" // Version 1.0
              + "0000 0000" // Num outputs
              + "0000 0001" // Num blocks: 1
              + "0000 0000" // Num inputs
              + "0000 0001" // Num instructions
              + "0101 001d 0000 0000" // Insn: Unknown nullary op
              + "FFFF FFFF FFFF FFFF 0000 0000" // Branch to end
          );

  public static final byte[] MALFORMED_BYTECODE_UNKNOWN_TYPE =
      Hex.decode(
          "5a4b 4d43" // Magic number
              + "0001 0000" // Version 1.0
              + "0000 0000" // Num outputs
              + "0000 0001" // Num blocks
              + "0000 0001" // Num inputs
              + "FFFF" // Input 0: ???
              + "0000 0000" // Instruction count
              + "FFFF FFFF FFFF FFFF 0000 0000" // Branch to end
          );

  public static final byte[] MALFORMED_BYTECODE_UNKNOWN_VERSION_MAJOR =
      Hex.decode(
          "5a4b 4d43" // Magic number
              + "00FF 0000" // Version 255.0
          );

  public static final byte[] MALFORMED_BYTECODE_UNKNOWN_VERSION_MINOR =
      Hex.decode(
          "5a4b 4d43" // Magic number
              + "0001 00FF" // Version 1.255
          );

  public static final byte[] MALFORMED_BYTECODE_UNKNOWN_VERSION_MINOR_WEIRD =
      Hex.decode(
          "5a4b 4d43" // Magic number
              + "0001 FFFF" // Version 1.-1
          );

  /** Produces a list of byte codes with malformed magic header. */
  public static List<byte[]> bytecodesWithMalformedMagic() {
    return VALID_STABLE.stream()
        .map(
            testCase -> {
              final MetaCircuit.Normalized normalizedCircuit =
                  MetaCircuitNormalizer.normalizeMetaCircuit(testCase.data());
              final byte[] serialized =
                  SafeDataOutputStream.serialize(
                      stream ->
                          MetaCircuitSerializer.serializeMetaCircuitToStream(
                              stream, normalizedCircuit));
              serialized[3] = 'A';
              return serialized;
            })
        .toList();
  }

  public static List<byte[]> malformedBytecodes() {
    final var bytecodes =
        new ArrayList<byte[]>(
            List.of(
                MALFORMED_BYTECODE_EMPTY,
                MALFORMED_BYTECODE_UNKNOWN_INSN,
                MALFORMED_BYTECODE_UNKNOWN_INSN_NULOP,
                MALFORMED_BYTECODE_UNKNOWN_TYPE,
                MALFORMED_BYTECODE_UNKNOWN_VERSION_MAJOR,
                MALFORMED_BYTECODE_UNKNOWN_VERSION_MINOR,
                MALFORMED_BYTECODE_UNKNOWN_VERSION_MINOR_WEIRD));

    // Take all valid circuits, serialized, and corrupt magic header.
    bytecodes.addAll(bytecodesWithMalformedMagic());

    return List.copyOf(bytecodes);
  }

  public static MetaCircuit loadBristolFile(final String filename) {

    System.out.println("Loading " + filename);
    System.out.flush();
    final byte[] fileBytes =
        WithResource.apply(
            () -> ExampleCircuits.class.getResourceAsStream(filename),
            InputStream::readAllBytes,
            "Could not load Bristol Fashion file: " + filename);

    System.out.println("     Loaded " + fileBytes.length + " bytes");
    System.out.println("     Parsing");
    System.out.flush();
    final var parsedGraph =
        BristolFashionGraph.parseFrom(new String(fileBytes, StandardCharsets.UTF_8));

    System.out.println("     Transforming");
    System.out.flush();
    final MetaCircuit circuit = parsedGraph.toMetaCircuit();

    System.out.println("     Done");
    System.out.flush();

    return circuit;
  }

  // Program loading

  private static List<String> filesInDirectory(final String directoryName) {
    final File directory = new File(ExampleCircuits.class.getResource(directoryName).getPath());
    final int prefixLength = directory.getParentFile().getAbsolutePath().length() + 1;
    return java.util.Arrays.stream(directory.listFiles())
        .map(File::getAbsolutePath)
        .filter(filename -> filename.endsWith(".mc"))
        .map(filename -> filename.substring(prefixLength))
        .toList();
  }

  private static String loadProgramText(final String filename) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> ExampleCircuits.class.getResourceAsStream(filename),
            InputStream::readAllBytes,
            "Could not load file: " + filename);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }

  public static MetaCircuit loadCircuitCase(final String filename) {
    final String programText = loadProgramText(filename);
    return ExceptionConverter.call(() -> MetaCircuitPrettyParser.parse(programText), filename);
  }

  private static String loadMalformedPrettyCircuit(final String filename) {
    return loadProgramText(filename);
  }
}
