package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class BristolFashionGraphConversionTest {

  static final BristolFashionGraph GOOD_CIRCUIT =
      new BristolFashionGraph(
          List.of(
              new BristolFashionGraph.Line("EQ", List.of(0), List.of(1)),
              new BristolFashionGraph.Line("EQ", List.of(1), List.of(2)),
              new BristolFashionGraph.Line("INV", List.of(2), List.of(3)),
              new BristolFashionGraph.Line("NOT", List.of(3), List.of(4)),
              new BristolFashionGraph.Line("XOR", List.of(4, 2), List.of(5)),
              new BristolFashionGraph.Line("AND", List.of(2, 4), List.of(6))),
          List.of(),
          List.of(),
          0);

  static final BristolFashionGraph BAD_CIRCUIT_UNKNOWN_OPERATION =
      new BristolFashionGraph(
          List.of(new BristolFashionGraph.Line("SOME_UNKNOWN_OPERATION", List.of(0), List.of(1))),
          List.of(1),
          List.of(1),
          1);

  @Test
  public void toMetaCircuit() {
    final List<Instruction> insns =
        GOOD_CIRCUIT.toMetaCircuit().functions().get(0).blocks().get(0).instructions();

    assertThat(insns)
        .containsExactly(
            new Instruction(new VariableId(0), Type.Sbit, new Operation.Constant(Type.Sbit, 0)),
            new Instruction(new VariableId(1), Type.Sbit, new Operation.Constant(Type.Sbit, 1)),
            new Instruction(
                new VariableId(2),
                Type.Sbit,
                new Operation.Unary(Operation.UnaryOp.BITWISE_NOT, new VariableId(1))),
            new Instruction(
                new VariableId(3),
                Type.Sbit,
                new Operation.Unary(Operation.UnaryOp.BITWISE_NOT, new VariableId(2))),
            new Instruction(
                new VariableId(4),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITWISE_XOR, new VariableId(3), new VariableId(1))),
            new Instruction(
                new VariableId(5),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITWISE_AND, new VariableId(1), new VariableId(3))));
  }

  @Test
  public void unknownInstruction() {
    assertThatThrownBy(() -> BAD_CIRCUIT_UNKNOWN_OPERATION.toMetaCircuit())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid Bristol Circuit: Unknown operation: SOME_UNKNOWN_OPERATION");
  }

  @Test
  public void unknownInputs() {
    final var inputSizes = List.of(4, 5);

    final var unknownWires = List.of(9, 10, 100, -1, -100);

    for (final int unknownWire : unknownWires) {

      final BristolFashionGraph circuit =
          new BristolFashionGraph(
              List.of(new BristolFashionGraph.Line("NOT", List.of(unknownWire), List.of(9))),
              inputSizes,
              List.of(1),
              9);

      Assertions.assertThatExceptionOfType(RuntimeException.class)
          .describedAs("Expected wire %d to be unknown", unknownWire)
          .isThrownBy(() -> circuit.toMetaCircuit())
          .withMessage("Invalid Bristol Circuit: Some wires were used before they were defined!");
    }
  }

  @Test
  public void knownConstants() {
    for (final int knownWire : List.of(0, 1)) {

      final BristolFashionGraph circuit =
          new BristolFashionGraph(
              List.of(new BristolFashionGraph.Line("EQ", List.of(knownWire), List.of(0))),
              List.of(0),
              List.of(1),
              0);

      assertThat(circuit.toMetaCircuit().functions().get(0).blocks()).hasSize(1);
    }
  }

  @Test
  public void inputProduction() {
    final BristolFashionGraph circuit =
        new BristolFashionGraph(List.of(), List.of(1, 1), List.of(), 0);

    final MetaCircuit metaCircuit = circuit.toMetaCircuit();
    assertThat(metaCircuit.functions().get(0).blocks()).hasSize(1);

    assertThat(metaCircuit.functions().get(0).blocks().get(0).instructions())
        .containsExactly(
            new Instruction(
                new VariableId(20), Type.PublicBool, new Operation.Constant(Type.PublicBool, 0)),
            new Instruction(
                new VariableId(21),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL,
                    new VariableId(10),
                    new VariableId(20))),
            new Instruction(
                new VariableId(0), Type.Sbit, new Operation.Cast(Type.Sbit, new VariableId(21))),
            new Instruction(
                new VariableId(22), Type.PublicBool, new Operation.Constant(Type.PublicBool, 0)),
            new Instruction(
                new VariableId(23),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL,
                    new VariableId(11),
                    new VariableId(22))),
            new Instruction(
                new VariableId(1), Type.Sbit, new Operation.Cast(Type.Sbit, new VariableId(23))));

    assertThat(metaCircuit.functions().get(0).blocks().get(0).inputs())
        .containsExactly(
            new Block.Input(new VariableId(10), Type.Sbit),
            new Block.Input(new VariableId(11), Type.Sbit));
  }

  @Test
  public void outputProduction() {
    final BristolFashionGraph circuit =
        new BristolFashionGraph(
            List.of(
                new BristolFashionGraph.Line("EQ", List.of(0), List.of(1)),
                new BristolFashionGraph.Line("EQ", List.of(1), List.of(2))),
            List.of(),
            List.of(1, 1),
            1);

    final MetaCircuit metaCircuit = circuit.toMetaCircuit();
    assertThat(metaCircuit.functions().get(0).blocks()).hasSize(1);

    assertThat(metaCircuit.functions().get(0).blocks().get(0).instructions())
        .containsExactly(
            new Instruction(
                new VariableId(0),
                new Type.SecretBinaryInteger(1),
                new Operation.Constant(Type.Sbit, 0)),
            new Instruction(
                new VariableId(1),
                new Type.SecretBinaryInteger(1),
                new Operation.Constant(Type.Sbit, 1)),
            new Instruction(
                new VariableId(2),
                new Type.SecretBinaryInteger(1),
                new Operation.Constant(Type.Sbit, 0)),
            new Instruction(
                new VariableId(3), Type.Sbit, new Operation.Cast(Type.Sbit, new VariableId(0))),
            new Instruction(
                new VariableId(4), Type.PublicBool, new Operation.Constant(Type.PublicBool, 0)),
            new Instruction(
                new VariableId(5),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL,
                    new VariableId(3),
                    new VariableId(4))),
            new Instruction(
                new VariableId(6),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITWISE_OR, new VariableId(2), new VariableId(5))),
            new Instruction(
                new VariableId(7),
                new Type.SecretBinaryInteger(1),
                new Operation.Constant(Type.Sbit, 0)),
            new Instruction(
                new VariableId(8), Type.Sbit, new Operation.Cast(Type.Sbit, new VariableId(1))),
            new Instruction(
                new VariableId(9), Type.PublicBool, new Operation.Constant(Type.PublicBool, 0)),
            new Instruction(
                new VariableId(10),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL,
                    new VariableId(8),
                    new VariableId(9))),
            new Instruction(
                new VariableId(11),
                Type.Sbit,
                new Operation.Binary(
                    Operation.BinaryOp.BITWISE_OR, new VariableId(7), new VariableId(10))));

    assertThat(metaCircuit.functions().get(0).blocks().get(0).inputs()).isEmpty();
    assertThat(metaCircuit.functions().get(0).returnTypes()).containsExactly(Type.Sbit, Type.Sbit);
  }

  @Test
  public void knownInputs() {
    final var inputSizes = List.of(4, 5);

    final var knownWires = List.of(0, 1, 2, 3, 4, 5, 6, 7, 8);

    for (final int knownWire : knownWires) {

      final BristolFashionGraph circuit =
          new BristolFashionGraph(
              List.of(new BristolFashionGraph.Line("NOT", List.of(knownWire), List.of(9))),
              inputSizes,
              List.of(1),
              9);

      assertThat(circuit.toMetaCircuit()).isNotNull();
    }
  }
}
