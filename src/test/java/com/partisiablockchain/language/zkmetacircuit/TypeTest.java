package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

final class TypeTest {

  @Test
  public void badSizes() {
    assertThatThrownBy(() -> new Type.SecretBinaryInteger(-1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid input: Bitwidth -1 not supported. Must be between 0 and 16383");
    assertThatThrownBy(() -> new Type.SecretBinaryInteger(16383 + 1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid input: Bitwidth 16384 not supported. Must be between 0 and 16383");
    assertThatThrownBy(() -> new Type.PublicInteger(-1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid input: Bitwidth -1 not supported. Must be between 0 and 16383");
    assertThatThrownBy(() -> new Type.PublicInteger(16383 + 1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid input: Bitwidth 16384 not supported. Must be between 0 and 16383");
  }
}
