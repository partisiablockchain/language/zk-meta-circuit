package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.HashSet;
import java.util.Map;
import org.junit.jupiter.api.Test;

final class OperationTest {

  @Test
  public void invariants() {
    final var uniqueOpcodes = new HashSet<Integer>();

    for (final Operation.UnaryOp op : Operation.UnaryOp.values()) {
      assertThat(op.opcode() & 0x3).as(op.toString()).isEqualTo(1);
      assertThat(uniqueOpcodes.add(op.opcode())).as(op.toString()).isTrue();
    }
    for (final Operation.BinaryOp op : Operation.BinaryOp.values()) {
      assertThat(op.opcode() & 0x3).as(op.toString()).isEqualTo(2);
      assertThat(uniqueOpcodes.add(op.opcode())).as(op.toString()).isTrue();
    }

    // non-op instructions
    assertThat(Operation.Constant.OPCODE & 0x3).isEqualTo(1);
    assertThat(uniqueOpcodes.add(Operation.Constant.OPCODE)).isTrue();
    assertThat(Operation.Select.OPCODE & 0x3).isEqualTo(3);
    assertThat(uniqueOpcodes.add(Operation.Select.OPCODE)).isTrue();
    assertThat(Operation.Extract.OPCODE & 0x3).isEqualTo(3);
    assertThat(uniqueOpcodes.add(Operation.Extract.OPCODE)).isTrue();
  }

  @Test
  public void valuesByKey() {
    final Map<String, TestEnum> map = Operation.valuesByKey(TestEnum.class, TestEnum::name);
    assertThat(map)
        .containsEntry("ONE", TestEnum.ONE)
        .containsEntry("TWO", TestEnum.TWO)
        .containsEntry("THREE", TestEnum.THREE);
  }

  @Test
  public void badOperands() {
    assertThatThrownBy(() -> new Operation.Extract(new VariableId(1), -1, 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Bitwidth -1 must be non-negative.");
    assertThatThrownBy(() -> new Operation.Extract(new VariableId(1), 1, -1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Offset -1 must be non-negative.");
    assertThatThrownBy(
            () -> new Operation.ExtractDynamically(new VariableId(1), -1, new VariableId(1)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Bitwidth -1 must be non-negative.");
  }

  enum TestEnum {
    ONE,
    TWO,
    THREE
  }
}
