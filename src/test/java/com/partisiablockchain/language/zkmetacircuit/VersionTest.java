package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

final class VersionTest {

  //// General tests

  static final List<Version> VERSIONS =
      List.of(
          new Version((short) -1, (short) 0),
          new Version((short) 1, (short) -1),
          Version.VERSION_1_0,
          Version.VERSION_1_1,
          Version.VERSION_1_2,
          new Version((short) 99, (short) 0));

  @Test
  public void lessThan() {
    for (int idx = 0; idx < VERSIONS.size() - 1; idx++) {
      assertThat(VERSIONS.get(idx).isLessThan(VERSIONS.get(idx + 1))).isTrue();
      assertThat(VERSIONS.get(idx + 1).isLessThan(VERSIONS.get(idx))).isFalse();
    }

    for (final Version version : VERSIONS) {
      assertThat(version.isLessThan(version)).isFalse();
    }
  }

  @Test
  public void joinCommutative() {
    for (final Version version : VERSIONS) {
      for (final Version versionOther : VERSIONS) {
        assertThat(version.join(versionOther))
            .isEqualTo(versionOther.join(version))
            .isIn(version, versionOther);
      }
    }
  }

  @Test
  public void testVersionCompatibilityWithSelf() {
    for (final Version version : VERSIONS) {
      assertThat(version.isBackwardsCompatibleWith(version)).isTrue();
    }
  }

  @Test
  public void compatibilityWithOthers() {
    for (final Version version : VERSIONS) {
      for (final Version versionOther : VERSIONS) {
        if (!version.equals(versionOther)) {
          assertThat(
                  version.isBackwardsCompatibleWith(versionOther)
                      && versionOther.isBackwardsCompatibleWith(version))
              .isFalse();
          if (version.major() == versionOther.major()) {
            assertThat(version.isBackwardsCompatibleWith(versionOther))
                .isNotEqualTo(versionOther.isBackwardsCompatibleWith(version));
          }
        }
      }
    }
  }
}
