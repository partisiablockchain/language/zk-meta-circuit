package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.language.immutabledetect.ImmutableDetect;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.stream.Stream;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class SerializeTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesValid")
  public void testSerializeCircuits(final ExampleCircuits.TestCase<MetaCircuit> testCase) {

    final MetaCircuit.Normalized normalizedCircuit =
        MetaCircuitNormalizer.normalizeMetaCircuit(testCase.data());
    final byte[] serialized =
        ExceptionConverter.call(
            () ->
                SafeDataOutputStream.serialize(
                    stream ->
                        MetaCircuitSerializer.serializeMetaCircuitToStream(
                            stream, normalizedCircuit)),
            "Could not serialize serialized circuit: " + testCase.filename());
    assertThat(serialized).describedAs(testCase.filename()).isNotNull();
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesValid")
  public void testDeserializeCircuits(
      final ExampleCircuits.TestCase<MetaCircuit> testCase, final boolean expectedStable) {
    ExceptionConverter.call(
        () -> {
          MetaCircuitValidator.validateMetaCircuit(testCase.data());
          return 0;
        },
        "Expected valid meta-circuit for test case: " + testCase.filename());

    final MetaCircuit.Normalized normalizedCircuit =
        MetaCircuitNormalizer.normalizeMetaCircuit(testCase.data());

    final byte[] serialized =
        ExceptionConverter.call(
            () ->
                SafeDataOutputStream.serialize(
                    stream ->
                        MetaCircuitSerializer.serializeMetaCircuitToStream(
                            stream, normalizedCircuit)),
            "Could not serialize serialized circuit: " + testCase.filename());

    final MetaCircuit.Normalized deserializedCircuit =
        ExceptionConverter.call(
            () ->
                SafeDataInputStream.deserialize(
                    MetaCircuitSerializer::deserializeMetaCircuitFromStream, serialized),
            "Could not deserialize serialized circuit: "
                + testCase.filename()
                + "\n"
                + hexFormat(serialized));

    assertThat(deserializedCircuit)
        .matches(ImmutableDetect::isImmutable, "is immutable")
        .extracting("metaCircuit")
        .matches(ImmutableDetect::isImmutable, "is immutable");

    if (expectedStable) {
      if (!deserializedCircuit.metaCircuit().equals(testCase.data())) {
        System.out.println("# Unstable circuit");
        System.out.println(MetaCircuitPretty.pretty(deserializedCircuit.metaCircuit()));
      }
      assertThat(deserializedCircuit.metaCircuit())
          .describedAs(testCase.filename())
          .isEqualTo(testCase.data());
    }

    // Check that deserialized circuit is still valid
    MetaCircuitValidator.validateMetaCircuit(deserializedCircuit.metaCircuit());

    final byte[] reserialized =
        SafeDataOutputStream.serialize(
            stream ->
                MetaCircuitSerializer.serializeMetaCircuitToStream(stream, deserializedCircuit));

    // Serialize -> deserialize is not stable, so repeat serialization is
    assertThat(serialized).isEqualTo(reserialized);

    final MetaCircuit reDeserializedCircuit =
        ExceptionConverter.call(
                () ->
                    SafeDataInputStream.deserialize(
                        MetaCircuitSerializer::deserializeMetaCircuitFromStream, reserialized),
                "Could not deserialize reserialized circuit: "
                    + testCase.filename()
                    + "\n"
                    + hexFormat(reserialized))
            .metaCircuit();

    // Serialize -> deserialize is not stable, is after repeated
    assertThat(reDeserializedCircuit)
        .describedAs(testCase.filename())
        .isEqualTo(deserializedCircuit.metaCircuit());
  }

  @ParameterizedTest
  @MethodSource("provideBytecodeMalformed")
  public void deserializeInvalidByteFormat(final byte[] byteCode) {
    assertThatCode(
            () ->
                SafeDataInputStream.deserialize(
                    MetaCircuitSerializer::deserializeMetaCircuitFromStream, byteCode))
        .as("Bytecode: " + hexFormat(byteCode))
        .isInstanceOf(RuntimeException.class);
  }

  @ParameterizedTest
  @MethodSource("provideBytecodeMalformedMagic")
  public void deserializeInvalidMagicHeader(final byte[] byteCode) {
    assertThatCode(
            () ->
                SafeDataInputStream.deserialize(
                    MetaCircuitSerializer::deserializeMetaCircuitFromStream, byteCode))
        .as("malformed magic")
        .hasMessageContaining("Malformed Bytecode")
        .hasMessageContaining("Unknown magic bytes");
  }

  @Test
  public void deserializeInvalidByteFormatSpecificErrors() {
    assertThatCode(
            () ->
                SafeDataInputStream.deserialize(
                    MetaCircuitSerializer::deserializeMetaCircuitFromStream,
                    ExampleCircuits.MALFORMED_BYTECODE_UNKNOWN_INSN))
        .as("unknown insn")
        .hasMessageContaining("Malformed Bytecode")
        .hasMessageContaining("Unknown opcode")
        .hasMessageContaining("0x00000000");

    assertThatCode(
            () ->
                SafeDataInputStream.deserialize(
                    MetaCircuitSerializer::deserializeMetaCircuitFromStream,
                    ExampleCircuits.MALFORMED_BYTECODE_UNKNOWN_TYPE))
        .as("malformed type")
        .hasMessageContaining("Malformed Bytecode")
        .hasMessageContaining("Unknown type prefix 0xff");

    assertThatCode(
            () ->
                SafeDataInputStream.deserialize(
                    MetaCircuitSerializer::deserializeMetaCircuitFromStream,
                    ExampleCircuits.MALFORMED_BYTECODE_UNKNOWN_VERSION_MAJOR))
        .hasMessageContaining("Bytecode version newer than supported versions")
        .hasMessageContaining("v1.0 - v1.8")
        .hasMessageContaining("v255.0");
  }

  //// Test providers

  static Stream<Arguments> provideBytecodeMalformed() {
    return ExampleCircuits.malformedBytecodes().stream().map(Arguments::of);
  }

  static Stream<Arguments> provideBytecodeMalformedMagic() {
    return ExampleCircuits.bytecodesWithMalformedMagic().stream().map(Arguments::of);
  }

  static Stream<Arguments> provideTestCasesValid() {
    return Stream.concat(
        ExampleCircuits.VALID_STABLE.stream().map(x -> Arguments.of(x, true)),
        ExampleCircuits.VALID_UNSTABLE.stream().map(x -> Arguments.of(x, false)));
  }

  //// Utility

  private static String hexFormat(final byte[] bytes) {
    return java.util.regex.Pattern.compile("....")
        .matcher(Hex.toHexString(bytes))
        .replaceAll(x -> x.group() + " ");
  }
}
