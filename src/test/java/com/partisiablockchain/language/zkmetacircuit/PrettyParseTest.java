package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.immutabledetect.ImmutableDetect;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class PrettyParseTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesWellformed")
  public void testPrettyRoundtrip(final ExampleCircuits.TestCase<MetaCircuit> testCase) {
    final String prettyCircuit = MetaCircuitPretty.pretty(testCase.data());
    System.out.println(prettyCircuit);
    final MetaCircuit parsedCircuit =
        ExceptionConverter.call(
            () -> MetaCircuitPrettyParser.parse(prettyCircuit),
            "Failed parsing: " + testCase.filename());

    assertThat(parsedCircuit)
        .describedAs(testCase.filename())
        .isEqualTo(testCase.data())
        .matches(ImmutableDetect::isImmutable, "is immutable");
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesMalformed")
  public void avoidParsingMalformed(ExampleCircuits.TestCase<String> testCase) {
    assertThatCode(() -> MetaCircuitPrettyParser.parse(testCase.data()))
        .as(testCase.filename())
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessageContaining("Invalid input");
  }

  static Stream<Arguments> provideTestCasesMalformed() {
    return ExampleCircuits.MALFORMED_PRETTY_CIRCUITS.stream().map(Arguments::of);
  }

  static Stream<Arguments> provideTestCasesWellformed() {
    return Stream.concat(
            ExampleCircuits.VALID_STABLE.stream(),
            Stream.concat(
                ExampleCircuits.VALID_UNSTABLE.stream(), ExampleCircuits.INVALID.stream()))
        .map(Arguments::of);
  }

  @Test
  public void validPrograms() {
    assertThat(
            MetaCircuitPrettyParser.parse(
                "(metacircuit (function %0 (block #0 (inputs (sbi32 $5) (i32 $7)))))"))
        .isNotNull();
  }

  @Test
  public void malformedPrograms() {
    assertThatThrownBy(() -> MetaCircuitPrettyParser.parse("(metacircuit (function %0 (outputs)))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Expected word \"block\", found \"outputs\"");

    assertThatThrownBy(
            () -> MetaCircuitPrettyParser.parse("(metacircuit (function %0 (inputs ())))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Expected word \"block\", found \"inputs\"");

    assertThatThrownBy(
            () -> MetaCircuitPrettyParser.parse("(metacircuit (function %0 (inputs (i32))))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Expected word \"block\", found \"inputs\"");

    assertThatThrownBy(
            () ->
                MetaCircuitPrettyParser.parse("(metacircuit (function %0 (inputs (unknown $1))))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Expected word \"block\", found \"inputs\"");

    assertThatThrownBy(
            () ->
                MetaCircuitPrettyParser.parse("(metacircuit (function %0 (inputs (i1 unknown))))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Expected word \"block\", found \"inputs\"");

    assertThatThrownBy(
            () -> MetaCircuitPrettyParser.parse("(metacircuit (function %0 (inputs (() $1))))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Expected word \"block\", found \"inputs\"");

    assertThatThrownBy(
            () -> MetaCircuitPrettyParser.parse("(metacircuit (function %0 (inputs unknown)))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Expected word \"block\", found \"inputs\"");

    assertThatThrownBy(
            () ->
                MetaCircuitPrettyParser.parse(
                    "(metacircuit (function %0 (output) (block #0 (i1 $0 (UNKNOWN)))))"))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(NullPointerException.class)
        .hasMessage("Invalid input: Unknown operation: UNKNOWN");
  }
}
