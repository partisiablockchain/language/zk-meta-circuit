package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatCode;

import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Path;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class ValidationTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesValid")
  public void validateValid(final ExampleCircuits.TestCase<MetaCircuit> testCase) {
    ExceptionConverter.call(
        () -> {
          MetaCircuitValidator.validateMetaCircuit(testCase.data());
          return 0;
        },
        testCase.filename());
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesInvalid")
  public void validateInvalidCircuits(final ExampleCircuits.TestCase<MetaCircuit> testCase) {
    assertThatCode(() -> MetaCircuitValidator.validateMetaCircuit(testCase.data()))
        .as(testCase.filename())
        .isInstanceOf(MetaCircuitValidationException.class)
        .hasMessageStartingWith("Meta circuit validation failed:")
        .hasNoCause();
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesInvalid")
  public void validateInvalidCircuitsToReference(
      final ExampleCircuits.TestCase<MetaCircuit> testCase) {
    final Path errorMessageFilepath =
        ReferenceTestUtility.getReferenceDirectoryPath(ValidationTest.class)
            .resolve(testCase.filename() + ".err");

    final var validationErrors = MetaCircuitValidator.validateMetaCircuitNoThrow(testCase.data());
    Assertions.assertThat(validationErrors).as(testCase.filename()).isNotEmpty();

    final String formatted = MetaCircuitValidator.formatValidationErrors(validationErrors);
    Assertions.assertThat(formatted).startsWith("Meta circuit validation failed:");

    if (!errorMessageFilepath.toFile().exists()) {
      ReferenceTestUtility.writeText(errorMessageFilepath, formatted);
      Assertions.fail("Regenerated %s. Rerun tests.".formatted(errorMessageFilepath));
    }

    // Validate compiled metaCircuit is as expected
    final String formattedExpected = ReferenceTestUtility.loadText(errorMessageFilepath);
    Assertions.assertThat(formatted)
        .describedAs(errorMessageFilepath.toString())
        .isEqualToNormalizingNewlines(formattedExpected);
  }

  public static Stream<Arguments> provideTestCasesValid() {
    return Stream.concat(
            ExampleCircuits.VALID_UNSTABLE.stream(), ExampleCircuits.VALID_STABLE.stream())
        .map(Arguments::of);
  }

  public static Stream<Arguments> provideTestCasesInvalid() {
    return ExampleCircuits.INVALID.stream().map(Arguments::of);
  }
}
