package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class PrettyTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesSerializable")
  public void testPrettyCircuits(final ExampleCircuits.TestCase<MetaCircuit> testCase) {
    assertThat(MetaCircuitPretty.pretty(testCase.data()))
        .isNotNull()
        .startsWith("(metacircuit")
        .doesNotContain("null")
        .doesNotContain("UNKNOWN")
        .doesNotContain("unknown")
        .doesNotContain(" \n")
        .doesNotContain("\t")
        .doesNotContainPattern("\\S  +\\S");
  }

  static Stream<Arguments> provideTestCasesSerializable() {
    return Stream.of(
            ExampleCircuits.VALID_STABLE, ExampleCircuits.VALID_UNSTABLE, ExampleCircuits.INVALID)
        .flatMap(List::stream)
        .map(Arguments::of);
  }

  //// Specific circuits

  private static final MetaCircuit VALID_1 =
      new MetaCircuit(
          List.of(
              new MetaCircuit.Function(
                  new FunctionId(0),
                  List.of(
                      new Block(
                          new BlockId(99),
                          List.of(new Block.Input(new VariableId(0), Type.Sbit)),
                          List.of(
                              new Instruction(
                                  new VariableId(1),
                                  Type.Sbit,
                                  new Operation.Constant(Type.Sbit, 1)),
                              new Instruction(
                                  new VariableId(2),
                                  Type.Sbit,
                                  new Operation.Binary(
                                      Operation.BinaryOp.BITWISE_XOR,
                                      new VariableId(0),
                                      new VariableId(1)))),
                          new Terminator.BranchAlways(
                              new Terminator.BranchInfo(BlockId.RETURN, List.of())))),
                  List.of())));

  private static final MetaCircuit VALID_2 =
      new MetaCircuit(
          List.of(
              new MetaCircuit.Function(
                  new FunctionId(0),
                  List.of(
                      new Block(
                          new BlockId(0),
                          List.of(),
                          List.of(
                              new Instruction(
                                  new VariableId(0),
                                  Type.Sbit,
                                  new Operation.Constant(Type.Sbit, 0)),
                              new Instruction(
                                  new VariableId(1),
                                  Type.Sbit,
                                  new Operation.Binary(
                                      Operation.BinaryOp.BITWISE_AND,
                                      new VariableId(0),
                                      new VariableId(0))),
                              new Instruction(
                                  new VariableId(2),
                                  Type.Sbit,
                                  new Operation.Binary(
                                      Operation.BinaryOp.BITWISE_XOR,
                                      new VariableId(1),
                                      new VariableId(1)))),
                          new Terminator.BranchAlways(
                              new Terminator.BranchInfo(new BlockId(1), List.of()))),
                      new Block(
                          new BlockId(1),
                          List.of(),
                          List.of(
                              new Instruction(
                                  new VariableId(0),
                                  Type.Sbit,
                                  new Operation.Constant(Type.Sbit, 1)),
                              new Instruction(
                                  new VariableId(1),
                                  Type.Sbit,
                                  new Operation.Binary(
                                      Operation.BinaryOp.BITWISE_AND,
                                      new VariableId(0),
                                      new VariableId(0))),
                              new Instruction(
                                  new VariableId(2),
                                  Type.Sbit,
                                  new Operation.Binary(
                                      Operation.BinaryOp.BITWISE_XOR,
                                      new VariableId(1),
                                      new VariableId(1)))),
                          new Terminator.BranchAlways(
                              new Terminator.BranchInfo(BlockId.RETURN, List.of())))),
                  List.of())));

  private static final MetaCircuit VALID_5_AND =
      new MetaCircuit(
          List.of(
              new MetaCircuit.Function(
                  new FunctionId(0),
                  List.of(
                      new Block(
                          new BlockId(0),
                          List.of(
                              new Block.Input(new VariableId(0), new Type.SecretBinaryInteger(64)),
                              new Block.Input(new VariableId(1), new Type.SecretBinaryInteger(64))),
                          List.of(
                              new Instruction(
                                  new VariableId(2),
                                  new Type.SecretBinaryInteger(64),
                                  new Operation.Binary(
                                      Operation.BinaryOp.BITWISE_AND,
                                      new VariableId(0),
                                      new VariableId(1)))),
                          new Terminator.BranchAlways(
                              new Terminator.BranchInfo(BlockId.RETURN, List.of())))),
                  List.of(new Type.SecretBinaryInteger(64)))));

  private static final MetaCircuit VALID_6_SELECT =
      new MetaCircuit(
          List.of(
              new MetaCircuit.Function(
                  new FunctionId(0),
                  List.of(
                      new Block(
                          new BlockId(0),
                          List.of(
                              new Block.Input(new VariableId(0), new Type.SecretBinaryInteger(1)),
                              new Block.Input(new VariableId(1), new Type.SecretBinaryInteger(64)),
                              new Block.Input(new VariableId(2), new Type.SecretBinaryInteger(64))),
                          List.of(
                              new Instruction(
                                  new VariableId(3),
                                  new Type.SecretBinaryInteger(64),
                                  new Operation.Select(
                                      new VariableId(0), new VariableId(1), new VariableId(2)))),
                          new Terminator.BranchAlways(
                              new Terminator.BranchInfo(BlockId.RETURN, List.of())))),
                  List.of(new Type.SecretBinaryInteger(64)))));

  @Test
  public void specificPretty() {
    final String expected =
        """
        (metacircuit
         (function %0
          (output)
          (block #99
            (inputs
              (sbi1 $0))
            (sbi1 $1 (constant 1))
            (sbi1 $2 (bitwise_xor $0 $1))
            (branch-always #return))))""";
    assertThat(MetaCircuitPretty.pretty(VALID_1)).isEqualTo(expected.strip());
  }

  @Test
  public void specificPretty2() {
    final String expected =
        """
        (metacircuit
         (function %0
          (output)
          (block #0
            (sbi1 $0 (constant 0))
            (sbi1 $1 (bitwise_and $0 $0))
            (sbi1 $2 (bitwise_xor $1 $1))
            (branch-always #1))
          (block #1
            (sbi1 $0 (constant 1))
            (sbi1 $1 (bitwise_and $0 $0))
            (sbi1 $2 (bitwise_xor $1 $1))
            (branch-always #return))))""";
    assertThat(MetaCircuitPretty.pretty(VALID_2)).isEqualTo(expected.strip());
  }

  @Test
  public void specificPretty5() {
    final String expected =
        """
        (metacircuit
         (function %0
          (output sbi64)
          (block #0
            (inputs
              (sbi64 $0)
              (sbi64 $1))
            (sbi64 $2 (bitwise_and $0 $1))
            (branch-always #return))))""";
    assertThat(MetaCircuitPretty.pretty(VALID_5_AND)).isEqualTo(expected.strip());
  }

  @Test
  public void specificPretty6() {
    final String expected =
        """
        (metacircuit
         (function %0
          (output sbi64)
          (block #0
            (inputs
              (sbi1 $0)
              (sbi64 $1)
              (sbi64 $2))
            (sbi64 $3 (select $0 $1 $2))
            (branch-always #return))))""";
    assertThat(MetaCircuitPretty.pretty(VALID_6_SELECT)).isEqualTo(expected.strip());
  }
}
