package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Testing the {@link Leb128} component. */
public final class Leb128Test {

  static Stream<Integer> lengths() {
    return Stream.of(
            IntStream.range(0, 30),
            IntStream.range(120, 130),
            IntStream.range(250, 270),
            IntStream.range(990, 1010),
            IntStream.range(0, 30).map(shiftAmount -> 1 << shiftAmount),
            IntStream.range(1, 30).map(shiftAmount -> 1 << shiftAmount).map(x -> x - 1),
            IntStream.range(1, 30).map(shiftAmount -> 1 << shiftAmount).map(x -> x + 1))
        .flatMap(IntStream::boxed);
  }

  @ParameterizedTest
  @MethodSource("lengths")
  void serializedDeserialized(Integer expected) {
    final byte[] serialized =
        SafeDataOutputStream.serialize(s -> Leb128.writeUnsignedInt(expected, s));
    final int deserialized = SafeDataInputStream.readFully(serialized, Leb128::readUnsignedInt);
    Assertions.assertThat(deserialized)
        .as("Serialized" + Arrays.toString(serialized))
        .isEqualTo(expected);
  }

  @Test
  public void integerTooLarge() {
    final byte[] bytes = Hex.decode("FF FF FF FF 70");
    Assertions.assertThatThrownBy(
            () -> SafeDataInputStream.deserialize(s -> Leb128.readUnsigned(30, s), bytes))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
  }

  @Test
  public void integerTooLong() {
    final byte[] bytes = Hex.decode("FF FF FF FF 70");
    Assertions.assertThatThrownBy(
            () -> SafeDataInputStream.deserialize(s -> Leb128.readUnsigned(10, s), bytes))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");
  }

  @Test
  public void leb128Unsigned() {
    Assertions.assertThat(Leb128.readUnsigned(1, byteStream(0x00))).isEqualTo(0);
    Assertions.assertThat(Leb128.readUnsigned(8, byteStream(0x00))).isEqualTo(0);
    Assertions.assertThat(Leb128.readUnsigned(16, byteStream(0x00))).isEqualTo(0);
    Assertions.assertThat(Leb128.readUnsigned(32, byteStream(0x00))).isEqualTo(0);
    Assertions.assertThat(Leb128.readUnsigned(62, byteStream(0x00))).isEqualTo(0);
    Assertions.assertThat(Leb128.readUnsigned(63, byteStream(0x00))).isEqualTo(0);

    Assertions.assertThat(Leb128.readUnsigned(32, byteStream(0xff, 0x7f))).isEqualTo(0x3fffL);
    Assertions.assertThat(Leb128.readUnsigned(32, byteStream(0xff, 0xff, 0x7f)))
        .isEqualTo(0x1fffffL);
    Assertions.assertThat(Leb128.readUnsigned(32, byteStream(0xff, 0xff, 0xff, 0x7f)))
        .isEqualTo(0xfffffffL);
    Assertions.assertThat(Leb128.readUnsigned(32, byteStream(0xff, 0xff, 0xff, 0xff, 0x07)))
        .isEqualTo(Integer.MAX_VALUE);

    // Binary pattern matching LEB128 encoding
    Assertions.assertThat(Leb128.readUnsigned(32, byteStream(0b11010111, 0b11110101, 0b00001101)))
        .isEqualTo(0b000110111101011010111);

    Assertions.assertThatThrownBy(() -> Leb128.readUnsigned(1, byteStream(0x02)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
    Assertions.assertThatThrownBy(
            () -> Leb128.readUnsigned(32, byteStream(0x80, 0x80, 0x80, 0x80, 0x10)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");

    Assertions.assertThatThrownBy(
            () -> Leb128.readUnsigned(32, byteStream(0x80, 0x80, 0x80, 0x80, 0x80)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");

    Assertions.assertThatThrownBy(
            () -> Leb128.readUnsigned(32, byteStream(0x82, 0x80, 0x80, 0x80, 0x80, 0x00)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");

    Assertions.assertThatThrownBy(() -> Leb128.readUnsigned(13, byteStream(0xFF, 0x8F)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");
    Assertions.assertThatThrownBy(() -> Leb128.readUnsigned(14, byteStream(0xFF, 0x8F)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");
    Assertions.assertThat(Leb128.readUnsigned(15, byteStream(0xFF, 0x8F, 0x01))).isEqualTo(18431);

    Assertions.assertThat(Leb128.readUnsigned(14, byteStream(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThat(Leb128.readUnsigned(15, byteStream(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThat(Leb128.readUnsigned(16, byteStream(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThat(Leb128.readUnsigned(17, byteStream(0x7F))).isEqualTo(0x7fL);

    Assertions.assertThatThrownBy(() -> Leb128.readUnsigned(6, byteStream(0x7F)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
    Assertions.assertThat(Leb128.readUnsigned(7, byteStream(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThatThrownBy(() -> Leb128.readUnsigned(8, byteStream(0xFF, 0x03)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
    Assertions.assertThatThrownBy(() -> Leb128.readUnsigned(9, byteStream(0xFF, 0x07)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
  }

  private SafeDataInputStream byteStream(int... bytes) {
    byte[] bs = new byte[bytes.length];

    for (int i = 0; i < bytes.length; i++) {
      bs[i] = (byte) bytes[i];
    }
    return SafeDataInputStream.createFromBytes(bs);
  }
}
