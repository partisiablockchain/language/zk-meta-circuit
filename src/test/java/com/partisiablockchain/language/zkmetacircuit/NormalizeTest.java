package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Map;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class NormalizeTest {

  //// General tests

  @ParameterizedTest
  @MethodSource("provideTestCasesValidStable")
  public void testNormalizeStable(final ExampleCircuits.TestCase<MetaCircuit> testCase) {
    System.out.println(testCase.filename());
    assertThat(MetaCircuitNormalizer.normalizeMetaCircuit(testCase.data()).metaCircuit())
        .describedAs(testCase.filename())
        .isEqualTo(testCase.data());
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesValidUnstable")
  public void testNormalizeUnstable(final ExampleCircuits.TestCase<MetaCircuit> testCase) {
    System.out.println(testCase.filename());
    assertThat(MetaCircuitNormalizer.normalizeMetaCircuit(testCase.data()).metaCircuit())
        .describedAs(testCase.filename())
        .isNotEqualTo(testCase.data());
  }

  //// Specific circuits

  private static final String TEST_1_NOT_NORMALIZED =
      """
      (metacircuit
       (function %0
        (output sbi64)
        (block #999
          (inputs
            (sbi64 $321)
            (i1 $7231))
          (sbi64 $2 (bitwise_and $321 $7231))
          (branch-always #231 $7231))
        (block #231
          (inputs
            (i1 $444))
          (sbi64 $666 (constant 0))
          (i1 $111 (constant 0))
          (branch-if $444
            (0 #return $666)
            (1 #999 $111)))))""";

  private static final String TEST_1_NORMALIZED =
      """
      (metacircuit
       (function %0
        (output sbi64)
        (block #0
          (inputs
            (sbi64 $0)
            (i1 $1))
          (sbi64 $2 (bitwise_and $0 $1))
          (branch-always #1 $1))
        (block #1
          (inputs
            (i1 $0))
          (sbi64 $1 (constant 0))
          (i1 $2 (constant 0))
          (branch-if $0
            (0 #return $1)
            (1 #0 $2)))))""";

  @Test
  public void normalizeSpecific() {
    final MetaCircuit circuit = MetaCircuitPrettyParser.parse(TEST_1_NOT_NORMALIZED);
    final MetaCircuit normalizedCircuit =
        MetaCircuitNormalizer.normalizeMetaCircuit(circuit).metaCircuit();

    assertThat(normalizedCircuit).isNotEqualTo(circuit);
    assertThat(MetaCircuitPretty.pretty(normalizedCircuit)).isEqualTo(TEST_1_NORMALIZED);
  }

  @Test
  public void testNormalizedToString() {
    final String tinyCircuit = "(metacircuit)";
    final MetaCircuit circuit = MetaCircuitPrettyParser.parse(tinyCircuit);
    assertThat(MetaCircuitNormalizer.normalizeMetaCircuit(circuit))
        .asString()
        .isEqualTo("MetaCircuit.Normalized[metaCircuit=MetaCircuit[functions=[]]]");
  }

  @Test
  public void testErrorOnFailToNormalizeVariable() {
    final var env = MetaCircuitNormalizer.NormalizeEnvironment.forBlockIdRewriteMap(Map.of());
    assertThatThrownBy(() -> env.getNormalizedId(new VariableId(4)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not normalize variable $4");
  }

  @Test
  public void testErrorOnFailToNormalizeBlock() {
    final var env = MetaCircuitNormalizer.NormalizeEnvironment.forBlockIdRewriteMap(Map.of());
    assertThatThrownBy(() -> env.getNormalizedId(new BlockId(4)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not normalize block id #4");
  }

  //// Test providers

  static Stream<Arguments> provideTestCasesValidStable() {
    return ExampleCircuits.VALID_STABLE.stream().map(x -> Arguments.of(x));
  }

  static Stream<Arguments> provideTestCasesValidUnstable() {
    return ExampleCircuits.VALID_UNSTABLE.stream().map(x -> Arguments.of(x));
  }
}
