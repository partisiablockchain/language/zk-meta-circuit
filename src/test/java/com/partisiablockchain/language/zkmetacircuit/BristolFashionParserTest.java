package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import org.junit.jupiter.api.Test;

final class BristolFashionParserTest {
  static final String GOOD_CIRCUIT =
      """
      4 5
      2 1 2
      1 1 2

      2 1 0 1 3 AND
      2 1 1 2 4 XOR
      2 1 2 4 5 XOR
      2 1 5 3 6 XOR
      """;

  static final String BAD_CIRCUIT_OUT_OF_ORDER =
      """
      4 5
      2 1 2
      1 1

      2 1 5 3 6 XOR
      2 1 0 1 3 AND
      2 1 1 2 4 XOR
      2 1 2 4 5 XOR
      """;

  @Test
  public void parseBristolFashionGraph() {
    final BristolFashionGraph graph = BristolFashionGraph.parseFrom(GOOD_CIRCUIT);
    assertThat(graph.inputSizes()).containsExactly(1, 2);
    assertThat(graph.outputSizes()).containsExactly(1);
    assertThat(graph.numInputWires()).isEqualTo(3);
    assertThat(graph.correctlyOrdered()).isTrue();
    assertThat(graph.firstOutputWire()).isEqualTo(4);

    // Can run:
    final MetaCircuit circuit = graph.toMetaCircuit();
    assertThat(circuit).isNotNull();
  }

  @Test
  public void parseIncorrectlyOrdered() {
    final BristolFashionGraph graph = BristolFashionGraph.parseFrom(BAD_CIRCUIT_OUT_OF_ORDER);
    assertThat(graph.inputSizes()).containsExactly(1, 2);
    assertThat(graph.numInputWires()).isEqualTo(3);
    assertThat(graph.outputSizes()).containsExactly(1);
    assertThat(graph.correctlyOrdered()).isFalse();

    // Can run:
    assertThatThrownBy(() -> graph.toMetaCircuit())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid Bristol Circuit: Some wires were used before they were defined!");
  }

  @Test
  public void bristolFashionGraphMustHavePositiveNumberOfWires() {
    assertThatThrownBy(() -> new BristolFashionGraph(List.of(), List.of(), List.of(), -1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("firstOutputWire cannot be negative");
  }
}
