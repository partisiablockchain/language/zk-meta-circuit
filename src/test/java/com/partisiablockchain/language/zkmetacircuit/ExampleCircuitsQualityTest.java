package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.immutabledetect.FindInObjectTree;
import java.util.List;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

final class ExampleCircuitsQualityTest {

  @ParameterizedTest
  @MethodSource("provideOps")
  public void checkAllOpsHaveBeenUsed(Object obj) {
    final var finder = new FindInObjectTree(obj::equals);
    Assertions.assertThat(metaCircuits()).as(obj.toString()).anyMatch(finder::anyMatch);
  }

  private static List<MetaCircuit> metaCircuits() {
    return ExampleCircuits.VALID_STABLE.stream().map(ExampleCircuits.TestCase::data).toList();
  }

  @ParameterizedTest
  @MethodSource("provideOperations")
  public void checkAllOperationsHaveBeenUsed(Class<?> cls) {
    final var finder = new FindInObjectTree(cls::isInstance);
    Assertions.assertThat(metaCircuits()).as(cls.toString()).anyMatch(finder::anyMatch);
  }

  static Stream<Object> provideOps() {
    return Stream.of(
            Operation.UnaryOp.values(), Operation.BinaryOp.values(), Operation.NullaryOp.values())
        .flatMap(Stream::of);
  }

  static Stream<Class<?>> provideOperations() {
    return Stream.of(Operation.class.getPermittedSubclasses());
  }
}
