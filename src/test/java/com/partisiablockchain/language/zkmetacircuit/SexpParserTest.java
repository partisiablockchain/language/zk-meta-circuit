package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.immutabledetect.ImmutableDetect;
import com.partisiablockchain.language.zkmetacircuit.sexp.Sexp;
import com.partisiablockchain.language.zkmetacircuit.sexp.SexpParser;
import java.util.List;
import org.junit.jupiter.api.Test;

final class SexpParserTest {

  @Test
  public void malformedExpressions() {
    assertThatThrownBy(() -> SexpParser.parse("(metacircuit")).isInstanceOf(RuntimeException.class);
    assertThatThrownBy(() -> SexpParser.parse(")"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unexpected close bracket");
    assertThatThrownBy(() -> SexpParser.parse("(abc xyz))"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Expected EOF");
    assertThatThrownBy(() -> SexpParser.parse("abc xyz)"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Expected EOF");
    assertThatThrownBy(() -> SexpParser.parse("abc xyz"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Expected EOF");
    assertThatThrownBy(() -> SexpParser.parse("(abc xyz) 123"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Expected EOF");
  }

  @Test
  public void happyPath() {

    assertThat(SexpParser.parse("abc")).isEqualTo(new Sexp.Word("abc"));

    assertThat(SexpParser.parse("         abc        ")).isEqualTo(new Sexp.Word("abc"));

    assertThat(SexpParser.parse("()")).isEqualTo(new Sexp.Sequence(List.of()));

    assertThat(SexpParser.parse("(abc)"))
        .isEqualTo(new Sexp.Sequence(List.of(new Sexp.Word("abc"))));

    assertThat(SexpParser.parse("(123)"))
        .isEqualTo(new Sexp.Sequence(List.of(new Sexp.Word("123"))));

    assertThat(SexpParser.parse("(abc xyz)"))
        .isEqualTo(new Sexp.Sequence(List.of(new Sexp.Word("abc"), new Sexp.Word("xyz"))));

    assertThat(SexpParser.parse("(abc $xyz)"))
        .isEqualTo(new Sexp.Sequence(List.of(new Sexp.Word("abc"), new Sexp.Word("$xyz"))));

    assertThat(SexpParser.parse("   (    abc    $xyz    )    "))
        .isEqualTo(new Sexp.Sequence(List.of(new Sexp.Word("abc"), new Sexp.Word("$xyz"))));

    assertThat(SexpParser.parse("(abc (xyz (123) xyz) abc)"))
        .isEqualTo(
            new Sexp.Sequence(
                List.of(
                    new Sexp.Word("abc"),
                    new Sexp.Sequence(
                        List.of(
                            new Sexp.Word("xyz"),
                            new Sexp.Sequence(List.of(new Sexp.Word("123"))),
                            new Sexp.Word("xyz"))),
                    new Sexp.Word("abc"))));

    assertThat(SexpParser.parse("      (      )      ")).isEqualTo(new Sexp.Sequence(List.of()));
  }

  private static final List<String> STABLE_SEXPS_EXAMPLES =
      List.of("abc", "()", "(abc)", "(abc xyz)", "(123)", "(abc (xyz (123) xyz) abc)");

  @Test
  public void sexpRoundTrip() {
    for (final String example : STABLE_SEXPS_EXAMPLES) {
      assertThat(SexpParser.parse(example))
          .asString()
          .isEqualTo(example)
          .matches(ImmutableDetect::isImmutable, "is immutable");
    }
  }
}
