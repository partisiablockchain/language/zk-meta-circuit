package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.WithResource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/** Utility class for supporting various reference testing patterns. */
public final class ReferenceTestUtility {

  private ReferenceTestUtility() {}

  /**
   * Determines the path to the given class' resource directory.
   *
   * @param clazz Class to determine relative to.
   * @param filename Filename relative to the resource folder.
   */
  public static Path getResourceSourcePath(final Class<?> clazz, final String filename) {
    final String[] split = clazz.getPackageName().split("\\.");
    final Path referencePathDir = Paths.get("./src/test/resources/", split);
    return referencePathDir.resolve(filename);
  }

  /**
   * Determines the path to the given class' reference resource directory.
   *
   * @param clazz Class to determine relative to.
   */
  public static Path getReferenceDirectoryPath(final Class<?> clazz) {
    final String[] names = clazz.getName().split("\\.", -1);
    final String baseName = names[names.length - 1];
    return getResourceSourcePath(ReferenceTestUtility.class, "ref").resolve(baseName);
  }

  /**
   * Writes the given text to the given filepath, in a platform dependent manner.
   *
   * @param filePath File path to write text to.
   * @param textWithUnixNewline Text to be written, with unix newlines.
   */
  public static void writeText(final Path filePath, final String textWithUnixNewline) {
    final String text = textWithUnixNewline.replace("\n", System.lineSeparator());

    WithResource.accept(
        () -> {
          // Create any directories required
          Files.createDirectories(filePath.getParent());

          // Write file itself
          return new java.io.PrintWriter(filePath.toFile(), StandardCharsets.UTF_8);
        },
        outputStream -> outputStream.write(text),
        "Could not write to file " + filePath);
  }

  /**
   * Loads the given file path as text.
   *
   * @param filePath File path to read from.
   * @return Text contents of file as string.
   */
  public static String loadText(final Path filePath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> new FileInputStream(filePath.toFile()),
            InputStream::readAllBytes,
            "Could not load file: " + filePath);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }
}
