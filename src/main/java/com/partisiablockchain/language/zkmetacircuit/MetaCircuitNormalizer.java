package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides utility methods for normalizing Meta-Circuits.
 *
 * <p>Normalization of a meta-circuit involves:
 *
 * <ul>
 *   <li>means rewriting all {@link VariableId} and {@link BlockId} to be continously growing from
 *       0.
 *   <li>Normalization is idempotent.
 * </ul>
 */
public final class MetaCircuitNormalizer {

  private MetaCircuitNormalizer() {}

  /**
   * Normalizes meta-circuit.
   *
   * @param metaCircuit Meta-Circuit to normalize.
   * @return Normalized meta-circuit. Never null.
   */
  public static MetaCircuit.Normalized normalizeMetaCircuit(final MetaCircuit metaCircuit) {
    return MetaCircuit.ofNormalized(
        metaCircuit.functions().stream().map(MetaCircuitNormalizer::normalizeFunction).toList());
  }

  private static MetaCircuit.Function normalizeFunction(final MetaCircuit.Function function) {

    // Create environment for rewriting block ids
    final Map<BlockId, BlockId> blockIdToNormalizedId = blockNormalizedIdMap(function.blocks());

    return new MetaCircuit.Function(
        function.functionId(),
        function.blocks().stream()
            .map(block -> normalizeBlock(block, blockIdToNormalizedId))
            .toList(),
        function.returnTypes());
  }

  /**
   * Creates normalization map for block ids based on the given list of blocks. The keys in the map
   * are the old ids, and the values are the ids that the old ids should be replaced with.
   *
   * @param blocks Blocks to create normalization map for.
   * @return Block id normalization map. Never null.
   */
  private static Map<BlockId, BlockId> blockNormalizedIdMap(final List<Block> blocks) {
    int blockNormalizedId = 0;
    final var m = new HashMap<BlockId, BlockId>();
    for (final Block block : blocks) {
      m.put(block.blockId(), new BlockId(blockNormalizedId++));
    }
    return Map.copyOf(m);
  }

  /**
   * Normalizes block in meta-circuit.
   *
   * @param block Block to normalize
   * @param blockIdToNormalizedId A normalization map for block ids.
   * @return Normalized block. Never null.
   */
  private static Block normalizeBlock(
      final Block block, final Map<BlockId, BlockId> blockIdToNormalizedId) {

    final NormalizeEnvironment environment =
        NormalizeEnvironment.forBlockIdRewriteMap(blockIdToNormalizedId);

    // Input variables
    final List<Block.Input> normalizedInputs =
        block.inputs().stream()
            .map(
                input -> {
                  final VariableId normalizedId = environment.add(input.assignedVariable());
                  return new Block.Input(normalizedId, input.variableType());
                })
            .toList();

    // Instructions
    final List<Instruction> normalizedInstructions =
        block.instructions().stream()
            .map(
                instruction -> {
                  environment.add(instruction.resultId());
                  return normalizeInstruction(instruction, environment);
                })
            .toList();

    // Branch information
    final Terminator normalizedTerminator = normalizeTerminator(block.terminator(), environment);

    return new Block(
        environment.getNormalizedId(block.blockId()),
        normalizedInputs,
        normalizedInstructions,
        normalizedTerminator);
  }

  /**
   * Normalizes terminator in meta-circuit.
   *
   * @param uncheckedTerminator Terminator to normalize.
   * @param environment Environment to normalize terminator in.
   * @return Normalized terminator. Never null.
   */
  private static Terminator normalizeTerminator(
      final Terminator uncheckedTerminator, final NormalizeEnvironment environment) {

    if (uncheckedTerminator instanceof Terminator.BranchAlways terminator) {
      return new Terminator.BranchAlways(normalizeBranchInfo(terminator.branch(), environment));

    } else if (uncheckedTerminator instanceof Terminator.Call terminator) {
      return new Terminator.Call(
          terminator.functionId(),
          normalizeVariables(terminator.functionArguments(), environment),
          normalizeBranchInfo(terminator.branchToAfterReturn(), environment));

    } else {
      final Terminator.BranchIf terminator = (Terminator.BranchIf) uncheckedTerminator;

      return new Terminator.BranchIf(
          environment.getNormalizedId(terminator.conditionVariable()),
          normalizeBranchInfo(terminator.branchTrue(), environment),
          normalizeBranchInfo(terminator.branchFalse(), environment));
    }
  }

  /**
   * Normalizes branch info in meta-circuit.
   *
   * @param branch Branch info to normalize.
   * @param environment Environment to normalize terminator in.
   * @return Normalized branch info. Never null.
   */
  private static Terminator.BranchInfo normalizeBranchInfo(
      final Terminator.BranchInfo branch, final NormalizeEnvironment environment) {
    return new Terminator.BranchInfo(
        environment.getNormalizedId(branch.targetBlock()),
        normalizeVariables(branch.branchInputVariables(), environment));
  }

  private static List<VariableId> normalizeVariables(
      final List<VariableId> variables, final NormalizeEnvironment environment) {
    return variables.stream().map(environment::getNormalizedId).toList();
  }

  /**
   * Normalizes instruction in meta-circuit.
   *
   * @param insn Instruction to normalize.
   * @param environment Environment to normalize terminator in.
   * @return Normalized instruction. Never null.
   */
  private static Instruction normalizeInstruction(
      final Instruction insn, final NormalizeEnvironment environment) {
    return new Instruction(
        environment.getNormalizedId(insn.resultId()),
        insn.resultType(),
        normalizeOperation(insn.operation(), environment));
  }

  /**
   * Normalizes operation in meta-circuit.
   *
   * @param uncheckedOperation Operation to normalize.
   * @param environment Environment to normalize terminator in.
   * @return Normalized operation. Never null.
   */
  private static Operation normalizeOperation(
      final Operation uncheckedOperation, final NormalizeEnvironment environment) {

    // Nullary
    if (uncheckedOperation instanceof Operation.Nullary operation) {
      return operation;

      // Constant
    } else if (uncheckedOperation instanceof Operation.Constant operation) {
      return operation;

      // Unary
    } else if (uncheckedOperation instanceof Operation.Unary operation) {
      return new Operation.Unary(
          operation.operation(), environment.getNormalizedId(operation.varIdx1()));

      // Cast
    } else if (uncheckedOperation instanceof Operation.Cast operation) {
      return new Operation.Cast(operation.type(), environment.getNormalizedId(operation.varIdx1()));

      // Binary
    } else if (uncheckedOperation instanceof Operation.Binary operation) {
      return new Operation.Binary(
          operation.operation(),
          environment.getNormalizedId(operation.varIdx1()),
          environment.getNormalizedId(operation.varIdx2()));

      // Select
    } else if (uncheckedOperation instanceof Operation.Select operation) {
      return new Operation.Select(
          environment.getNormalizedId(operation.varCondition()),
          environment.getNormalizedId(operation.varIfTrue()),
          environment.getNormalizedId(operation.varIfFalse()));

      // ExtractDynamically
    } else if (uncheckedOperation instanceof Operation.ExtractDynamically operation) {
      return new Operation.ExtractDynamically(
          environment.getNormalizedId(operation.varIdxExtractFrom()),
          operation.bitWidth(),
          environment.getNormalizedId(operation.varIdxOffsetFromLow()));

      // InsertDynamically
    } else if (uncheckedOperation instanceof Operation.InsertDynamically operation) {
      return new Operation.InsertDynamically(
          environment.getNormalizedId(operation.varIdxInsertInto()),
          environment.getNormalizedId(operation.varIdxInserted()),
          environment.getNormalizedId(operation.varIdxOffsetFromLow()));

      // Extract
    } else {
      final Operation.Extract operation = (Operation.Extract) uncheckedOperation;
      return new Operation.Extract(
          environment.getNormalizedId(operation.varIdx1()),
          operation.bitWidth(),
          operation.offsetFromLow());
    }
  }

  /**
   * Context class for normalization of instructions. Contains normalization maps for blocks and
   * variables, along with methods for adding new variable ids to the normalization maps.
   */
  static final class NormalizeEnvironment {

    /** Stores variable normalizations. */
    private final HashMap<VariableId, VariableId> variablesToAbsoluteNormalizedId = new HashMap<>();

    /** The next variable id to be used for normalized ids. */
    private int nextVariableId = 0;

    /** Stores block id normalizations. */
    private final Map<BlockId, BlockId> blockIdToNormalizedId;

    /**
     * Constructor for {@link NormalizeEnvironment}.
     *
     * @param blockIdToNormalizedId Block id normalization map.
     */
    private NormalizeEnvironment(final Map<BlockId, BlockId> blockIdToNormalizedId) {
      this.blockIdToNormalizedId = blockIdToNormalizedId;
    }

    /**
     * Create new {@link NormalizeEnvironment} for the {@link MetaCircuit.Function} given by the
     * block map.
     *
     * @param blockIdToNormalizedId Block id normalization map.
     * @return Newly created environment.
     */
    public static NormalizeEnvironment forBlockIdRewriteMap(
        final Map<BlockId, BlockId> blockIdToNormalizedId) {
      return new NormalizeEnvironment(blockIdToNormalizedId);
    }

    /**
     * Determine normalized {@link VariableId} for given {@link VariableId}.
     *
     * @param id Id to determine normalized version of.
     * @return Normalized id.
     */
    public VariableId getNormalizedId(final VariableId id) {
      final VariableId varId = variablesToAbsoluteNormalizedId.get(id);
      if (varId == null) {
        throw new RuntimeException(
            "Could not normalize variable %s".formatted(MetaCircuitPretty.prettyVariable(id)));
      }
      return varId;
    }

    /**
     * Determine normalized {@link BlockId} for given {@link BlockId}.
     *
     * @param id Id to determine normalized version of.
     * @return Normalized id.
     */
    public BlockId getNormalizedId(final BlockId id) {
      if (id.equals(BlockId.RETURN)) {
        return BlockId.RETURN;
      }
      final BlockId blockId = blockIdToNormalizedId.get(id);
      if (blockId == null) {
        throw new RuntimeException(
            "Could not normalize block id %s".formatted(MetaCircuitPretty.prettyBlockId(id)));
      }
      return blockId;
    }

    /**
     * Add new variable to normalization environment, producing a new normalized id.
     *
     * @param id Id to add to environment.
     * @return Normalized variable id.
     */
    public VariableId add(final VariableId id) {
      final var newNormalizedId = new VariableId(nextVariableId++);
      variablesToAbsoluteNormalizedId.put(id, newNormalizedId);
      return newNormalizedId;
    }
  }
}
