package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Utility class for determining required version for the given meta circuit.
 *
 * <p>By determining the required version for a given meta circuit, we can emit bytecode with
 * precisely that encoded version, and allowing older versions of the meta circuit library to parse
 * circuits encoded by newer ones, when the circuit doesn't use any of the newer features.
 */
public final class MetaCircuitVersioning {
  private MetaCircuitVersioning() {}

  /**
   * Determine required version and hence parser for the given meta circuit.
   *
   * @param circuit Circuit to determine version for.
   * @return Version to emit.
   */
  public static VersionConfig determineEarliestVersion(final MetaCircuit circuit) {
    Version version = Version.VERSION_1_0;

    int largestSeenTypeWidth = 0;

    if (circuit.functions().size() != 1
        || circuit.functions().get(0).functionId().functionId() != 0) {
      version = version.join(Version.VERSION_FIRST_TO_INTRODUCE_FUNCTIONS);
    }

    for (final MetaCircuit.Function function : circuit.functions()) {
      for (final Block block : function.blocks()) {
        for (final Block.Input input : block.inputs()) {
          largestSeenTypeWidth = Integer.max(input.variableType().width(), largestSeenTypeWidth);
        }

        for (final Instruction insn : block.instructions()) {
          version = version.join(insn.operation().versionIntroduced());
          largestSeenTypeWidth = Integer.max(insn.resultType().width(), largestSeenTypeWidth);
        }

        version = version.join(block.terminator().versionIntroduced());
      }
    }

    version = version.join(minimumVersionForTypeWidth(largestSeenTypeWidth));
    return configFromVersion(version);
  }

  /**
   * Determine version configuration from given version.
   *
   * @param version Version to determine configuration from.
   * @return Configuration for the given version.
   */
  public static VersionConfig configFromVersion(Version version) {
    return new VersionConfig(
        version, !version.isLessThan(Version.VERSION_FIRST_TO_INTRODUCE_FUNCTIONS));
  }

  /**
   * Largest bit-size supported for {@link Type}, before version {@link
   * Version#VERSION_FIRST_TO_SUPPORT_16383_BITS}.
   */
  private static final int PRE_VERSION_1_8_MAX_TYPE_WIDTH = 512;

  /**
   * Determines the minimum version for the given {@link Type#width}.
   *
   * @param typeWidth type width to determine minimum version for.
   * @return Version of MetaCircuit that could support that version.
   */
  private static Version minimumVersionForTypeWidth(int typeWidth) {
    return typeWidth > PRE_VERSION_1_8_MAX_TYPE_WIDTH
        ? Version.VERSION_FIRST_TO_SUPPORT_16383_BITS
        : Version.VERSION_1_0;
  }
}
