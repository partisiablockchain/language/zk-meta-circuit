package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/** Types in Zero-knowledge Meta-Circuits. */
@Immutable
public sealed interface Type {

  /** Secret-shared bit. */
  SecretBinaryInteger Sbit = new SecretBinaryInteger(1);

  /** Public bit. */
  PublicInteger PublicBool = new PublicInteger(1);

  /** Public unit, containing no data. */
  PublicInteger Unit = new PublicInteger(0);

  /**
   * The bit-width of the type.
   *
   * @return Bit-width of the type.
   */
  int width();

  /** The maximum bit width supported for {@link width}. */
  int MAXIMUM_ALLOWED_BIT_WIDTH = 16383;

  /**
   * Integer composed by several secret-shared bits.
   *
   * <p>Supports bit widths of up to {@link SecretBinaryInteger#MAXIMUM_ALLOWED_BIT_WIDTH}.
   */
  @Immutable
  record SecretBinaryInteger(int width) implements Type {
    /** Type prefix byte, in the serialized Meta-Circuit format. */
    public static final byte SERIALIZE_PREFIX = (byte) 0x01;

    /**
     * Constructor for {@link SecretBinaryInteger}.
     *
     * @param width Bit width of type. Must be between 0 and {@link MAXIMUM_ALLOWED_BIT_WIDTH}.
     */
    public SecretBinaryInteger {
      if (width < 0 || MAXIMUM_ALLOWED_BIT_WIDTH < width) {
        throw new IllegalArgumentException(
            "Invalid input: Bitwidth %d not supported. Must be between 0 and %d"
                .formatted(width, MAXIMUM_ALLOWED_BIT_WIDTH));
      }
    }
  }

  /**
   * Public integer, not secret-shared.
   *
   * <p>Supports bit widths of up to {@link PublicInteger#MAXIMUM_ALLOWED_BIT_WIDTH}.
   */
  @Immutable
  record PublicInteger(int width) implements Type {
    /** Type prefix byte, in the serialized Meta-Circuit format. */
    public static final byte SERIALIZE_PREFIX = (byte) 0x03;

    /**
     * Constructor for {@link PublicInteger}.
     *
     * @param width Bit width of type. Must be between 0 and {@link MAXIMUM_ALLOWED_BIT_WIDTH}.
     */
    public PublicInteger {
      if (width < 0 || MAXIMUM_ALLOWED_BIT_WIDTH < width) {
        throw new IllegalArgumentException(
            "Invalid input: Bitwidth %d not supported. Must be between 0 and %d"
                .formatted(width, MAXIMUM_ALLOWED_BIT_WIDTH));
      }
    }
  }
}
