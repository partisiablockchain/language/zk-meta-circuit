package com.partisiablockchain.language.zkmetacircuit.sexp;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.function.Predicate;

/** S-expression object. */
public sealed interface Sexp {

  /**
   * Casts to {@link Word} or throws error.
   *
   * @return The word. Never null
   * @exception RuntimeException If not a word.
   */
  Word expectWord();

  /**
   * Checks that this is a {@link Word} with a specific value. Throws an error otherwise.
   *
   * @param expectedWord The expected word value
   * @exception RuntimeException If not a word or if word value is not as expected.
   */
  void expectWord(String expectedWord);

  /**
   * Casts to {@link Sequence} or throws error.
   *
   * @return The sequence. Never null
   * @exception RuntimeException If not a sequence.
   */
  Sequence expectSequence();

  /** S-expression word. */
  record Word(String word) implements Sexp {

    @Override
    public Sequence expectSequence() {
      throw new RuntimeException("Invalid input: Expected sequence, found " + word);
    }

    @Override
    public Word expectWord() {
      return this;
    }

    @Override
    public void expectWord(String expectedWord) {
      if (!word.equals(expectedWord)) {
        throw new RuntimeException(
            "Invalid input: Expected word \"%s\", found \"%s\"".formatted(expectedWord, word));
      }
    }

    @Override
    public String toString() {
      return word;
    }
  }

  /** S-expression sequence. */
  record Sequence(List<Sexp> contents) implements Sexp {

    /**
     * Fetches the nth contained object.
     *
     * @param idx Index of object.
     * @return S-expression object. Never null
     */
    public Sexp get(final int idx) {
      return contents.get(idx);
    }

    /**
     * Length of the sequence.
     *
     * @return Length of the sequence.
     */
    public int length() {
      return contents.size();
    }

    /**
     * Get an iterator for the objects of the sequence.
     *
     * @return The new iterator
     */
    public SequenceIter iterator() {
      return new SequenceIter(this);
    }

    @Override
    public Word expectWord() {
      throw new RuntimeException("Invalid input: Expected word, found " + this);
    }

    @Override
    public void expectWord(String expectedWord) {
      throw new RuntimeException("Invalid input: Expected word, found " + this);
    }

    @Override
    public Sequence expectSequence() {
      return this;
    }

    /**
     * Creates predicate that check if a Sequence starts with an object fulfilling a condition.
     *
     * @param pred The predicate condition for the first object of the sequence
     * @return A predicate condition for the sequence
     */
    public static Predicate<Sequence> startsWith(Predicate<Sexp> pred) {
      return sequence -> sequence.length() > 0 && pred.test(sequence.get(0));
    }

    /**
     * Creates predicate that checks if a Sequence starts with a specific word.
     *
     * @param word The word to check for.
     * @return A predicate condition for the sequence
     */
    public static Predicate<Sequence> startsWithWord(String word) {
      return startsWith(sexp -> sexp instanceof Word && sexp.expectWord().word().equals(word));
    }

    @Override
    public String toString() {
      return "(" + String.join(" ", contents.stream().map(Object::toString).toList()) + ")";
    }
  }
}
