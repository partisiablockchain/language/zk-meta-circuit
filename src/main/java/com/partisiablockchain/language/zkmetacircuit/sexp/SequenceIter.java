package com.partisiablockchain.language.zkmetacircuit.sexp;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Stream;

/** Iterator over {@link Sexp.Sequence}. */
public final class SequenceIter {

  private final Sexp.Sequence seq;
  private int position = 0;

  /**
   * Create new iterator over sequence.
   *
   * @param seq Sequence to create iterator for.
   */
  SequenceIter(final Sexp.Sequence seq) {
    this.seq = seq;
  }

  /**
   * Produces stream over the remaining objects in sequence, and updates position to end.
   *
   * @return Stream of remaining objects in sequence. Never null.
   */
  public Stream<Sexp> takeRemaining() {
    final Stream<Sexp> stream = seq.contents().stream().skip(position);
    position = seq.length();
    return stream;
  }

  /**
   * Produces stream over objects in sequence that fits the predicate, and updates position to after
   * the last object.
   *
   * @param pred Predicate that objects must fit.
   * @return Stream of objects. Never null.
   */
  public Stream<Sexp> takeWhile(final Predicate<Sexp> pred) {
    final var resultList = new ArrayList<Sexp>();
    while (pred.test(peek())) {
      resultList.add(next());
    }
    return resultList.stream();
  }

  /**
   * Produces stream over objects in sequence that are sequences and fits the predicate, and updates
   * position to after the last object.
   *
   * @param pred Predicate that sequence objects must fit.
   * @return Stream of objects. Never null.
   */
  public Stream<Sexp.Sequence> takeSequenceWhile(Predicate<Sexp.Sequence> pred) {
    return takeWhile(sexp -> sexp instanceof Sexp.Sequence && pred.test((Sexp.Sequence) sexp))
        .map(Sexp::expectSequence);
  }

  /**
   * Returns next object if it fits the predicate, otherwise null. Position is updated if object
   * fit.
   *
   * @param pred Predicate that object must fit.
   * @return Single object. Might be null if predicate did not fit.
   */
  public Sexp takeIf(final Predicate<Sexp> pred) {
    if (pred.test(peek())) {
      return next();
    }
    return null;
  }

  /**
   * Returns next object if it is a sequence and fits the predicate, otherwise null. Position is
   * updated if object fit.
   *
   * @param pred Predicate that the sequence object must fit.
   * @return Single object. Might be null if predicate did not fit.
   */
  public Sexp.Sequence takeSequenceIf(final Predicate<Sexp.Sequence> pred) {
    return (Sexp.Sequence)
        takeIf(sexp -> sexp instanceof Sexp.Sequence && pred.test((Sexp.Sequence) sexp));
  }

  /**
   * Returns next object.
   *
   * @return Single object. Null if we are at the end of the stream.
   */
  public Sexp peek() {
    if (position >= seq.length()) {
      return null;
    }
    return seq.get(position);
  }

  /**
   * Returns next object.
   *
   * @return Single object. Never null.
   * @exception RuntimeException If already at the end of the sequence.
   */
  public Sexp next() {
    if (position >= seq.length()) {
      throw new RuntimeException("Invalid input: Expected sexp, but found end of sequence");
    }
    return seq.get(position++);
  }

  /**
   * Returns next object as a word.
   *
   * @return Single word. Never null.
   * @exception RuntimeException If already at the end of the sequence, or if next object was not a
   *     word.
   */
  public Sexp.Word nextWord() {
    return next().expectWord();
  }

  /**
   * Returns next object as a sequence.
   *
   * @return Single sequence. Never null.
   * @exception RuntimeException If already at the end of the sequence, or if next object was not a
   *     sequence.
   */
  public Sexp.Sequence nextSequence() {
    return next().expectSequence();
  }

  /**
   * Asserts that position is at the end of the sequence.
   *
   * @exception RuntimeException If not actually at the end of the sequence as was otherwise
   *     expected.
   */
  public void expectEnd() {
    if (position != seq.length()) {
      throw new RuntimeException(
          "Invalid input: Expected sequence of length %d, but was %s"
              .formatted(position, seq.length()));
    }
  }
}
