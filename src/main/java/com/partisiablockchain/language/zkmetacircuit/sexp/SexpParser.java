package com.partisiablockchain.language.zkmetacircuit.sexp;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/** Parses S-expression text to the simple {@link Sexp} S-expression AST. */
public final class SexpParser {

  private final String text;
  private int position;

  private SexpParser(final String text, final int initialPosition) {
    this.text = text;
    this.position = initialPosition;
  }

  /**
   * Parses S-expression text to the simple {@link Sexp} S-expression AST.
   *
   * @param text Text to parse.
   * @return S-expression AST.
   * @exception RuntimeException Thrown if text is not an S-expression.
   */
  public static Sexp parse(final String text) {
    return new SexpParser(text, 0).parseEntireFile();
  }

  //// Parse to s-expressions

  private Sexp parseEntireFile() {
    final Sexp ast = parseSexp();

    // Check for EOF
    skipWhitespace();
    if (position != text.length()) {
      throw new RuntimeException("Expected EOF");
    }
    return ast;
  }

  private Sexp parseSexp() {
    skipWhitespace();
    final char nextChar = peek();

    if (nextChar == '(') {
      skipOne(); // Skip starting '('
      skipWhitespace();

      final var contents = new ArrayList<Sexp>();
      while (peek() != ')') {
        contents.add(parseSexp());
        skipWhitespace();
      }

      skipOne(); // Read ending ')'
      return new Sexp.Sequence(List.copyOf(contents));
    } else if (nextChar == ')') {
      throw new RuntimeException("Unexpected close bracket");
    } else {
      return new Sexp.Word(readWord());
    }
  }

  //// General utility

  private char peek() {
    return text.charAt(position);
  }

  private void skipOne() {
    position += 1;
  }

  static final Set<Character> NON_WORD_CHARS = Set.of('(', ')');

  private String readWord() {
    final int wordStart = position;

    while (position < text.length()
        && !(Character.isWhitespace(text.charAt(position))
            || NON_WORD_CHARS.contains(text.charAt(position)))) {
      position++;
    }

    return text.substring(wordStart, position);
  }

  private void skipWhitespace() {
    while (position < text.length() && Character.isWhitespace(text.charAt(position))) {
      position++;
    }
  }
}
