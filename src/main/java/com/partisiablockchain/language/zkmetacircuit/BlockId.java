package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/**
 * Identifier for a {@link Block}.
 *
 * @param blockId Integer value of {@link BlockId}.
 */
@Immutable
public record BlockId(int blockId) {

  /**
   * Constructor for a {@link BlockId}.
   *
   * @param blockId Integer value of {@link BlockId}.
   */
  public BlockId {
    if (blockId < -1) {
      throw new IllegalArgumentException(
          "BlockIds must not be negative, but was %d".formatted(blockId));
    }
  }

  /**
   * Represents the return pseudo-block of a {@link MetaCircuit.Function}.
   *
   * <p>Branches to this block will result in the current function returning, and the arguments to
   * the branch will become the return values of the function.
   */
  public static final BlockId RETURN = new BlockId(-1);
}
