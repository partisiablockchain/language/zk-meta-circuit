package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Helper for validating that circuits are well-formed.
 *
 * <p>Currently validates:
 *
 * <ul>
 *   <li>Instructions refers to previously set variables.
 *   <li>Values produced by operations have types that are identical to those expected by the
 *       instruction definition.
 *   <li>Referenced variables are correctly typed for the given operation.
 * </ul>
 */
public final class MetaCircuitValidator {

  private MetaCircuitValidator() {}

  /**
   * Position of an {@link FunctionSignature} within a meta-circuit.
   *
   * @param functionId Id of function.
   * @param blockId Id of block. Null allowed.
   * @param variableId Id of variable. Null allowed.
   */
  record Position(FunctionId functionId, BlockId blockId, VariableId variableId) {

    @Override
    public String toString() {
      final List<String> out = new ArrayList<>();
      if (functionId == null) {
        out.add("Circuit");
      } else {
        out.add("Function %" + functionId.functionId());
      }
      if (blockId != null) {
        out.add("Block #" + blockId.blockId());
      }
      if (variableId != null) {
        out.add("Variable $" + variableId.variableId());
      }

      return String.join(" ", out);
    }
  }

  /**
   * Validation error message with {@link Position} information.
   *
   * @param pos Position information. Never null.
   * @param message Error message. Never null.
   */
  record ValidationError(Position pos, String message) {

    /** Constructor for validation error. */
    public ValidationError {
      requireNonNull(pos);
      requireNonNull(message);
    }
  }

  /**
   * Validation representation of a {@link MetaCircuit.Function} signature.
   *
   * @param parameterTypes Types of the functions input parameters.
   * @param returnTypes Types of values produced by function.
   */
  private record FunctionSignature(List<Type> parameterTypes, List<Type> returnTypes) {

    public FunctionSignature {
      requireNonNull(parameterTypes);
      requireNonNull(returnTypes);
    }
  }

  /**
   * Performs validation of a Meta-Circuit, throwing an exception if encountering an error.
   *
   * @param metaCircuit Meta-circuit to validate.
   * @exception MetaCircuitValidationException In case of one or more validation errors.
   */
  public static void validateMetaCircuit(final MetaCircuit metaCircuit) {
    final List<ValidationError> errors = validateMetaCircuitNoThrow(metaCircuit);

    // Create error message if any errors found
    if (!errors.isEmpty()) {
      throw new MetaCircuitValidationException(formatValidationErrors(errors));
    }

    // All good if reaching down here
  }

  /**
   * Formats validation errors to a descriptive summary message.
   *
   * @param errors List of errors to format.
   * @return Summary validation error message.
   */
  public static String formatValidationErrors(List<ValidationError> errors) {
    final StringBuilder out = new StringBuilder();
    out.append("Meta circuit validation failed:");
    for (final ValidationError error : errors) {
      out.append("\n   ").append(error.pos()).append(": ").append(error.message());
    }
    return out.toString();
  }

  /**
   * Performs validation of a Meta-Circuit, returning a list of errors.
   *
   * @param metaCircuit Meta-circuit to validate.
   * @return Immutable list of errors detected. Empty if Meta-circuit is valid. Never null.
   */
  public static List<ValidationError> validateMetaCircuitNoThrow(final MetaCircuit metaCircuit) {
    // Initialize validation errors
    final var validationErrors = new ArrayList<ValidationError>();

    final Map<FunctionId, FunctionSignature> functionEnvironment =
        determineFunctionSignatureEnvironment(metaCircuit, validationErrors);

    // Validate each function
    for (final MetaCircuit.Function function : metaCircuit.functions()) {
      validateFunction(function, functionEnvironment, validationErrors);
    }

    return List.copyOf(validationErrors);
  }

  private static void validateFunction(
      MetaCircuit.Function function,
      final Map<FunctionId, FunctionSignature> functionEnvironment,
      final List<ValidationError> validationErrors) {

    // Find the expected input types for each block
    final Map<BlockId, List<Type>> blockSignatures =
        determineBlockSignatureMap(function, validationErrors);

    // Validate that some blocks exists
    if (function.blocks().isEmpty()) {
      validationErrors.add(
          new ValidationError(new Position(null, null, null), "Meta Circuit had no blocks"));
    }

    // Validate each block
    for (final Block block : function.blocks()) {
      validateBlock(
          function.functionId(), block, validationErrors, blockSignatures, functionEnvironment);
    }
  }

  private static Map<BlockId, List<Type>> determineBlockSignatureMap(
      final MetaCircuit.Function function, final List<ValidationError> validationErrors) {
    final var blockSignatures = new HashMap<BlockId, List<Type>>();

    // End block must be in map
    blockSignatures.put(BlockId.RETURN, function.returnTypes());

    // Individual blocks
    for (final Block block : function.blocks()) {
      final List<Type> prevDefinition =
          blockSignatures.put(
              block.blockId(), block.inputs().stream().map(Block.Input::variableType).toList());

      // Validate unique block ids
      if (prevDefinition != null) {
        final var pos = new Position(function.functionId(), block.blockId(), null);
        validationErrors.add(new ValidationError(pos, "Block with duplicated id"));
      }
    }

    return Map.copyOf(blockSignatures);
  }

  private static Map<FunctionId, FunctionSignature> determineFunctionSignatureEnvironment(
      final MetaCircuit metaCircuit, final List<ValidationError> validationErrors) {
    final Map<FunctionId, FunctionSignature> functionEnvironment =
        new HashMap<FunctionId, FunctionSignature>();

    // Individual functions
    for (final MetaCircuit.Function function : metaCircuit.functions()) {
      final Position pos = new Position(function.functionId(), null, null);

      if (function.blocks().isEmpty()) {
        validationErrors.add(new ValidationError(pos, "Function is missing its body."));
        continue;
      }

      final List<Type> functionParameters =
          function.blocks().get(0).inputs().stream().map(Block.Input::variableType).toList();

      final FunctionSignature signature =
          new FunctionSignature(functionParameters, function.returnTypes());

      final FunctionSignature prevDefinition =
          functionEnvironment.put(function.functionId(), signature);

      // Validate unique function ids
      if (prevDefinition != null) {
        validationErrors.add(new ValidationError(pos, "Function with duplicated id"));
      }
    }

    return Map.copyOf(functionEnvironment);
  }

  /**
   * Performs validation of a Meta-Circuit block, appending errors to the given list.
   *
   * @param block Block to validate.
   * @param validationErrors Mutable list of errors detected.
   */
  private static void validateBlock(
      final FunctionId functionId,
      final Block block,
      final List<ValidationError> validationErrors,
      final Map<BlockId, List<Type>> blockSignatures,
      final Map<FunctionId, FunctionSignature> functionEnvironment) {

    final Position blockPos = new Position(functionId, block.blockId(), null);

    // Tracks types of variables
    final var variableEnvironment = new HashMap<VariableId, Type>();

    // Validate input variables
    // Add to variableEnvironment and ensure no duplicate assignments.
    for (final Block.Input input : block.inputs()) {
      final Type prevDefinition =
          variableEnvironment.put(input.assignedVariable(), input.variableType());
      if (prevDefinition != null) {
        validationErrors.add(
            new ValidationError(
                blockPos,
                String.format(
                    "Input overwrites previously defined variable, with type %s",
                    MetaCircuitPretty.pretty(prevDefinition))));
      }
    }

    // Validate instructions
    for (final Instruction instruction : block.instructions()) {
      final Position insnPos = new Position(functionId, block.blockId(), instruction.resultId());

      // Check that all referenced variables are present
      boolean allReferencedVariables = true;

      for (final VariableId referencedVariable : referencedVariables(instruction.operation())) {
        final Type type = variableEnvironment.getOrDefault(referencedVariable, null);
        if (type == null) {
          allReferencedVariables = false;
          validationErrors.add(
              new ValidationError(
                  insnPos,
                  String.format(
                      "Referenced unknown variable $%s", referencedVariable.variableId())));
        }
      }

      // Check that operands types are correct and that result type of the operation is
      // identical to resultType.
      if (allReferencedVariables) {
        final Type operationResultType =
            validateOperation(
                instruction.resultType(),
                instruction.operation(),
                variableEnvironment,
                message -> {
                  validationErrors.add(new ValidationError(insnPos, message));
                });
        if (!operationResultType.equals(instruction.resultType())) {
          validationErrors.add(
              new ValidationError(
                  insnPos,
                  String.format(
                      "%s operation produces type %s, but expected %s",
                      instruction.operation().symbolicName(),
                      MetaCircuitPretty.pretty(operationResultType),
                      MetaCircuitPretty.pretty(instruction.resultType()))));
        }
      }

      // Add to variableEnvironment and ensure no duplicate assignments.
      final Type prevDefinition =
          variableEnvironment.put(instruction.resultId(), instruction.resultType());
      if (prevDefinition != null) {
        validationErrors.add(
            new ValidationError(
                insnPos,
                String.format(
                    "%s operation overwrites previously defined variable $%s, with type %s",
                    instruction.operation().symbolicName(),
                    instruction.resultId().variableId(),
                    MetaCircuitPretty.pretty(prevDefinition))));
      }
    }

    // Branch blocks must be known
    final Consumer<String> addValidationError =
        msg -> validationErrors.add(new ValidationError(blockPos, msg));
    validateTerminator(
        block.terminator(),
        blockSignatures,
        functionEnvironment,
        variableEnvironment,
        addValidationError);
  }

  private static void validateTerminator(
      final Terminator uncheckedTerminator,
      final Map<BlockId, List<Type>> blockSignatures,
      final Map<FunctionId, FunctionSignature> functionEnvironment,
      final Map<VariableId, Type> variableEnvironment,
      final Consumer<String> addValidationError) {
    if (uncheckedTerminator instanceof Terminator.BranchAlways terminator) {
      validateBranch(
          terminator.symbolicName(),
          terminator.branch(),
          blockSignatures,
          variableEnvironment,
          addValidationError);

    } else if (uncheckedTerminator instanceof Terminator.Call terminator) {
      validateCall(
          terminator,
          blockSignatures,
          functionEnvironment,
          variableEnvironment,
          addValidationError);

    } else {
      final Terminator.BranchIf terminator = (Terminator.BranchIf) uncheckedTerminator;

      // Branch variable must be known
      final Type conditionVariableType =
          variableEnvironment.getOrDefault(terminator.conditionVariable(), null);
      if (conditionVariableType == null) {
        addValidationError.accept(
            "%s referenced unknown variable $%d"
                .formatted(terminator.symbolicName(), terminator.conditionVariable().variableId()));

        // Branch variable must be of type public bool
      } else if (!conditionVariableType.equals(Type.PublicBool)) {
        addValidationError.accept(
            "%s referenced non-bool variable $%d: %s"
                .formatted(
                    terminator.symbolicName(),
                    terminator.conditionVariable().variableId(),
                    MetaCircuitPretty.pretty(conditionVariableType)));
      }

      // Validate branches
      validateBranch(
          terminator.symbolicName(),
          terminator.branchFalse(),
          blockSignatures,
          variableEnvironment,
          addValidationError);
      validateBranch(
          terminator.symbolicName(),
          terminator.branchTrue(),
          blockSignatures,
          variableEnvironment,
          addValidationError);
    }
  }

  private static void validateCall(
      final Terminator.Call terminator,
      final Map<BlockId, List<Type>> blockSignatures,
      final Map<FunctionId, FunctionSignature> functionEnvironment,
      final Map<VariableId, Type> variableEnvironment,
      final Consumer<String> addValidationError) {

    // Validate call to function
    final FunctionSignature functionSignature = functionEnvironment.get(terminator.functionId());

    if (functionSignature == null) {
      addValidationError.accept(
          "%s referenced unknown function %s"
              .formatted(
                  terminator.symbolicName(),
                  MetaCircuitPretty.prettyFunctionId(terminator.functionId())));
      return;
    }

    validateBranch(
        terminator.symbolicName(),
        terminator.functionArguments(),
        functionSignature.parameterTypes(),
        variableEnvironment,
        addValidationError);

    // Validate return to block
    final List<Type> typesGiven =
        validateVariablesExists(
            terminator.symbolicName(),
            terminator.branchToAfterReturn().branchInputVariables(),
            variableEnvironment,
            addValidationError);

    if (typesGiven == null) {
      return;
    }

    final List<Type> typesOnReturn =
        Stream.of(typesGiven, functionSignature.returnTypes()).flatMap(List::stream).toList();

    final List<Type> blockExpectedTypes =
        blockSignatures.getOrDefault(terminator.branchToAfterReturn().targetBlock(), null);
    validateArguments(
        terminator.symbolicName(), typesOnReturn, blockExpectedTypes, addValidationError);
  }

  private static void validateBranch(
      final String symbolicName,
      final Terminator.BranchInfo branch,
      final Map<BlockId, List<Type>> blockSignatures,
      final Map<VariableId, Type> variableEnvironment,
      final Consumer<String> addValidationError) {
    final List<Type> blockExpectedTypes = blockSignatures.getOrDefault(branch.targetBlock(), null);

    // Validate that block exists
    if (blockExpectedTypes == null) {
      addValidationError.accept(
          "%s referenced unknown block %s"
              .formatted(symbolicName, MetaCircuitPretty.prettyBlockId(branch.targetBlock())));
      return;
    }

    validateBranch(
        symbolicName,
        branch.branchInputVariables(),
        blockExpectedTypes,
        variableEnvironment,
        addValidationError);
  }

  private static void validateBranch(
      final String symbolicName,
      final List<VariableId> branchInputVariables,
      final List<Type> blockExpectedTypes,
      final Map<VariableId, Type> variableEnvironment,
      final Consumer<String> addValidationError) {
    // Validate that types match block signature
    final List<Type> typesGiven =
        validateVariablesExists(
            symbolicName, branchInputVariables, variableEnvironment, addValidationError);
    if (typesGiven == null) {
      return;
    }
    validateArguments(symbolicName, typesGiven, blockExpectedTypes, addValidationError);
  }

  private static void validateArguments(
      final String symbolicName,
      final List<Type> typesGiven,
      final List<Type> typesRequired,
      final Consumer<String> addValidationError) {
    if (!typesGiven.equals(typesRequired)) {
      addValidationError.accept(
          "%s variable types (%s) did not match expected (%s)"
              .formatted(
                  symbolicName,
                  joinStr(typesGiven, ", ", MetaCircuitPretty::pretty),
                  joinStr(typesRequired, ", ", MetaCircuitPretty::pretty)));
    }
  }

  private static List<Type> validateVariablesExists(
      final String symbolicName,
      final List<VariableId> branchInputVariables,
      final Map<VariableId, Type> variableEnvironment,
      final Consumer<String> addValidationError) {
    final List<VariableId> variablesMissing =
        branchInputVariables.stream().filter(v -> !variableEnvironment.containsKey(v)).toList();
    if (!variablesMissing.isEmpty()) {
      addValidationError.accept(
          symbolicName
              + " referenced unknown variables "
              + joinStr(variablesMissing, ", ", x -> "$" + x.variableId()));
      return null;
    }

    // Validate that types match block signature
    return branchInputVariables.stream().map(variableEnvironment::get).toList();
  }

  private static final Set<Type> ALLOWED_CONDITION_TYPES = Set.of(Type.Sbit, Type.PublicBool);
  private static final Set<Operation.UnaryOp> UNOP_PUBLIC_TO_PUBLIC =
      Set.of(Operation.UnaryOp.LOAD_METADATA, Operation.UnaryOp.NEXT_VARIABLE_ID);

  /**
   * Validates operation.
   *
   * @param uncheckedOperation Operation to validate.
   * @param variableEnvironment variableEnvironment to validate the operation in.
   * @param addValidationError Consumer for adding validation error. Ensures operation's context is
   *     also given for the error.
   * @return Type of the operation result; Never null.
   */
  private static Type validateOperation(
      final Type expectedType,
      final Operation uncheckedOperation,
      final Map<VariableId, Type> variableEnvironment,
      final Consumer<String> addValidationError) {

    // Constant operation is trivial
    if (uncheckedOperation instanceof Operation.Constant operation) {
      return operation.type();

    } else if (uncheckedOperation instanceof Operation.Nullary operation) {
      return NULLARY_TYPES.get(operation.operation());

      // Cast operation is trivial
    } else if (uncheckedOperation instanceof Operation.Cast operation) {
      return operation.type();

      // Unary operations: Load variable (Public -> Secret)
    } else if (uncheckedOperation instanceof Operation.Unary operation
        && operation.operation().equals(Operation.UnaryOp.LOAD_VARIABLE)) {

      // Check type of argument operand
      final Type argumentType = variableEnvironment.get(operation.varIdx1());
      assertArgumentPublic(operation.operation(), argumentType, addValidationError);

      // Check result type
      if (!(expectedType instanceof Type.SecretBinaryInteger)) {
        addValidationError.accept(
            "%s can only produce secret data, but was expected to produce: %s"
                .formatted(operation.operation(), MetaCircuitPretty.pretty(expectedType)));
      }

      return expectedType;

      // Unary operation: Output (Always: Secret -> i0).
    } else if (uncheckedOperation instanceof Operation.Unary operation
        && Operation.UnaryOp.OUTPUT.equals(operation.operation())) {

      // Check type of argument operand
      final Type argumentType = variableEnvironment.get(operation.varIdx1());
      if (!(argumentType instanceof Type.SecretBinaryInteger)) {
        addValidationError.accept(
            "%s only accepts secret-typed arguments, but were given: %s"
                .formatted(operation.operation(), MetaCircuitPretty.pretty(argumentType)));
      }

      return Type.Unit;

      // Unary operations: Public -> Public
    } else if (uncheckedOperation instanceof Operation.Unary operation
        && UNOP_PUBLIC_TO_PUBLIC.contains(operation.operation())) {

      // Check type of argument operand
      final Type argumentType = variableEnvironment.get(operation.varIdx1());
      assertArgumentPublic(operation.operation(), argumentType, addValidationError);

      // Check result type
      if (!(expectedType instanceof Type.PublicInteger)) {
        addValidationError.accept(
            "%s can only produce public data, but was expected to produce: %s"
                .formatted(operation.operation(), MetaCircuitPretty.pretty(expectedType)));
      }
      return expectedType;

      // Unary operations: for all T: T -> T
    } else if (uncheckedOperation instanceof Operation.Unary operation) {
      return variableEnvironment.get(operation.varIdx1());

      // Select operation
    } else if (uncheckedOperation instanceof Operation.Select operation) {
      final Type typeCondition = variableEnvironment.get(operation.varCondition());
      final Type typeIfTrue = variableEnvironment.get(operation.varIfTrue());
      final Type typeIfFalse = variableEnvironment.get(operation.varIfFalse());

      if (!ALLOWED_CONDITION_TYPES.contains(typeCondition)) {
        addValidationError.accept(
            "%s operation's condition variable must be of type i1 or sbi1. It was: %s"
                .formatted(operation.symbolicName(), MetaCircuitPretty.pretty(typeCondition)));
      }
      if (!typeIfTrue.equals(typeIfFalse)) {
        addValidationError.accept(
            "%s operation's branches must have identical types. One was %s, the other %s"
                .formatted(
                    operation.symbolicName(),
                    MetaCircuitPretty.pretty(typeIfTrue),
                    MetaCircuitPretty.pretty(typeIfFalse)));
      }

      // Check that selects with the same secrety for all operands
      assertSameKind(
          "Select condition and branches", typeIfTrue, typeCondition, addValidationError);

      return typeIfTrue;

      // Extract
    } else if (uncheckedOperation instanceof Operation.Extract operation) {
      final Type typeExtractedFrom = variableEnvironment.get(operation.varIdx1());
      final boolean fitsWithinType =
          operation.offsetFromLow() + operation.bitWidth() <= typeBitLength(typeExtractedFrom);
      if (!fitsWithinType) {
        addValidationError.accept(
            "%s operation offset (%d) and size (%d) extended outside type %s"
                .formatted(
                    operation.symbolicName(),
                    operation.offsetFromLow(),
                    operation.bitWidth(),
                    MetaCircuitPretty.pretty(typeExtractedFrom)));
      }

      return ofSize(typeExtractedFrom, operation.bitWidth());

      // Extract Dynamically
    } else if (uncheckedOperation instanceof Operation.ExtractDynamically operation) {
      final Type typeExtractedFrom = variableEnvironment.get(operation.varIdxExtractFrom());
      final Type typeOffset = variableEnvironment.get(operation.varIdxOffsetFromLow());
      final Type typeExtracted = ofSize(typeExtractedFrom, operation.bitWidth());

      assertArgumentPublic(operation.symbolicName(), typeOffset, addValidationError);
      final boolean fitsWithinType =
          typeBitLength(typeExtracted) <= typeBitLength(typeExtractedFrom);
      if (!fitsWithinType) {
        addValidationError.accept(
            "%s cannot extract %s from %s"
                .formatted(
                    operation.symbolicName(),
                    MetaCircuitPretty.pretty(expectedType),
                    MetaCircuitPretty.pretty(typeExtractedFrom)));
      }

      return typeExtracted;

      // Insert Dynamically
    } else if (uncheckedOperation instanceof Operation.InsertDynamically operation) {
      final Type typeInsertInto = variableEnvironment.get(operation.varIdxInsertInto());
      final Type typeOffset = variableEnvironment.get(operation.varIdxOffsetFromLow());
      final Type typeInserted = variableEnvironment.get(operation.varIdxInserted());
      assertSameKind("Insert operands", typeInsertInto, typeInserted, addValidationError);
      assertArgumentPublic(operation.symbolicName(), typeOffset, addValidationError);

      final boolean fitsWithinType = typeBitLength(typeInserted) <= typeBitLength(typeInsertInto);
      if (!fitsWithinType) {
        addValidationError.accept(
            "%s cannot insert %s into %s"
                .formatted(
                    operation.symbolicName(),
                    MetaCircuitPretty.pretty(typeInserted),
                    MetaCircuitPretty.pretty(typeInsertInto)));
      }

      return typeInsertInto;

      // Binary operations
    } else {
      final Operation.Binary operation = (Operation.Binary) uncheckedOperation;
      final Type typeVar1 = variableEnvironment.get(operation.varIdx1());
      final Type typeVar2 = variableEnvironment.get(operation.varIdx2());

      return COMPARISON_OPERATIONS
          .get(operation.operation())
          .validate(operation.operation(), typeVar1, typeVar2, addValidationError);
    }
  }

  private static Type validateConcatOperation(
      @SuppressWarnings("unused") final Operation.BinaryOp operation,
      final Type typeVar1,
      final Type typeVar2,
      final Consumer<String> addValidationError) {
    assertSameKind("Operation operands", typeVar1, typeVar2, addValidationError);
    return ofSize(typeVar1, typeBitLength(typeVar1) + typeBitLength(typeVar2));
  }

  private static void assertSameKind(
      final Object name,
      final Type typeVar1,
      final Type typeVar2,
      final Consumer<String> addValidationError) {
    if (!typesSameKind(typeVar1, typeVar2)) {
      addValidationError.accept(
          "%s must be of the same kind. One was %s, the other %s"
              .formatted(
                  name, MetaCircuitPretty.pretty(typeVar1), MetaCircuitPretty.pretty(typeVar2)));
    }
  }

  private static void assertArgumentPublic(
      final Object name, final Type type, final Consumer<String> addValidationError) {
    if (!(type instanceof Type.PublicInteger)) {
      addValidationError.accept(
          "%s argument must be public, but was: %s"
              .formatted(name, MetaCircuitPretty.pretty(type)));
    }
  }

  private static Type ofSize(Type uncheckedType, int size) {
    return uncheckedType instanceof Type.SecretBinaryInteger
        ? new Type.SecretBinaryInteger(size)
        : new Type.PublicInteger(size);
  }

  private static void validateArithmeticCompatible(
      final Operation.BinaryOp operation,
      final Type typeVar1,
      final Type typeVar2,
      final Consumer<String> addValidationError) {
    if (!typesArithmeticCompatible(typeVar1, typeVar2)) {
      addValidationError.accept(
          "%s operation's operands must be compatible. One was %s, the other %s"
              .formatted(
                  operation,
                  MetaCircuitPretty.pretty(typeVar1),
                  MetaCircuitPretty.pretty(typeVar2)));
    }
  }

  private static Type validateArithmeticBinop(
      final Operation.BinaryOp operation,
      final Type typeVar1,
      final Type typeVar2,
      final Consumer<String> addValidationError) {
    validateArithmeticCompatible(operation, typeVar1, typeVar2, addValidationError);
    return typeVar1;
  }

  private static Type validateComparison(
      final Operation.BinaryOp operation,
      final Type typeVar1,
      final Type typeVar2,
      final Consumer<String> addValidationError) {
    validateArithmeticCompatible(operation, typeVar1, typeVar2, addValidationError);
    return ofSize(typeVar1, 1);
  }

  private static Type validateBitshift(
      final Operation.BinaryOp operation,
      final Type typeVar1,
      final Type typeVar2,
      final Consumer<String> addValidationError) {
    if (typeVar2 instanceof Type.SecretBinaryInteger) {
      addValidationError.accept(
          "%s operation's right hand side must be public, but was %s"
              .formatted(operation, MetaCircuitPretty.pretty(typeVar2)));
    }
    return typeVar1;
  }

  private interface BinopValidationFn {
    Type validate(
        Operation.BinaryOp operation,
        Type typeVar1,
        Type typeVar2,
        Consumer<String> addValidationError);
  }

  private static final Map<Operation.BinaryOp, BinopValidationFn> COMPARISON_OPERATIONS =
      Map.ofEntries(
          // Bitwise and arithmetic
          Map.entry(Operation.BinaryOp.BITWISE_AND, MetaCircuitValidator::validateArithmeticBinop),
          Map.entry(Operation.BinaryOp.BITWISE_OR, MetaCircuitValidator::validateArithmeticBinop),
          Map.entry(Operation.BinaryOp.BITWISE_XOR, MetaCircuitValidator::validateArithmeticBinop),
          Map.entry(Operation.BinaryOp.ADD_WRAPPING, MetaCircuitValidator::validateArithmeticBinop),
          Map.entry(
              Operation.BinaryOp.MULT_WRAPPING_SIGNED,
              MetaCircuitValidator::validateArithmeticBinop),
          Map.entry(
              Operation.BinaryOp.MULT_WRAPPING_UNSIGNED,
              MetaCircuitValidator::validateArithmeticBinop),

          // Bit concat
          Map.entry(Operation.BinaryOp.BIT_CONCAT, MetaCircuitValidator::validateConcatOperation),

          // Bitshifts
          Map.entry(
              Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL, MetaCircuitValidator::validateBitshift),
          Map.entry(
              Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL, MetaCircuitValidator::validateBitshift),

          // Comparisons
          Map.entry(Operation.BinaryOp.EQUAL, MetaCircuitValidator::validateComparison),
          Map.entry(Operation.BinaryOp.LESS_THAN_SIGNED, MetaCircuitValidator::validateComparison),
          Map.entry(
              Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED,
              MetaCircuitValidator::validateComparison),
          Map.entry(
              Operation.BinaryOp.LESS_THAN_UNSIGNED, MetaCircuitValidator::validateComparison),
          Map.entry(
              Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED,
              MetaCircuitValidator::validateComparison));

  private static final Map<Operation.NullaryOp, Type> NULLARY_TYPES =
      Map.ofEntries(Map.entry(Operation.NullaryOp.NUM_VARIABLES, new Type.PublicInteger(32)));

  private static boolean typesArithmeticCompatible(final Type type1, final Type type2) {
    return typeBitLength(type1) == typeBitLength(type2);
  }

  private static boolean typesSameKind(final Type type1, final Type type2) {
    return type1 instanceof Type.SecretBinaryInteger == type2 instanceof Type.SecretBinaryInteger;
  }

  private static int typeBitLength(final Type uncheckedType) {
    if (uncheckedType instanceof Type.SecretBinaryInteger type) {
      return type.width();
    } else {
      final Type.PublicInteger type = (Type.PublicInteger) uncheckedType;
      return type.width();
    }
  }

  private static Set<VariableId> referencedVariables(final Operation uncheckedOperation) {
    // Constant
    if (uncheckedOperation instanceof Operation.Constant) {
      return Set.of();

      // Nullary
    } else if (uncheckedOperation instanceof Operation.Nullary) {
      return Set.of();

      // Cast
    } else if (uncheckedOperation instanceof Operation.Cast operation) {
      return Set.of(operation.varIdx1());

      // Unary
    } else if (uncheckedOperation instanceof Operation.Unary operation) {
      return Set.of(operation.varIdx1());

      // Select
    } else if (uncheckedOperation instanceof Operation.Select operation) {
      return setOf(operation.varCondition(), operation.varIfTrue(), operation.varIfFalse());

      // Extract
    } else if (uncheckedOperation instanceof Operation.Extract operation) {
      return Set.of(operation.varIdx1());

      // Extract dynamically
    } else if (uncheckedOperation instanceof Operation.ExtractDynamically operation) {
      return setOf(operation.varIdxExtractFrom(), operation.varIdxOffsetFromLow());

      // Insert dynamically
    } else if (uncheckedOperation instanceof Operation.InsertDynamically operation) {
      return setOf(
          operation.varIdxInsertInto(),
          operation.varIdxInserted(),
          operation.varIdxOffsetFromLow());

      // Binary
    } else {
      final Operation.Binary operation = (Operation.Binary) uncheckedOperation;
      return setOf(operation.varIdx1(), operation.varIdx2());
    }
  }

  /**
   * Produces a new {@link Set}. Similar to {@link Set#of}, but this version will allow duplicant
   * values.
   *
   * @param elements Set elements. Duplicate values allowed.
   * @return Newly produced set.
   */
  private static Set<VariableId> setOf(VariableId... elements) {
    final Set<VariableId> set = new HashSet<VariableId>();
    for (final VariableId value : elements) {
      set.add(value);
    }
    return set;
  }

  private static <T> String joinStr(
      final List<T> list, final String sep, final Function<T, String> strMapper) {
    return String.join(sep, list.stream().map(strMapper).toList());
  }
}
