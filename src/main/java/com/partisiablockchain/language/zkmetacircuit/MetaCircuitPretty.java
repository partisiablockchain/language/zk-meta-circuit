package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.Locale;

/**
 * Utility class for pretty-printing Meta-Circuits for debugging.
 *
 * <p>Inverse of {@link MetaCircuitPrettyParser}.
 */
public final class MetaCircuitPretty {

  private MetaCircuitPretty() {}

  /**
   * Produces a prettified meta-circuit.
   *
   * @param metaCircuit Meta-Circuit to prettify.
   * @return Prettified string. never null.
   */
  public static String pretty(final MetaCircuit metaCircuit) {
    final StringBuilder out = new StringBuilder();
    out.append("(metacircuit");

    // Display blocks
    for (final MetaCircuit.Function function : metaCircuit.functions()) {
      prettyFunction(out, function);
    }
    out.append(")");

    return out.toString();
  }

  /**
   * Produces a prettified type.
   *
   * @param type Type to prettify.
   * @return Prettified string. never null.
   */
  public static String pretty(final Type type) {
    final StringBuilder out = new StringBuilder();
    prettyType(out, type);
    return out.toString();
  }

  private static void prettyFunction(final StringBuilder out, final MetaCircuit.Function function) {
    out.append("\n (function ").append(prettyFunctionId(function.functionId()));

    // End block and output variables
    out.append("\n  (output");
    for (final Type outputType : function.returnTypes()) {
      out.append(" ");
      prettyType(out, outputType);
    }
    out.append(")");

    // Display blocks
    for (final Block block : function.blocks()) {
      prettyBlock(out, block);
    }
    out.append(")");
  }

  private static void prettyBlock(final StringBuilder out, final Block block) {
    out.append("\n  (block ").append(prettyBlockId(block.blockId()));

    // input variables
    if (!block.inputs().isEmpty()) {
      out.append("\n    (inputs");
      for (final Block.Input input : block.inputs()) {
        out.append("\n      (");
        prettyType(out, input.variableType());
        out.append(" ").append(prettyVariable(input.assignedVariable()));
        out.append(")");
      }
      out.append(")");
    }

    // Instructions
    for (final Instruction insn : block.instructions()) {
      out.append("\n    ");
      prettyInstruction(out, insn);
    }

    // Branch info
    prettyTerminator(out, block.terminator());

    out.append(")");
  }

  private static void prettyTerminator(
      final StringBuilder out, final Terminator uncheckedTerminator) {
    if (uncheckedTerminator instanceof Terminator.BranchAlways terminator) {
      out.append("\n    (branch-always ");
      prettyBranchInfo(out, terminator.branch());
      out.append(")");
    } else if (uncheckedTerminator instanceof Terminator.Call terminator) {
      out.append("\n    (call")
          .append("\n      (")
          .append(prettyFunctionId(terminator.functionId()));
      prettyVariableList(out, terminator.functionArguments());
      out.append(")").append("\n      (");
      prettyBranchInfo(out, terminator.branchToAfterReturn());
      out.append("))");
    } else {
      final Terminator.BranchIf terminator = (Terminator.BranchIf) uncheckedTerminator;
      out.append("\n    (branch-if ")
          .append(prettyVariable(terminator.conditionVariable()))
          .append("\n      (0 ");
      prettyBranchInfo(out, terminator.branchFalse());
      out.append(")\n      (1 ");
      prettyBranchInfo(out, terminator.branchTrue());
      out.append("))");
    }
  }

  private static void prettyBranchInfo(
      final StringBuilder out, final Terminator.BranchInfo branch) {
    out.append(prettyBlockId(branch.targetBlock()));
    prettyVariableList(out, branch.branchInputVariables());
  }

  private static void prettyVariableList(
      final StringBuilder out, final List<VariableId> variables) {
    prettyList(out, variables.stream().map(MetaCircuitPretty::prettyVariable).toList());
  }

  private static void prettyList(final StringBuilder out, final Iterable<String> arguments) {
    for (final String arg : arguments) {
      out.append(" ").append(arg);
    }
  }

  private static void prettyInstruction(final StringBuilder out, final Instruction insn) {
    out.append("(");
    prettyType(out, insn.resultType());
    out.append(" %s ".formatted(prettyVariable(insn.resultId())));
    prettyOperation(out, insn.operation());
    out.append(")");
  }

  private static void prettyType(final StringBuilder out, final Type uncheckedType) {
    if (uncheckedType instanceof Type.PublicInteger type) {
      out.append("i%d".formatted(type.width()));
    } else {
      final Type.SecretBinaryInteger type = (Type.SecretBinaryInteger) uncheckedType;
      out.append("sbi%d".formatted(type.width()));
    }
  }

  private static void prettyOperation(final StringBuilder out, final Operation operation) {
    out.append("(").append(operation.symbolicName().toLowerCase(Locale.ENGLISH));
    prettyList(out, prettyOperationArguments(operation));
    out.append(")");
  }

  private static List<String> prettyOperationArguments(final Operation uncheckedOp) {
    // Constant
    if (uncheckedOp instanceof Operation.Constant operation) {
      return List.of("" + operation.const1());

      // Cast
    } else if (uncheckedOp instanceof Operation.Cast operation) {
      return List.of(prettyVariable(operation.varIdx1()));

      // Nullary
    } else if (uncheckedOp instanceof Operation.Nullary operation) {
      return List.of();

      // Binary
    } else if (uncheckedOp instanceof Operation.Binary operation) {
      return List.of(prettyVariable(operation.varIdx1()), prettyVariable(operation.varIdx2()));

      // Select
    } else if (uncheckedOp instanceof Operation.Select operation) {
      return List.of(
          prettyVariable(operation.varCondition()),
          prettyVariable(operation.varIfTrue()),
          prettyVariable(operation.varIfFalse()));

      // Extract
    } else if (uncheckedOp instanceof Operation.Extract operation) {
      return List.of(
          prettyVariable(operation.varIdx1()),
          "" + operation.bitWidth(),
          "" + operation.offsetFromLow());

      // Extract Dynamically
    } else if (uncheckedOp instanceof Operation.ExtractDynamically operation) {
      return List.of(
          prettyVariable(operation.varIdxExtractFrom()),
          "" + operation.bitWidth(),
          prettyVariable(operation.varIdxOffsetFromLow()));

      // Insert Dynamically
    } else if (uncheckedOp instanceof Operation.InsertDynamically operation) {
      return List.of(
          prettyVariable(operation.varIdxInsertInto()),
          prettyVariable(operation.varIdxInserted()),
          prettyVariable(operation.varIdxOffsetFromLow()));

      // Unary
    } else {
      final Operation.Unary operation = (Operation.Unary) uncheckedOp;
      return List.of(prettyVariable(operation.varIdx1()));
    }
  }

  static String prettyVariable(final VariableId variable) {
    return "$%d".formatted(variable.variableId());
  }

  static String prettyFunctionId(final FunctionId functionId) {
    return "%%%d".formatted(functionId.functionId());
  }

  static String prettyBlockId(final BlockId blockId) {
    if (blockId.equals(BlockId.RETURN)) {
      return "#return";
    }
    return "#%d".formatted(blockId.blockId());
  }
}
