package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/** Major-minor semantic versioning container. */
@Immutable
public record Version(short major, short minor) {

  //// Well-known versions

  /** * Constant for version 1.0. */
  public static final Version VERSION_1_0 = new Version((short) 1, (short) 0);

  /** * Constant for version 1.1. */
  public static final Version VERSION_1_1 = new Version((short) 1, (short) 1);

  /** * Constant for version 1.2. */
  public static final Version VERSION_1_2 = new Version((short) 1, (short) 2);

  /** * Constant for version 1.3. */
  public static final Version VERSION_1_3 = new Version((short) 1, (short) 3);

  /** * Constant for version 1.4. */
  public static final Version VERSION_1_4 = new Version((short) 1, (short) 4);

  /** * Constant for version 1.5. */
  public static final Version VERSION_1_5 = new Version((short) 1, (short) 5);

  /** * Constant for version 1.6. */
  public static final Version VERSION_1_6 = new Version((short) 1, (short) 6);

  /** * Constant for version 1.7. */
  public static final Version VERSION_1_7 = new Version((short) 1, (short) 7);

  /** * Constant for version 1.8. */
  public static final Version VERSION_1_8 = new Version((short) 1, (short) 8);

  // Remember to update README when adding to this list.

  /** The version that introduced {@link MetaCircuit.Function}. */
  public static final Version VERSION_FIRST_TO_INTRODUCE_FUNCTIONS = VERSION_1_6;

  /** Version to support 16383-bit types. */
  public static final Version VERSION_FIRST_TO_SUPPORT_16383_BITS = VERSION_1_8;

  //// Methods

  /**
   * Determines the joined version of the two versions.
   *
   * @param other Version to join with.
   * @return A joined version.
   */
  public Version join(final Version other) {
    return isLessThan(other) ? other : this;
  }

  boolean isLessThan(final Version other) {
    final boolean minorLessThan = this.major == other.major && this.minor < other.minor;
    final boolean majorLessThan = this.major < other.major;
    return minorLessThan || majorLessThan;
  }

  /**
   * Returns true if this given version should be supported given that the other argument is
   * supported.
   *
   * @param other Supported version
   * @return true if this version should be supported
   */
  public boolean isBackwardsCompatibleWith(Version other) {
    final boolean majorCompatible = this.major == other.major;
    final boolean minorCompatible = this.minor <= other.minor;
    return majorCompatible && minorCompatible;
  }
}
