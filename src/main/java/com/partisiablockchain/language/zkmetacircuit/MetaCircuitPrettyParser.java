package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkmetacircuit.sexp.SequenceIter;
import com.partisiablockchain.language.zkmetacircuit.sexp.Sexp;
import com.partisiablockchain.language.zkmetacircuit.sexp.SexpParser;
import java.util.List;
import java.util.Locale;

/**
 * Parses Meta-circuit S-expression format to a MetaCircuit.
 *
 * <p>Inverse of {@link MetaCircuitPretty}.
 */
public final class MetaCircuitPrettyParser {

  private MetaCircuitPrettyParser() {}

  /**
   * Parses Meta-circuit S-expression format to a MetaCircuit.
   *
   * @param text Text to parse.
   * @return Newly parsed MetaCircuit. Never null
   * @exception RuntimeException If text is not formatted as Meta-circuit S-expression.
   */
  public static MetaCircuit parse(final String text) {
    return parseMetaCircuit(SexpParser.parse(text).expectSequence());
  }

  //// Specific Meta-Circuit components

  private static MetaCircuit parseMetaCircuit(final Sexp.Sequence sexp) {
    final SequenceIter iter = sexp.iterator();
    iter.next().expectWord("metacircuit");

    final List<MetaCircuit.Function> functions =
        iter.takeRemaining()
            .map(Sexp::expectSequence)
            .map(MetaCircuitPrettyParser::parseFunction)
            .toList();

    return new MetaCircuit(functions);
  }

  private static MetaCircuit.Function parseFunction(final Sexp.Sequence sexp) {
    final SequenceIter iter = sexp.iterator();
    iter.next().expectWord("function");

    final FunctionId functionId = parseFunctionId(iter.nextWord());

    // End block and output variables
    final List<Type> outputVariableTypes;
    final Sexp.Sequence outputSeq = iter.takeSequenceIf(Sexp.Sequence.startsWithWord("output"));
    if (outputSeq != null) {
      final var outputIter = outputSeq.iterator();
      outputIter.next(); // Skip "output" prefix.
      outputVariableTypes =
          outputIter
              .takeRemaining()
              .map(Sexp::expectWord)
              .map(MetaCircuitPrettyParser::parseType)
              .toList();
    } else {
      outputVariableTypes = List.of();
    }

    // Parse blocks
    final List<Block> blocks =
        iter.takeRemaining()
            .map(Sexp::expectSequence)
            .map(MetaCircuitPrettyParser::parseBlock)
            .toList();

    return new MetaCircuit.Function(functionId, blocks, outputVariableTypes);
  }

  private static Block parseBlock(final Sexp.Sequence sexp) {
    final SequenceIter iter = sexp.iterator();
    iter.next().expectWord("block");

    // Block id
    final BlockId blockId = parseBlockId(iter.nextWord());

    // Input variables
    List<Block.Input> inputs = List.of();
    final Sexp.Sequence inputsSeq = iter.takeSequenceIf(Sexp.Sequence.startsWithWord("inputs"));
    if (inputsSeq != null) {
      final var inputsIter = inputsSeq.iterator();
      inputsIter.next(); // Skip "inputs";
      inputs =
          inputsIter
              .takeRemaining()
              .map(Sexp::expectSequence)
              .map(MetaCircuitPrettyParser::parseInput)
              .toList();
    }

    // Instructions
    final List<Instruction> instructions =
        iter.takeSequenceWhile(Sexp.Sequence.startsWith(MetaCircuitPrettyParser::isTypeWord))
            .map(MetaCircuitPrettyParser::parseInstruction)
            .toList();

    // Branch information
    final Terminator terminator = parseTerminator(iter);

    iter.expectEnd();

    return new Block(blockId, inputs, instructions, terminator);
  }

  private static Terminator parseTerminator(final SequenceIter iter) {
    // Check for branch-if
    final var branchIfSeq = iter.takeSequenceIf(Sexp.Sequence.startsWithWord("branch-if"));
    if (branchIfSeq != null) {
      return parseBranchIfTerminator(branchIfSeq);
    }

    // Check for branch (always)
    final var branchAlwaysSeq = iter.takeSequenceIf(Sexp.Sequence.startsWithWord("branch-always"));
    if (branchAlwaysSeq != null) {
      return parseBranchAlwaysTerminator(branchAlwaysSeq);
    }

    // Check for Call
    final var branchCall = iter.takeSequenceIf(Sexp.Sequence.startsWithWord("call"));
    if (branchCall != null) {
      return parseCallTerminator(branchCall);
    }

    // Default is to return
    return new Terminator.BranchAlways(new Terminator.BranchInfo(BlockId.RETURN, List.of()));
  }

  private static Terminator.BranchIf parseBranchIfTerminator(final Sexp.Sequence seq) {
    // Parse branch-if sexp
    final var branchesIter = seq.iterator();
    branchesIter.next(); // Skip branch-if
    final VariableId conditionVariable = parseVariableId(branchesIter.nextWord());

    final var branchFalseIter = branchesIter.nextSequence().iterator();
    final var branchTrueIter = branchesIter.nextSequence().iterator();
    branchesIter.expectEnd();

    // Parse False branch
    branchFalseIter.next().expectWord("0");
    final Terminator.BranchInfo branchFalse = parseBranchInfo(branchFalseIter);

    // Parse True branch
    branchTrueIter.next().expectWord("1");
    final Terminator.BranchInfo branchTrue = parseBranchInfo(branchTrueIter);

    return new Terminator.BranchIf(conditionVariable, branchTrue, branchFalse);
  }

  private static Terminator.BranchAlways parseBranchAlwaysTerminator(final Sexp.Sequence seq) {
    final var branchesIter = seq.iterator();
    branchesIter.next();
    final Terminator.BranchInfo branchInfo = parseBranchInfo(branchesIter);
    return new Terminator.BranchAlways(branchInfo);
  }

  private static Terminator.Call parseCallTerminator(final Sexp.Sequence seq) {
    final var branchesIter = seq.iterator();
    branchesIter.next(); // Skip call

    final var functionAndArguments = branchesIter.next().expectSequence().iterator();
    final FunctionId functionId = parseFunctionId(functionAndArguments.next().expectWord());
    final List<VariableId> arguments =
        functionAndArguments
            .takeRemaining()
            .map(Sexp::expectWord)
            .map(MetaCircuitPrettyParser::parseVariableId)
            .toList();

    final Terminator.BranchInfo branchInfo =
        parseBranchInfo(branchesIter.next().expectSequence().iterator());
    return new Terminator.Call(functionId, arguments, branchInfo);
  }

  private static Terminator.BranchInfo parseBranchInfo(final SequenceIter iter) {
    final BlockId targetBlock = parseBlockId(iter.nextWord());
    final List<VariableId> branchInputVariables =
        iter.takeRemaining()
            .map(Sexp::expectWord)
            .map(MetaCircuitPrettyParser::parseVariableId)
            .toList();
    return new Terminator.BranchInfo(targetBlock, branchInputVariables);
  }

  private static Block.Input parseInput(final Sexp.Sequence sexp) {
    final SequenceIter iter = sexp.iterator();
    final Type type = parseType(iter.nextWord());
    final VariableId variable = parseVariableId(iter.nextWord());
    iter.expectEnd();
    return new Block.Input(variable, type);
  }

  private static Instruction parseInstruction(final Sexp.Sequence sexp) {
    final SequenceIter iter = sexp.iterator();
    final Type resultType = parseType(iter.nextWord());
    final VariableId variable = parseVariableId(iter.nextWord());
    final SequenceIter operationSeqIterator = iter.nextSequence().iterator();
    final Operation operation = parseOperation(operationSeqIterator, resultType);
    operationSeqIterator.expectEnd();
    iter.expectEnd();
    return new Instruction(variable, resultType, operation);
  }

  /**
   * Parses an operation from the given iterator.
   *
   * @param iter Sequence iterator over the sequence to parse.
   * @param resultType Previously parsed result type. Required for certain operations.
   * @return Newly parsed operation.
   */
  private static Operation parseOperation(final SequenceIter iter, final Type resultType) {
    final String opname = iter.nextWord().word().toUpperCase(Locale.ENGLISH);

    // Constant
    if (opname.equals(Operation.Constant.SYMBOLIC_NAME)) {
      return new Operation.Constant(resultType, parseInteger(iter.nextWord()));

      // Cast
    } else if (opname.equals(Operation.Cast.SYMBOLIC_NAME)) {
      return new Operation.Cast(resultType, parseVariableId(iter.nextWord()));

      // Select
    } else if (opname.equals(Operation.Select.SYMBOLIC_NAME)) {
      return new Operation.Select(
          parseVariableId(iter.nextWord()),
          parseVariableId(iter.nextWord()),
          parseVariableId(iter.nextWord()));

      // Extract
    } else if (opname.equals(Operation.Extract.SYMBOLIC_NAME)) {
      return new Operation.Extract(
          parseVariableId(iter.nextWord()),
          parseInteger(iter.nextWord()),
          parseInteger(iter.nextWord()));

      // Extract dynamically
    } else if (opname.equals(Operation.ExtractDynamically.SYMBOLIC_NAME)) {
      return new Operation.ExtractDynamically(
          parseVariableId(iter.nextWord()),
          parseInteger(iter.nextWord()),
          parseVariableId(iter.nextWord()));

      // Insert dynamically
    } else if (opname.equals(Operation.InsertDynamically.SYMBOLIC_NAME)) {
      return new Operation.InsertDynamically(
          parseVariableId(iter.nextWord()),
          parseVariableId(iter.nextWord()),
          parseVariableId(iter.nextWord()));

      // Binary
    } else if ((Object) valueOfOrNull(Operation.BinaryOp.class, opname)
        instanceof Operation.BinaryOp binop) {
      return new Operation.Binary(
          binop, parseVariableId(iter.nextWord()), parseVariableId(iter.nextWord()));

      // Unary
    } else if ((Object) valueOfOrNull(Operation.UnaryOp.class, opname)
        instanceof Operation.UnaryOp unop) {
      return new Operation.Unary(unop, parseVariableId(iter.nextWord()));

      // Nullary
    } else if ((Object) valueOfOrNull(Operation.NullaryOp.class, opname)
        instanceof Operation.NullaryOp nulop) {
      return new Operation.Nullary(nulop);
    }

    throw new UnsupportedOperationException("Invalid input: Unknown operation: " + opname);
  }

  //// Word parsing

  private static BlockId parseBlockId(final Sexp.Word sexp) {
    if (sexp.word().equals("#return")) {
      return BlockId.RETURN;
    } else if (!sexp.word().startsWith("#")) {
      throw new RuntimeException(
          "Invalid input: Expected block id, found \"%s\"".formatted(sexp.word()));
    }
    return new BlockId(Integer.parseInt(sexp.word().substring(1)));
  }

  private static int parseInteger(final Sexp.Word sexp) {
    try {
      return Integer.parseInt(sexp.word());
    } catch (NumberFormatException e) {
      throw new RuntimeException(
          "Invalid input: Expected integer, found \"%s\"".formatted(sexp.word()), e);
    }
  }

  private static VariableId parseVariableId(final Sexp.Word sexp) {
    if (!sexp.word().matches("^\\$\\d+$")) {
      throw new RuntimeException(
          "Invalid input: Expected variable, found \"%s\"".formatted(sexp.word()));
    }
    return new VariableId(Integer.parseInt(sexp.word().substring(1)));
  }

  private static FunctionId parseFunctionId(final Sexp.Word sexp) {
    if (!sexp.word().matches("^\\%\\d+$")) {
      throw new RuntimeException(
          "Invalid input: Expected function id, found \"%s\"".formatted(sexp.word()));
    }
    return new FunctionId(Integer.parseInt(sexp.word().substring(1)));
  }

  private static Type parseType(final Sexp.Word sexp) {
    final Type type = parseTypeOrNull(sexp);
    if (type == null) {
      throw new UnsupportedOperationException("Invalid input: Unknown type: " + sexp.word());
    }
    return type;
  }

  private static Type parseTypeOrNull(final Sexp.Word sexp) {
    if (sexp.word().matches("^sbi\\d+$")) {
      return new Type.SecretBinaryInteger(Integer.parseInt(sexp.word().substring(3)));
    } else if (sexp.word().matches("^i\\d+$")) {
      return new Type.PublicInteger(Integer.parseInt(sexp.word().substring(1)));
    }
    return null;
  }

  //// Predicates and utility

  private static boolean isTypeWord(final Sexp sexp) {
    return sexp instanceof Sexp.Word word && parseTypeOrNull(word) != null;
  }

  private static <T extends Enum<T>> T valueOfOrNull(final Class<T> enumType, final String name) {
    try {
      return Enum.valueOf(enumType, name);
    } catch (IllegalArgumentException e) {
      return null;
    }
  }
}
