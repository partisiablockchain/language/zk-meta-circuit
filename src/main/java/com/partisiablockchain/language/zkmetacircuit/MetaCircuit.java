package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;
import java.util.List;

/**
 * Zero-knowledge Meta-circuit containing several basic blocks.
 *
 * @param functions Functions in meta circuit.
 */
public record MetaCircuit(List<Function> functions) {

  /**
   * Constructor for {@link MetaCircuit}.
   *
   * @param functions Functions in meta circuit.
   */
  public MetaCircuit {
    requireNonNull(functions);
  }

  /**
   * Individual function in meta-circuit.
   *
   * @param functionId Id of the function.
   * @param blocks Basic blocks of the Meta-Circuit.
   * @param returnTypes Types of the produced values.
   */
  public record Function(FunctionId functionId, List<Block> blocks, List<Type> returnTypes) {
    /**
     * Constructor.
     *
     * @param functionId Id of the function.
     * @param blocks Basic blocks of the Meta-Circuit.
     * @param returnTypes Types of the output variables.
     */
    public Function {
      requireNonNull(functionId);
      requireNonNull(blocks);
      requireNonNull(returnTypes);
    }
  }

  static Normalized ofNormalized(final List<Function> functions) {
    return new MetaCircuit.Normalized(new MetaCircuit(functions));
  }

  /**
   * Wrapper for normalized meta-circuits. Can only be created by {@link MetaCircuitNormalizer} or
   * {@link MetaCircuitSerializer}.
   */
  @Immutable
  @SuppressWarnings("Immutable")
  public static final class Normalized {
    private final MetaCircuit metaCircuit;

    /**
     * Wrapped meta-circuit.
     *
     * @return Wrapped meta-circuit. Never null.
     */
    public MetaCircuit metaCircuit() {
      return metaCircuit;
    }

    Normalized(final MetaCircuit metaCircuit) {
      requireNonNull(metaCircuit);
      this.metaCircuit = metaCircuit;
    }

    @Override
    public String toString() {
      return "MetaCircuit.Normalized[metaCircuit=%s]".formatted(metaCircuit);
    }
  }
}
