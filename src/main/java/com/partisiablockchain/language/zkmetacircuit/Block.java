package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;
import java.util.List;

/**
 * Basic block within a Zero-knowledge Meta-circuit, containing some instructions, and possibly a
 * branching operation.
 *
 * <p>If the branch-iffield is null, the block is expected to be the end block, in the circuit.
 */
public record Block(
    BlockId blockId, List<Input> inputs, List<Instruction> instructions, Terminator terminator) {

  /**
   * Constructor.
   *
   * @param blockId Id of block. Never null.
   * @param inputs Expected inputs for block. Never null.
   * @param instructions Instructions for block to contain. Never null.
   * @param terminator Terminator for block. Never null.
   */
  public Block {
    requireNonNull(blockId);
    requireNonNull(inputs);
    requireNonNull(instructions);
    requireNonNull(terminator);
  }

  /** Represents input arguments for block. */
  @Immutable
  public record Input(VariableId assignedVariable, Type variableType) {

    /**
     * Constructor.
     *
     * @param assignedVariable Variable id of input. Never null.
     * @param variableType Type of input variable. Never null.
     */
    public Input {
      requireNonNull(assignedVariable);
      requireNonNull(variableType);
    }
  }
}
