package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.IntFunction;
import org.bouncycastle.util.encoders.Hex;

/** Provides utility methods for serializing and deserializing Meta-Circuits. */
public final class MetaCircuitSerializer {

  private MetaCircuitSerializer() {}

  /**
   * The magic bytes that occur at the start of every meta circuit byte array to distinguish it from
   * other file formats.
   */
  private static final byte[] MAGIC_BYTES = new byte[] {0x5A, 0x4B, 0x4D, 0x43};

  /**
   * Byte code version that is parsable by the serializer. The serializer cannot parse versions of
   * different major versions, nor of higher minors.
   *
   * <p>The version field in the meta circuit is mainly for quickly determining whether the meta
   * circuit bytecode can be parsed at all. Manually encoding a weird version shouldn't really be a
   * problem other than causing failures at other points in the parsing.
   */
  public static final Version PARSABLE_VERSION = Version.VERSION_1_8;

  private static final int CONDITION_ALWAYS = -1;
  private static final int CONDITION_CALL = -2;

  private static final FunctionId DEFAULT_FUNCTION_ID = new FunctionId(0);

  /**
   * Serializes object to stream.
   *
   * @param stream Stream to serialize to.
   * @param normalizedMetaCircuit Meta-Circuit to serialize.
   */
  public static void serializeMetaCircuitToStream(
      final SafeDataOutputStream stream, final MetaCircuit.Normalized normalizedMetaCircuit) {
    final MetaCircuit metaCircuit = normalizedMetaCircuit.metaCircuit();
    final VersionConfig mcVersionConfig =
        MetaCircuitVersioning.determineEarliestVersion(metaCircuit);

    // Write magic and version bytes
    stream.write(MAGIC_BYTES);
    stream.writeShort(mcVersionConfig.version().major());
    stream.writeShort(mcVersionConfig.version().minor());

    // Write functions
    if (mcVersionConfig.supportsFunctions()) {
      stream.writeInt(metaCircuit.functions().size());
    }
    for (final MetaCircuit.Function function : metaCircuit.functions()) {
      if (mcVersionConfig.supportsFunctions()) {
        stream.writeInt(function.functionId().functionId());
      }
      serializeFunctionToStream(stream, function);
    }
  }

  private static void serializeFunctionToStream(
      final SafeDataOutputStream stream, final MetaCircuit.Function function) {
    // Write outputs
    serializeListToStream(
        stream, function.returnTypes(), MetaCircuitSerializer::serializeTypeToStream);

    // Write blocks
    stream.writeInt(function.blocks().size());
    for (final Block block : function.blocks()) {
      serializeBlockToStream(stream, block);
    }
  }

  /**
   * Deserializes Meta-circuit from stream. Does not check for full reading, so caller must perform
   * this check themselves, for example by using {@link SafeDataInputStream#readFully}.
   *
   * @param stream Stream to serialize from.
   * @return Newly parsed Meta-Circuit. Never null.
   */
  public static MetaCircuit.Normalized deserializeMetaCircuitFromStream(
      final SafeDataInputStream stream) {
    // Read magic bytes
    final byte[] maybeMagic = stream.readBytes(MAGIC_BYTES.length);
    if (!Arrays.equals(maybeMagic, MAGIC_BYTES)) {
      throw new RuntimeException(
          "Malformed Bytecode: Unknown magic bytes 0x"
              + Hex.toHexString(maybeMagic)
              + ", expected 0x"
              + Hex.toHexString(MAGIC_BYTES));
    }

    // Read versions
    final short versionMajor = stream.readShort();
    final short versionMinor = stream.readShort();
    final Version parsedVersion = new Version(versionMajor, versionMinor);
    if (!parsedVersion.isBackwardsCompatibleWith(PARSABLE_VERSION)) {
      throw new RuntimeException(
          "Bytecode version newer than supported versions"
              + "\n  Supported: v%d.0 - v%d.%d\n  Parsed:    v%d.%d"
                  .formatted(
                      PARSABLE_VERSION.major(),
                      PARSABLE_VERSION.major(),
                      PARSABLE_VERSION.minor(),
                      parsedVersion.major(),
                      parsedVersion.minor()));
    }
    final VersionConfig mcVersionConfig = MetaCircuitVersioning.configFromVersion(parsedVersion);

    // Read functions
    final List<MetaCircuit.Function> functions;
    if (mcVersionConfig.supportsFunctions()) {
      functions =
          readDynamicList(
              stream,
              idx -> {
                final FunctionId functionId = deserializeFunctionIdFromStream(stream);
                return deserializeFunctionFromStream(stream, functionId);
              });
    } else {
      functions = List.of(deserializeFunctionFromStream(stream, DEFAULT_FUNCTION_ID));
    }

    return MetaCircuit.ofNormalized(functions);
  }

  private static MetaCircuit.Function deserializeFunctionFromStream(
      final SafeDataInputStream stream, FunctionId functionId) {
    // Read outputs
    final List<Type> returnTypes =
        readDynamicList(stream, idx -> deserializeTypeFromStream(stream));

    // Read Blocks
    final List<Block> blocks =
        readDynamicList(stream, idx -> deserializeBlockFromStream(stream, new BlockId(idx)));

    return new MetaCircuit.Function(functionId, blocks, returnTypes);
  }

  private static void serializeBlockToStream(final SafeDataOutputStream stream, final Block block) {

    // Input variables
    serializeListToStream(
        stream, block.inputs(), (s, i) -> serializeTypeToStream(s, i.variableType()));

    // Instructions
    serializeListToStream(
        stream, block.instructions(), MetaCircuitSerializer::serializeInstructionToStream);

    // Branch information
    serializeTerminatorToStream(stream, block.terminator());
  }

  private static void serializeTerminatorToStream(
      final SafeDataOutputStream stream, final Terminator uncheckedTerminator) {

    if (uncheckedTerminator instanceof Terminator.BranchAlways terminator) {
      stream.writeInt(CONDITION_ALWAYS);
      serializeBranchInfoToStream(stream, terminator.branch());

    } else if (uncheckedTerminator instanceof Terminator.Call terminator) {
      stream.writeInt(CONDITION_CALL);
      serializeFunctionIdToStream(stream, terminator.functionId());
      serializeListToStream(
          stream, terminator.functionArguments(), MetaCircuitSerializer::serializeVariableToStream);
      serializeBranchInfoToStream(stream, terminator.branchToAfterReturn());

    } else {
      final Terminator.BranchIf terminator = (Terminator.BranchIf) uncheckedTerminator;

      serializeVariableToStream(stream, terminator.conditionVariable());
      serializeBranchInfoToStream(stream, terminator.branchTrue());
      serializeBranchInfoToStream(stream, terminator.branchFalse());
    }
  }

  private static void serializeBranchInfoToStream(
      final SafeDataOutputStream stream, final Terminator.BranchInfo branch) {
    serializeBlockIdToStream(stream, branch.targetBlock());
    serializeListToStream(
        stream, branch.branchInputVariables(), MetaCircuitSerializer::serializeVariableToStream);
  }

  private static Block deserializeBlockFromStream(
      final SafeDataInputStream stream, final BlockId blockId) {

    // Input variables
    final List<Block.Input> inputs =
        readDynamicList(
            stream,
            idx -> {
              final Type variableType = deserializeTypeFromStream(stream);
              return new Block.Input(new VariableId(idx), variableType);
            });

    // Instructions
    final List<Instruction> instructions =
        readDynamicList(
            stream,
            idx -> deserializeInstructionFromStream(stream, new VariableId(idx + inputs.size())));

    // Branch information
    final Terminator terminator = deserializeTerminatorFromStream(stream);

    return new Block(blockId, inputs, instructions, terminator);
  }

  private static Terminator deserializeTerminatorFromStream(final SafeDataInputStream stream) {

    final int conditionVariableRaw = stream.readInt();

    if (conditionVariableRaw == CONDITION_ALWAYS) {
      final Terminator.BranchInfo branch = deserializeBranchInfoFromStream(stream);
      return new Terminator.BranchAlways(branch);

    } else if (conditionVariableRaw == CONDITION_CALL) {
      final FunctionId functionId = deserializeFunctionIdFromStream(stream);
      final List<VariableId> functionArguments =
          readDynamicList(stream, idx -> deserializeVariableIdFromStream(stream));

      final Terminator.BranchInfo branchToAfterReturn = deserializeBranchInfoFromStream(stream);
      return new Terminator.Call(functionId, functionArguments, branchToAfterReturn);

    } else {
      final Terminator.BranchInfo branchTrue = deserializeBranchInfoFromStream(stream);
      final Terminator.BranchInfo branchFalse = deserializeBranchInfoFromStream(stream);
      return new Terminator.BranchIf(new VariableId(conditionVariableRaw), branchTrue, branchFalse);
    }
  }

  private static Terminator.BranchInfo deserializeBranchInfoFromStream(
      final SafeDataInputStream stream) {

    final BlockId targetBlock = deserializeBlockIdFromStream(stream);

    final List<VariableId> branchInputVariables =
        readDynamicList(stream, idx -> deserializeVariableIdFromStream(stream));

    return new Terminator.BranchInfo(targetBlock, branchInputVariables);
  }

  /**
   * Deserializes instruction from stream.
   *
   * <h4>Bytecode Format</h4>
   *
   * <pre>| type: 2 bytes | opcode: 2 bytes | arg_0: 4 bytes | arg_1: 4 bytes | ... | arg_n </pre>
   *
   * <p>Where argument count {@code n} is encoded in the lower 2 bits of {@code opcode}.
   */
  private static Instruction deserializeInstructionFromStream(
      final SafeDataInputStream stream, final VariableId resultId) {
    final Type resultType = deserializeTypeFromStream(stream);
    final Operation operation = deserializeOperationFromStream(stream, resultType);
    return new Instruction(resultId, resultType, operation);
  }

  private static void serializeInstructionToStream(
      final SafeDataOutputStream stream, final Instruction insn) {
    serializeTypeToStream(stream, insn.resultType());
    serializeOperationToStream(stream, insn.operation());
  }

  private static void serializeOperationToStream(
      final SafeDataOutputStream stream, final Operation uncheckedOperation) {
    // Unary
    if (uncheckedOperation instanceof Operation.Unary operation) {
      stream.writeShort(operation.operation().opcode());
      serializeVariableToStream(stream, operation.varIdx1());

      // Nullary
    } else if (uncheckedOperation instanceof Operation.Nullary operation) {
      stream.writeShort(Operation.Nullary.OPCODE);
      stream.writeInt(operation.operation().opcode());

      // Cast
    } else if (uncheckedOperation instanceof Operation.Cast operation) {
      stream.writeShort(Operation.Cast.OPCODE);
      serializeVariableToStream(stream, operation.varIdx1());

      // Binary
    } else if (uncheckedOperation instanceof Operation.Binary operation) {
      stream.writeShort(operation.operation().opcode());
      serializeVariableToStream(stream, operation.varIdx1());
      serializeVariableToStream(stream, operation.varIdx2());

      // Select

    } else if (uncheckedOperation instanceof Operation.Select operation) {
      stream.writeShort(Operation.Select.OPCODE);
      serializeVariableToStream(stream, operation.varCondition());
      serializeVariableToStream(stream, operation.varIfTrue());
      serializeVariableToStream(stream, operation.varIfFalse());

      // Extract
    } else if (uncheckedOperation instanceof Operation.Extract operation) {
      stream.writeShort(Operation.Extract.OPCODE);
      serializeVariableToStream(stream, operation.varIdx1());
      stream.writeInt(operation.bitWidth());
      stream.writeInt(operation.offsetFromLow());

      // Extract Dynamically
    } else if (uncheckedOperation instanceof Operation.ExtractDynamically operation) {
      stream.writeShort(Operation.ExtractDynamically.OPCODE);
      serializeVariableToStream(stream, operation.varIdxExtractFrom());
      stream.writeInt(operation.bitWidth());
      serializeVariableToStream(stream, operation.varIdxOffsetFromLow());

      // Insert Dynamically
    } else if (uncheckedOperation instanceof Operation.InsertDynamically operation) {
      stream.writeShort(Operation.InsertDynamically.OPCODE);
      serializeVariableToStream(stream, operation.varIdxInsertInto());
      serializeVariableToStream(stream, operation.varIdxInserted());
      serializeVariableToStream(stream, operation.varIdxOffsetFromLow());

      // Constant
    } else {
      final Operation.Constant operation = (Operation.Constant) uncheckedOperation;
      stream.writeShort(Operation.Constant.OPCODE);
      stream.writeInt(operation.const1());
    }
  }

  /**
   * Deserializes instruction from stream.
   *
   * <h4>Bytecode Format</h4>
   *
   * <pre>| opcode | arg_0 | arg_1 | ... | arg_n </pre>
   *
   * <p>Where argument count {@code n} is encoded in the lower 2 bits of {@code opcode}.
   *
   * @param stream Stream to serialize from.
   * @return Newly parsed instruction. Never null.
   */
  private static Operation deserializeOperationFromStream(
      final SafeDataInputStream stream, final Type resultType) {
    final int opcode = stream.readShort();
    final int numArgs = opcode & 0x03;
    final List<Integer> args = readArray(numArgs, idx -> stream.readInt());

    // Constant
    if (opcode == Operation.Constant.OPCODE) {
      return new Operation.Constant(resultType, args.get(0));

      // Cast
    } else if (opcode == Operation.Cast.OPCODE) {
      return new Operation.Cast(resultType, new VariableId(args.get(0)));

      // Nullary
    } else if (opcode == Operation.Nullary.OPCODE) {
      final Operation.NullaryOp nullaryop =
          Operation.NullaryOp.byOpcode.getOrDefault(args.get(0), null);
      if (nullaryop == null) {
        throw new UnsupportedOperationException(
            String.format("Malformed Bytecode: Unknown nullary operation 0x%08x", args.get(0)));
      }
      return new Operation.Nullary(nullaryop);

      // Select
    } else if (opcode == Operation.Select.OPCODE) {
      return new Operation.Select(
          new VariableId(args.get(0)), new VariableId(args.get(1)), new VariableId(args.get(2)));

      // Extract
    } else if (opcode == Operation.Extract.OPCODE) {
      return new Operation.Extract(new VariableId(args.get(0)), args.get(1), args.get(2));

      // Extract dynamically
    } else if (opcode == Operation.ExtractDynamically.OPCODE) {
      return new Operation.ExtractDynamically(
          new VariableId(args.get(0)), args.get(1), new VariableId(args.get(2)));

      // Insert dynamically
    } else if (opcode == Operation.InsertDynamically.OPCODE) {
      return new Operation.InsertDynamically(
          new VariableId(args.get(0)), new VariableId(args.get(1)), new VariableId(args.get(2)));
    }

    // Binary
    final Operation.BinaryOp binop = Operation.BinaryOp.byOpcode.getOrDefault(opcode, null);
    if (binop != null) {
      return new Operation.Binary(binop, new VariableId(args.get(0)), new VariableId(args.get(1)));
    }

    // Unary
    final Operation.UnaryOp unop = Operation.UnaryOp.byOpcode.getOrDefault(opcode, null);
    if (unop != null) {
      return new Operation.Unary(unop, new VariableId(args.get(0)));
    }

    throw new UnsupportedOperationException(
        String.format("Malformed Bytecode: Unknown opcode 0x%08x", opcode));
  }

  /**
   * Deserializes Type from stream.
   *
   * <h4>Bytecode Format</h4>
   *
   * <pre>0xTT 0xWW</pre>
   *
   * <p>Where {@code TT} is the prefix code for the general type kind, and {@code WW} is some
   * extension, most often the width.
   *
   * @param stream Stream to serialize from.
   * @return Newly parsed Type. Never null.
   */
  private static Type deserializeTypeFromStream(final SafeDataInputStream stream) {
    final byte prefix = stream.readSignedByte();
    // SBI
    if (prefix == Type.SecretBinaryInteger.SERIALIZE_PREFIX) {
      final int bitsize = Leb128.readUnsignedInt(stream);
      return new Type.SecretBinaryInteger(bitsize);

      // Public integer
    } else if (prefix == Type.PublicInteger.SERIALIZE_PREFIX) {
      final int bitsize = Leb128.readUnsignedInt(stream);
      return new Type.PublicInteger(bitsize);

      // Unknown
    } else {
      throw new UnsupportedOperationException(
          String.format("Malformed Bytecode: Unknown type prefix 0x%02x", prefix));
    }
  }

  private static void serializeTypeToStream(
      final SafeDataOutputStream stream, final Type uncheckedType) {
    // Sbi
    if (uncheckedType instanceof Type.SecretBinaryInteger type) {
      stream.writeByte(Type.SecretBinaryInteger.SERIALIZE_PREFIX);
      Leb128.writeUnsignedInt(type.width(), stream);

      // Public integer
    } else {
      final var type = (Type.PublicInteger) uncheckedType;
      stream.writeByte(Type.PublicInteger.SERIALIZE_PREFIX);
      Leb128.writeUnsignedInt(type.width(), stream);
    }
  }

  private static void serializeVariableToStream(
      final SafeDataOutputStream stream, final VariableId variable) {
    stream.writeInt(variable.variableId());
  }

  private static void serializeBlockIdToStream(
      final SafeDataOutputStream stream, final BlockId block) {
    stream.writeInt(block.blockId());
  }

  private static void serializeFunctionIdToStream(
      final SafeDataOutputStream stream, final FunctionId functionId) {
    stream.writeInt(functionId.functionId());
  }

  private static VariableId deserializeVariableIdFromStream(final SafeDataInputStream stream) {
    return new VariableId(stream.readInt());
  }

  private static BlockId deserializeBlockIdFromStream(final SafeDataInputStream stream) {
    return new BlockId(stream.readInt());
  }

  private static FunctionId deserializeFunctionIdFromStream(final SafeDataInputStream stream) {
    return new FunctionId(stream.readInt());
  }

  //// Utility

  /**
   * Reads a list of elements from stream. Format:
   *
   * <pre>| T elem_0 | T elem_1 | ... | T elem_length |</pre>
   *
   * @param numElements number elements to read
   * @param elementReader Reads individual elements
   * @return immutable list of read elements
   */
  private static <T> List<T> readArray(final int numElements, final IntFunction<T> elementReader) {
    final var list = new ArrayList<T>(numElements);
    for (int elemIdx = 0; elemIdx < numElements; elemIdx++) {
      list.add(requireNonNull(elementReader.apply(elemIdx)));
    }
    return List.copyOf(list);
  }

  /**
   * Reads a list of elements from stream. Format:
   *
   * <pre>| int length | T elem_0 | T elem_1 | ... | T elem_length |</pre>
   *
   * @param stream Stream to read from
   * @param elementReader Reads individual elements
   * @return immutable list of read elements
   */
  private static <T> List<T> readDynamicList(
      final SafeDataInputStream stream, final IntFunction<T> elementReader) {
    final int numElements = stream.readInt();
    return readArray(numElements, elementReader);
  }

  private static <T> void serializeListToStream(
      final SafeDataOutputStream stream,
      final List<T> list,
      final BiConsumer<SafeDataOutputStream, T> serializeElement) {
    stream.writeInt(list.size());
    serializeArrayToStream(stream, list, serializeElement);
  }

  private static <T> void serializeArrayToStream(
      final SafeDataOutputStream stream,
      final List<T> list,
      final BiConsumer<SafeDataOutputStream, T> serializeElement) {
    for (final T elem : list) {
      serializeElement.accept(stream, elem);
    }
  }
}
