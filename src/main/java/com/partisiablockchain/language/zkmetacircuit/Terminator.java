package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;
import java.util.List;

/** Terminator of a {@link Block}. */
@Immutable
public sealed interface Terminator {

  /**
   * The bytecode version when this terminator was first introduced.
   *
   * @return The bytecode version.
   */
  Version versionIntroduced();

  /**
   * Symbolic name for error messages.
   *
   * @return Symbolic name, uppercase. Never null.
   */
  String symbolicName();

  /**
   * Unconditional branch.
   *
   * @param branch Jump information. Never null.
   */
  @Immutable
  public record BranchAlways(BranchInfo branch) implements Terminator {

    /**
     * Constructor.
     *
     * @param branch Jump information. Never null.
     */
    public BranchAlways {
      requireNonNull(branch);
    }

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_0;
    }

    @Override
    public String symbolicName() {
      return "BRANCH";
    }
  }

  /** Conditional branching. */
  @Immutable
  public record BranchIf(
      VariableId conditionVariable, BranchInfo branchTrue, BranchInfo branchFalse)
      implements Terminator {

    /**
     * Constructor.
     *
     * @param conditionVariable Variable to branch upon. Never null.
     * @param branchTrue Jump information for case when condition evaluates to true. Never null.
     * @param branchFalse Jump information for case when condition evaluates to false. Never null.
     */
    public BranchIf {
      requireNonNull(conditionVariable);
      requireNonNull(branchTrue);
      requireNonNull(branchFalse);
    }

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_0;
    }

    @Override
    public String symbolicName() {
      return "BRANCH-IF";
    }

    static final int OPCODE = -1;
  }

  /**
   * Call to another function. Once function have completed, call into another block in this
   * function.
   *
   * <p>When execution encounters a {@link Call} terminator it should:
   *
   * <ul>
   *   <li>Push {@link Call#branchToAfterReturn} to the return stack.
   *   <li>Evaluate the function, starting in the first block of the function indicated by {@link
   *       Call#functionId}, using {@link Call#functionArguments} as arguments to the block.
   *   <li>Once a return occurs (by branching to {@link BlockId#RETURN}), the return should be
   *       transformed to a branch to the top of the return stack (that is {@link
   *       Call#branchToAfterReturn}.
   *   <li>The arguments to the branch should be laid out with the arguments from {@link
   *       Call#branchToAfterReturn} first, and the return values second.
   * </ul>
   *
   * @param functionId Id of function to call.
   * @param functionArguments Arguments to call function with.
   * @param branchToAfterReturn Branch to perform when function returns.
   */
  @Immutable
  public record Call(
      FunctionId functionId,
      @SuppressWarnings("Immutable") List<VariableId> functionArguments,
      BranchInfo branchToAfterReturn)
      implements Terminator {

    /**
     * Constructor for {@link Call}.
     *
     * @param functionId Id of function to call.
     * @param functionArguments Arguments to call function with.
     * @param branchToAfterReturn Branch to perform when function returns.
     */
    public Call {
      requireNonNull(functionId);
      requireNonNull(functionArguments);
      requireNonNull(branchToAfterReturn);
    }

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_6;
    }

    @Override
    public String symbolicName() {
      return "CALL";
    }

    static final int OPCODE = -2;
  }

  /** Represents the information used in order to jump to a block. */
  @Immutable
  public record BranchInfo(
      BlockId targetBlock, @SuppressWarnings("Immutable") List<VariableId> branchInputVariables) {

    /**
     * Constructor.
     *
     * @param targetBlock Id of the branch to jump to. Never null.
     * @param branchInputVariables Variables to use for input variables. Never null.
     */
    public BranchInfo {
      requireNonNull(targetBlock);
      requireNonNull(branchInputVariables);
    }
  }
}
