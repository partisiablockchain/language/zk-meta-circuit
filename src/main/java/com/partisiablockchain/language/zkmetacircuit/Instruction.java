package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;

/** Instruction within a Block of Zero-knowledge Circuit. */
@Immutable
public record Instruction(VariableId resultId, Type resultType, Operation operation) {

  /**
   * Constructor.
   *
   * @param resultId Variable to assign result to.
   * @param resultType Type of result variable.
   * @param operation Actual instruction operation.
   */
  public Instruction {
    requireNonNull(resultId);
    requireNonNull(resultType);
    requireNonNull(operation);
  }
}
