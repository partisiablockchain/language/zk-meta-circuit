package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Operation within a Block of Zero-knowledge Circuit.
 *
 * <p>Each operation is assigned an unique {@code OPCODE} allowing it to be determined within the
 * serialized bytecode format. The opcodes must be unique across all {@link Operation}s in order to
 * distinguish them.
 *
 * <p>Additionally, each {@link Operation} must have a unique {@link Operation#symbolicName} for
 * identification in the format used by {@link MetaCircuitPretty} and {@link
 * MetaCircuitPrettyParser}.
 */
@Immutable
public sealed interface Operation {

  /**
   * The bytecode version when this operation was first introduced.
   *
   * @return The bytecode version.
   */
  Version versionIntroduced();

  /**
   * Symbolic name for error messages.
   *
   * @return Symbolic name, uppercase. Never null.
   */
  String symbolicName();

  /**
   * Decides the specific operation a Nullary instruction should compute.
   *
   * @see Nullary
   */
  enum NullaryOp {
    /** Number of variables available to the ZK computation. */
    NUM_VARIABLES(0x1, Version.VERSION_1_1);

    /** Opcode of the operation. Must be unique across all {@link NullaryOp}s. */
    private final int opcode;

    /**
     * The bytecode version when this operation was first introduced.
     *
     * @return The bytecode version.
     */
    private final Version versionIntroduced;

    /**
     * Opcode of the operation.
     *
     * @return opcode value
     */
    public int opcode() {
      return opcode;
    }

    /**
     * The bytecode version when this operation was first introduced.
     *
     * @return The bytecode version.
     */
    public Version versionIntroduced() {
      return versionIntroduced;
    }

    /** Map from integer {@link opcode} field opcode. */
    public static Map<Integer, NullaryOp> byOpcode =
        valuesByKey(NullaryOp.class, NullaryOp::opcode);

    NullaryOp(final int opcode, final Version versionIntroduced) {
      this.opcode = opcode;
      this.versionIntroduced = versionIntroduced;
    }
  }

  /**
   * Decides the specific operation a Unary instruction should compute. Note that opcode's lower 2
   * bits should have pattern {@code 0b01}.
   *
   * @see Unary
   */
  enum UnaryOp {
    /** Bit-flip instruction. Each individual bit of input variable is flipped. */
    BITWISE_NOT(0x09, Version.VERSION_1_0),
    /**
     * Arithmetic sign change / additive inverse / negation instruction.
     *
     * <p>For binary numbers, this is defined as two's complement.
     */
    NEGATE(0x0d, Version.VERSION_1_0),

    /** Loads variable as result type. */
    LOAD_VARIABLE(0x15, Version.VERSION_1_1),

    /** Loads variable metadata as result type. */
    LOAD_METADATA(0x19, Version.VERSION_1_1),

    /**
     * Produces the ID of the next variable, given some valid ID. If given value is zero, return the
     * ID of the first variable. Produces zero after the last variable.
     */
    NEXT_VARIABLE_ID(0x21, Version.VERSION_1_4),

    /**
     * Outputs the given secret-shared variable.
     *
     * <p>The variable is added to the list of result values to be produced at the end of execution.
     * Variables are produced in the order of {@code OUTPUT} calls. The operation will always
     * produce {@link Type#Unit}, no matter given input {@link Type}.
     */
    OUTPUT(0x25, Version.VERSION_1_5);

    /** Opcode of the operation. Must be unique between all opcodes, not just between UnaryOps. */
    private final int opcode;

    /**
     * The bytecode version when this operation was first introduced.
     *
     * @return The bytecode version.
     */
    private final Version versionIntroduced;

    /**
     * Opcode of the operation.
     *
     * @return opcode value
     */
    public int opcode() {
      return opcode;
    }

    /**
     * The bytecode version when this operation was first introduced.
     *
     * @return The bytecode version.
     */
    public Version versionIntroduced() {
      return versionIntroduced;
    }

    /** Map from integer {@link opcode} field opcode. */
    public static Map<Integer, UnaryOp> byOpcode = valuesByKey(UnaryOp.class, UnaryOp::opcode);

    UnaryOp(final int opcode, final Version versionIntroduced) {
      this.opcode = opcode;
      this.versionIntroduced = versionIntroduced;
    }
  }

  /**
   * Decides the specific operation a Binary instruction should compute.Note that opcode's lower 2
   * bits should have pattern {@code 0b10}.
   *
   * @see Binary
   */
  enum BinaryOp {

    //// Bit operations

    /** Bitwise And operation. Sign agnostic. */
    BITWISE_AND(0x02, Version.VERSION_1_0),
    /** Bitwise Or operation. Sign agnostic. */
    BITWISE_OR(0x06, Version.VERSION_1_0),
    /** Bitwise Xor operation. Sign agnostic. */
    BITWISE_XOR(0x0a, Version.VERSION_1_0),
    /**
     * Shift Right/Down Logical.
     *
     * <p>Discards the lowest-bit, moves all other bits down one place, and shifts zero into the
     * high bit.
     */
    BITSHIFT_RIGHT_LOGICAL(0x12, Version.VERSION_1_0),
    /**
     * Shift Left/Up Logical.
     *
     * <p>Discards the higest-bit, moves all other bits up one place, and shifts zero into the low
     * bit.
     */
    BITSHIFT_LEFT_LOGICAL(0x16, Version.VERSION_1_0),
    /** Bit concatination. Sign agnostic. */
    BIT_CONCAT(0x4a, Version.VERSION_1_3),

    //// Arithmetic operations

    /** Arithmetic addition operation. Sign agnostic. */
    ADD_WRAPPING(0x0e, Version.VERSION_1_0),
    /**
     * Signed arithmetic multiplication operation. Wraps around to negative numbers if larger than
     * type range.
     */
    MULT_WRAPPING_SIGNED(0x1a, Version.VERSION_1_0),
    /**
     * Unsigned arithmetic multiplication operation. Wraps around to zero if larger than type range.
     */
    MULT_WRAPPING_UNSIGNED(0x22, Version.VERSION_1_2),

    //// Relational operations

    /** Equality relation. Sign agnostic. */
    EQUAL(0x32, Version.VERSION_1_0),

    /** Signed less than relation. Bits are trated as two's complement. */
    LESS_THAN_SIGNED(0x36, Version.VERSION_1_0),
    /** Signed less than or equal relation. Bits are trated as two's complement. */
    LESS_THAN_OR_EQUAL_SIGNED(0x3a, Version.VERSION_1_0),

    /** Unsigned less than relation. */
    LESS_THAN_UNSIGNED(0x42, Version.VERSION_1_2),
    /** Unsigned less than or equal relation. */
    LESS_THAN_OR_EQUAL_UNSIGNED(0x46, Version.VERSION_1_2);

    /** Opcode of the operation. Must be unique between all opcodes, not just between UnaryOps. */
    private final int opcode;

    /**
     * The bytecode version when this operation was first introduced.
     *
     * @return The bytecode version.
     */
    private final Version versionIntroduced;

    /**
     * Opcode of the operation.
     *
     * @return opcode value
     */
    public int opcode() {
      return opcode;
    }

    /**
     * The bytecode version when this operation was first introduced.
     *
     * @return The bytecode version.
     */
    public Version versionIntroduced() {
      return versionIntroduced;
    }

    /** Map from integer {@link opcode} field opcode. */
    public static Map<Integer, BinaryOp> byOpcode = valuesByKey(BinaryOp.class, BinaryOp::opcode);

    BinaryOp(final int opcode, final Version versionIntroduced) {
      this.opcode = opcode;
      this.versionIntroduced = versionIntroduced;
    }
  }

  /**
   * Nullary Operation taking zero variables. The {@code operation} field decides which operation.
   *
   * @see NullaryOp
   */
  @Immutable
  public record Nullary(NullaryOp operation) implements Operation {
    /** {@code OPCODE} for {@link Nullary}. */
    public static final int OPCODE = 0x1d;

    /**
     * Constructor.
     *
     * @param operation Specific operation to perform.
     */
    public Nullary {
      requireNonNull(operation);
    }

    @Override
    public Version versionIntroduced() {
      return operation.versionIntroduced();
    }

    @Override
    public String symbolicName() {
      return operation.toString();
    }
  }

  /**
   * Unary Operation taking one variable. The {@code operation} field decides which operation.
   *
   * @see UnaryOp
   */
  @Immutable
  public record Unary(UnaryOp operation, VariableId varIdx1) implements Operation {
    /**
     * Constructor.
     *
     * @param operation Specific operation to perform.
     * @param varIdx1 Only argument to operation.
     */
    public Unary {
      requireNonNull(operation);
      requireNonNull(varIdx1);
    }

    @Override
    public Version versionIntroduced() {
      return operation.versionIntroduced();
    }

    @Override
    public String symbolicName() {
      return operation.toString();
    }
  }

  /**
   * Cast to result type. Input bits are either truncated or zero-extended to width of result type,
   * and reinterpreted.
   */
  @Immutable
  public record Cast(Type type, VariableId varIdx1) implements Operation {
    /** {@code OPCODE} for {@link Cast}. */
    public static final int OPCODE = 0x11;

    /** {@link symbolicName} for {@link Cast}. */
    public static final String SYMBOLIC_NAME = "CAST";

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_0;
    }

    /**
     * Constructor.
     *
     * @param type Type to cast argument to.
     * @param varIdx1 First and only argument to operation.
     */
    public Cast {
      requireNonNull(type);
      requireNonNull(varIdx1);
    }

    @Override
    public String symbolicName() {
      return SYMBOLIC_NAME;
    }
  }

  /**
   * Constant Operation taking no variables; instead directly producing a value with the constant
   * value.
   */
  @Immutable
  public record Constant(Type type, int const1) implements Operation {

    /** {@code OPCODE} for {@link Constant}. */
    public static final int OPCODE = 0x05;

    /** {@link symbolicName} for {@link Constant}. */
    public static final String SYMBOLIC_NAME = "CONSTANT";

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_0;
    }

    /**
     * Constructor.
     *
     * @param type Type of constant.
     * @param const1 Constant value.
     */
    public Constant {
      requireNonNull(type);
    }

    @Override
    public String symbolicName() {
      return SYMBOLIC_NAME;
    }
  }

  /**
   * Binary Operation taking two variables. The {@code operation} field decides which operation.
   *
   * @see BinaryOp
   */
  @Immutable
  public record Binary(BinaryOp operation, VariableId varIdx1, VariableId varIdx2)
      implements Operation {

    @Override
    public Version versionIntroduced() {
      return operation.versionIntroduced();
    }

    /**
     * Constructor.
     *
     * @param operation Specific operation to perform.
     * @param varIdx1 First argument to operation.
     * @param varIdx2 Second argument to operation.
     */
    public Binary {
      requireNonNull(operation);
      requireNonNull(varIdx1);
      requireNonNull(varIdx2);
    }

    @Override
    public String symbolicName() {
      return operation.toString();
    }
  }

  /** Select Operation, taking three variables. */
  @Immutable
  public record Select(VariableId varCondition, VariableId varIfTrue, VariableId varIfFalse)
      implements Operation {

    /** {@code OPCODE} for {@link Select}. */
    public static final int OPCODE = 0x03;

    /** {@link symbolicName} for {@link Select}. */
    public static final String SYMBOLIC_NAME = "SELECT";

    /**
     * Constructor.
     *
     * @param varCondition Variable to branch on.
     * @param varIfTrue Variable to produce if varCondition evaluates to true.
     * @param varIfFalse Variable to produce if varCondition evaluates to false.
     */
    public Select {
      requireNonNull(varCondition);
      requireNonNull(varIfTrue);
      requireNonNull(varIfFalse);
    }

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_0;
    }

    @Override
    public String symbolicName() {
      return SYMBOLIC_NAME;
    }
  }

  /**
   * Extract Operation, taking three variables.
   *
   * <p>Note that {@code EXTRACT v1 N 0}, where {@code N = bitsize(v1)} is an identity operation.
   *
   * @param varIdx1 Variable to extract from.
   * @param bitWidth Number of bits to extract. Must be non-negative.
   * @param offsetFromLow Offset of bits to insert at, counting from the low bits. Must be
   *     non-negative.
   */
  @Immutable
  public record Extract(VariableId varIdx1, int bitWidth, int offsetFromLow) implements Operation {

    /** {@code OPCODE} for {@link Extract}. */
    public static final int OPCODE = 0x07;

    /** {@link symbolicName} for {@link Extract}. */
    public static final String SYMBOLIC_NAME = "EXTRACT";

    /**
     * Create Extract operation.
     *
     * @param varIdx1 Variable to extract from.
     * @param bitWidth Number of bits to extract. Must be non-negative.
     * @param offsetFromLow Offset of bits to insert at, counting from the low bits. Must be
     *     non-negative.
     */
    public Extract {
      requireNonNull(varIdx1);
      if (bitWidth < 0) {
        throw new RuntimeException("Bitwidth %d must be non-negative.".formatted(bitWidth));
      }
      if (offsetFromLow < 0) {
        throw new RuntimeException("Offset %d must be non-negative.".formatted(offsetFromLow));
      }
    }

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_3;
    }

    @Override
    public String symbolicName() {
      return SYMBOLIC_NAME;
    }
  }

  /**
   * ExtractDynamically Operation, allowing extraction of a number of bits at a dynamic offset from
   * a variable.
   *
   * <p>Note that {@code EXTRACTDYN v1 N 0}, where {@code N = bitsize(v1)} is an identity operation.
   *
   * @param varIdxExtractFrom Variable to extract from.
   * @param bitWidth Number of bits to extract. Must be non-negative.
   * @param varIdxOffsetFromLow Variable to use as offset of bits to extract from, counting from the
   *     low bits.
   */
  @Immutable
  public record ExtractDynamically(
      VariableId varIdxExtractFrom, int bitWidth, VariableId varIdxOffsetFromLow)
      implements Operation {

    /** {@code OPCODE} for {@link ExtractDynamically}. */
    public static final int OPCODE = 0x0b;

    /** {@link symbolicName} for {@link ExtractDynamically}. */
    public static final String SYMBOLIC_NAME = "EXTRACTDYN";

    /**
     * Create {@link ExtractDynamically} operation.
     *
     * @param varIdxExtractFrom Variable to extract from.
     * @param bitWidth Number of bits to extract. Must be non-negative.
     * @param varIdxOffsetFromLow Variable to use as offset of bits to extract from, counting from
     *     the low bits.
     */
    public ExtractDynamically {
      requireNonNull(varIdxExtractFrom);
      if (bitWidth < 0) {
        throw new RuntimeException("Bitwidth %d must be non-negative.".formatted(bitWidth));
      }
      requireNonNull(varIdxOffsetFromLow);
    }

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_7;
    }

    @Override
    public String symbolicName() {
      return SYMBOLIC_NAME;
    }
  }

  /**
   * InsertDynamically Operation, allowing overwriting a range of one variable's bits with another
   * variable's bits, at a public offset.
   *
   * <p>Note that {@code INSERTDYN v1 v2 0}, where {@code bitsize(v1) = bitsize(v2)} is an identity
   * operation over {@code v2}.
   *
   * @param varIdxInsertInto Target variable to insert into.
   * @param varIdxInserted Variable to insert.
   * @param varIdxOffsetFromLow Variable to use as offset of bits to insert at, counting from the
   *     low bits.
   */
  @Immutable
  public record InsertDynamically(
      VariableId varIdxInsertInto, VariableId varIdxInserted, VariableId varIdxOffsetFromLow)
      implements Operation {

    /** {@code OPCODE} for {@link InsertDynamically}. */
    public static final int OPCODE = 0x0f;

    /** {@link symbolicName} for {@link InsertDynamically}. */
    public static final String SYMBOLIC_NAME = "INSERTDYN";

    /**
     * Create {@link InsertDynamically} operation.
     *
     * @param varIdxInsertInto Variable to insert into.
     * @param varIdxInserted Variable to insert.
     * @param varIdxOffsetFromLow Variable to use as offset of bits to insert at, counting from the
     *     low bits.
     */
    public InsertDynamically {
      requireNonNull(varIdxInsertInto);
      requireNonNull(varIdxInserted);
      requireNonNull(varIdxOffsetFromLow);
    }

    @Override
    public Version versionIntroduced() {
      return Version.VERSION_1_7;
    }

    @Override
    public String symbolicName() {
      return SYMBOLIC_NAME;
    }
  }

  /// Utility

  /**
   * Utility for creating a map of from a property of an enum value to the enum value itself. Mainly
   * used for creating the {@code byOpcode} maps for {@link UnaryOp} and {@link BinaryOp} enums.
   *
   * @param <E> Enum type to create map for
   * @param <K> Key property type
   * @param classObj Enum class to create map for
   * @param keyExtractor Property to use as key
   * @return Never null
   */
  static <E extends Enum<E>, K> Map<K, E> valuesByKey(
      final Class<E> classObj, final Function<E, K> keyExtractor) {
    return Arrays.stream(classObj.getEnumConstants())
        .collect(Collectors.toUnmodifiableMap(keyExtractor, Function.identity()));
  }
}
