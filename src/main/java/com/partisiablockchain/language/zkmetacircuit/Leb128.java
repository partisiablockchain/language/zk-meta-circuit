package com.partisiablockchain.language.zkmetacircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/**
 * Decoder for LEB128 variable-length integer encoding and decoding.
 *
 * <p>LEB128 was originally defined in the DWARF Debugging Information Format.
 *
 * <p>See section 7.6 in https://dwarfstd.org/doc/dwarf-2.0.0.pdf
 */
public final class Leb128 {

  /** Class has only static methods, so it cannot be instantiated. */
  private Leb128() {}

  /**
   * Decode an unsigned LEB128 32-bit integer from a stream of bytes.
   *
   * @param bytes The byte stream to fetch bytes from
   * @return The unsigned N-bit value as a int.
   * @throws RuntimeException if the number cannot be parsed
   */
  public static int readUnsignedInt(SafeDataInputStream bytes) {
    return readUnsigned(31, bytes);
  }

  static int readUnsigned(int bitsN, SafeDataInputStream bytes) {
    int n = bytes.readUnsignedByte();
    if (n < 0x80) {
      if (bitsN <= 6 && n >= (1L << bitsN)) {
        throw new RuntimeException(
            "Integer too large: Too many bits in LEB128 encoded unsigned number.");
      }
      return n;
    } else {
      if (bitsN <= 7) {
        throw new RuntimeException(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");
      }
      final int m = readUnsigned(bitsN - 7, bytes);
      return (m << 7) + (n & 0x7f);
    }
  }

  /**
   * Encode an unsigned LEB128 32-bit integer to a stream of bytes.
   *
   * @param value Number to encode.
   * @param bytes The byte stream to fetch bytes from
   */
  public static void writeUnsignedInt(int value, SafeDataOutputStream bytes) {
    boolean first = true;
    while (value > 0 || first) {
      final int sevenBitSegment = value & 0x7F;
      value = value >> 7;
      final int contBit = (value != 0) ? 0x80 : 0x00;
      bytes.writeByte((byte) (contBit | sevenBitSegment));
      first = false;
    }
  }
}
